%% MED BDS
% Store current directory
current_dir = pwd;

readRinexObs304_dirpath = "..\..\GNSS_Receiver_QC_2020\MATLAB_sourcecode";

%data dir relative to readRinexObs304_dirpath 
data_dir   = "..\..\Data\readRinexObs304\Obs_files\PH_RINEX-Obs-filer";
filename    = "OPEC_20220010000_BDS_LESES.obs";
filename = append(data_dir, '\', filename);


% changing working directory to readRinexObs304
cd(readRinexObs304_dirpath)

includeAllGNSSsystems = 1;
includeAllObsCodes = 1;
desiredGNSSsystems = [];
desiredObsCodes = [];
desiredObsBands = [];
readLLI  = 1;
readSS   = 1;

[GNSS_obs, GNSS_LLI, GNSS_SS, GNSS_SVs, time_epochs, nepochs, GNSSsystems,...
    obsCodes, approxPosition, max_sat, tInterval, markerName, rinexVersion, recType, timeSystem, leapSec, gnssType,...
    rinexProgr, rinexDate, antDelta, tFirstObs, tLastObs, clockOffsetsON, GLO_Slot2ChannelMap, success] = ...
    readRinexObs304(filename,readSS,readLLI,includeAllGNSSsystems,includeAllObsCodes, desiredGNSSsystems,... 
    desiredObsCodes, desiredObsBands);

 % changing working directory back
 cd(current_dir)

%% UTEN BDS
% Store current directory
current_dir = pwd;



%data dir relative to readRinexObs304_dirpath 

data_dir   = "..\..\Data\readRinexObs304\Obs_files\PH_RINEX-Obs-filer";
filename    = "OPEC_20220010000_BDS_LESES_IKKE.obs";
filename = append(data_dir, '\', filename);


% changing working directory to readRinexObs304
cd(readRinexObs304_dirpath)

includeAllGNSSsystems = 1;
includeAllObsCodes = 1;
desiredGNSSsystems = [];
desiredObsCodes = [];
desiredObsBands = [];
readLLI  = 1;
readSS   = 1;

[GNSS_obs, GNSS_LLI, GNSS_SS, GNSS_SVs, time_epochs, nepochs, GNSSsystems,...
    obsCodes, approxPosition, max_sat, tInterval, markerName, rinexVersion, recType, timeSystem, leapSec, gnssType,...
    rinexProgr, rinexDate, antDelta, tFirstObs, tLastObs, clockOffsetsON, GLO_Slot2ChannelMap, success] = ...
    readRinexObs304(filename,readSS,readLLI,includeAllGNSSsystems,includeAllObsCodes, desiredGNSSsystems,... 
    desiredObsCodes, desiredObsBands);

 % changing working directory back
 cd(current_dir)