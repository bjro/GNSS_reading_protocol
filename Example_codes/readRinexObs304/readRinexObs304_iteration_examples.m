% The following script illustrates a varyity of ways in which a user may
% automatedly itterate through the data produced from readRinexObs304 to execute
% a varyity of computation. One such example is the computation of linear
% combinations.

% This script uses a test observation file (test_file_obs.rnx) that has been cropped to a 10
% minute observation period. GPS, GLONASS, Galieo and Beidou are all part
% of this observation file. Note that this observation file does
% contain encrypted P observation codes.

%%
% Store current directory
current_dir = pwd;

readRinexObs304_dirpath = "..\..\readRinexObs304\readRinexObs304_sourcecode";

%data dir relative to readRinexObs304_dirpath 
data_dir   = "..\..\Data";
filename    = "test_obs_file.rnx";
filename = append(data_dir, '\', filename);

%% read all GNSS systems, code and phase observation types and all bands. Don't read SS and LLI

% changing working directory to readRinexObs304
cd(readRinexObs304_dirpath)

includeAllGNSSsystems = 1;
includeAllObsCodes = 0;
desiredGNSSsystems = [];
desiredObsCodes = ["C", "L"];
desiredObsBands = [1,2,3,4,5,6,7,8,9];
readLLI  = 0;
readSS   = 0;

[GNSS_obs, GNSS_LLI, GNSS_SS, GNSS_SVs, time_epochs, nepochs, GNSSsystems,...
    obsCodes, approxPosition, max_sat, tInterval, markerName, rinexVersion, recType, timeSystem, leapSec, gnssType,...
    rinexProgr, rinexDate, antDelta, tFirstObs, tLastObs, clockOffsetsON, GLO_Slot2ChannelMap, success] = ...
    readRinexObs304(filename,readSS,readLLI,includeAllGNSSsystems,includeAllObsCodes, desiredGNSSsystems,... 
    desiredObsCodes, desiredObsBands);

%% Iterate through all GNSS systems. For each epochs, display code and phase observations

% map container to map from GNSS system code to actual name
GNSSnames = containers.Map(["G","R","C","E"], ["GPS", "GLONASS", "Beidou", "Galileo"]);
nGNSSsystems = length(GNSSsystems);
for sysIndex = 1:nGNSSsystems
   % get current GNSS system and print name
   sys = GNSSsystems{sysIndex};
   fprintf('\nCurrently showing %s observations\n\n', GNSSnames(sys))
   
   % get list of obs codes available for current GNSS system
   obsCodes_current_sys = obsCodes{sysIndex};
   n_obsCodes_current_sys = length(obsCodes_current_sys);
   % iterate through all epoch for the current GNSS system
   for epoch =1:nepochs
      fprintf('\nEpoch: %d\n\n', epoch)
      % get number of sat for current GNSS system that have observations
      % this epoch
      n_sat = GNSS_SVs{sysIndex}(epoch, 1);
      
      % get sat IDs of current GNSS system with observation this epoch 
      SVs = GNSS_SVs{sysIndex}(epoch, 2:n_sat+1);
      
      %iterate through all satelites of current GNSS system this epoch
      for sat = 1:n_sat
         SV = SVs(sat);
         fprintf('\n%s\n', GNSSnames(sys))
         fprintf('PRN: %d\n\n', SV)
         % iterate through all obscodes of current GNSS system
         for obsIndex = 1:n_obsCodes_current_sys
            % get current obsCode and obs
            obsCode = obsCodes_current_sys{obsIndex};
            obs = GNSS_obs{sysIndex}(SV, obsIndex, epoch);
            
            % display observation if it is not 0
            % if it is 0, then there is no observation of this type for
            % this sattelite this epoch
            if obs~=0
               fprintf('%s: %f\n', obsCode, obs);
            end
         end
      end
   end
end


%% Iterate through only GPS and GLONASS. For the first 5 epochs, display C1C and C2C observations if present

desiredGNSS_systems = ["G", "R"];
desiredObsCodes = ["C1C", "C2C"];
n_desiredObsCodes = length(desiredObsCodes);
n_epochs_to_display = 5;

% map container to map from GNSS system code to actual name
GNSSnames = containers.Map(["G","R","C","E"], ["GPS", "GLONASS", "Beidou", "Galileo"]);
nGNSSsystems = length(desiredGNSS_systems);

for k = 1:nGNSSsystems
   %get current GNSS system
   sys = desiredGNSS_systems(k);
   
   % get current GNSS system index, return None if current sys is not in
   % data
   sysIndex = find([GNSSsystems{:}]==sys); 
   
   fprintf('\nCurrently showing %s observations\n\n', GNSSnames(sys))
   
   % get list of obs codes available for current GNSS system
   obsCodes_current_sys = obsCodes{sysIndex};
   n_obsCodes_current_sys = length(obsCodes_current_sys);
   % iterate through epochs for the current GNSS system
   for epoch =1:n_epochs_to_display
      
      fprintf('\nEpoch: %d\n\n', epoch)
      % get number of sat for current GNSS system that have observations
      % this epoch
      n_sat = GNSS_SVs{sysIndex}(epoch, 1);
      
      % get sat IDs of current GNSS system with observation this epoch 
      SVs = GNSS_SVs{sysIndex}(epoch, 2:n_sat+1);
      
      %iterate through all satelites of current GNSS system this epoch
      for sat = 1:n_sat
         SV = SVs(sat);
         fprintf('\n%s\n', GNSSnames(sys))
         fprintf('PRN: %d\n\n', SV)
         % iterate through all obscodes of current GNSS system
         for obsnum = 1:n_desiredObsCodes
            %get desired obsCode
            obsCode = desiredObsCodes(obsnum);
            % get index of current obsCode, returns None if current sat
            % does not have current desired obsCode
            obsIndex = find([obsCodes{sysIndex}]==obsCode);
            % get current obs
            obs = GNSS_obs{sysIndex}(SV, obsIndex, epoch);
            
            % display observation if it is not 0
            % if it is 0, then there is no observation of this type for
            % this sattelite this epoch
            if obs~=0
               fprintf('%s: %f\n', obsCode, obs);
            end
         end
      end
   end
end


%% Iterate through only GPS. For the first 10 epochs, compute the linear combination L1C-L2X if both L1C and L2X are present

desiredGNSS_systems = ["G"];
phase1_code = "L1C";
phase2_code = "L2X";

c = 299792458;
phase1_wavelength = c/(1575.42*10^6);
phase2_wavelength = c/(1227.60*10^6);
n_epochs_to_display = 5;

% map container to map from GNSS system code to actual name
GNSSnames = containers.Map(["G","R","C","E"], ["GPS", "GLONASS", "Beidou", "Galileo"]);
nGNSSsystems = length(desiredGNSS_systems);

for k = 1:nGNSSsystems
   %get current GNSS system
   sys = desiredGNSS_systems(k);
   
   % get current GNSS system index, return None if current sys is not in
   % data
   sysIndex = find([GNSSsystems{:}]==sys); 
   
   fprintf('\nCurrently showing %s linear combinations\n\n', GNSSnames(sys))
   
   % iterate through epochs for the current GNSS system
   for epoch =1:n_epochs_to_display
      fprintf('\nEpoch: %d\n\n', epoch)
      % get number of sat for current GNSS system that have observations
      % this epoch
      n_sat = GNSS_SVs{sysIndex}(epoch, 1);
      
      % get sat IDs of current GNSS system with observation this epoch 
      SVs = GNSS_SVs{sysIndex}(epoch, 2:n_sat+1);
      
      %iterate through all satelites of current GNSS system this epoch
      for sat = 1:n_sat
         SV = SVs(sat);
         fprintf('\n%s\n', GNSSnames(sys))
         fprintf('PRN: %d\n\n', SV)
         
         % get index of phase 1 and phase 2 obs for current sat if present
         phase1_index = ismember(obsCodes{sysIndex},phase1_code);
         phase2_index = ismember(obsCodes{sysIndex},phase2_code);
         
         % get phaseobservations anf convert from units of cycles to meters
         phase1 = GNSS_obs{sysIndex}(SV, phase1_index, epoch)*phase1_wavelength;
         phase2 = GNSS_obs{sysIndex}(SV, phase2_index, epoch)*phase2_wavelength;
         
         %check that none of the phase obs are missing
         if ~any([phase1, phase2] == 0)
            diff = phase1-phase2; 
            fprintf('%s-%s combination: %f\n', phase1_code,phase2_code, diff);
         end
      end
   end
end


%%
 % changing working directory back
 cd(current_dir)
