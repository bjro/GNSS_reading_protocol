
% This script illustrates how the input arguments of readRinexObs304 can be
% changed to get different output data.

% This script uses a test observation file (test_file_obs.rnx) that has been cropped to a 10
% minute observation period. GPS, GLONASS, Galieo and Beidou are all part
% of this observation file. Note that this observation file does 
% contain encrypted P observation codes.

%%
% Store current directory
current_dir = pwd;

readRinexObs304_dirpath = "..\..\readRinexObs304\readRinexObs304_sourcecode";

%data dir relative to readRinexObs304_dirpath 
data_dir   = "..\..\Data";
filename    = "test_obs_file.rnx";
filename = append(data_dir, '\', filename);

%% read all GNSS systems, observation types and bands. Read SS and LLI

% changing working directory to readRinexObs304
cd(readRinexObs304_dirpath)

includeAllGNSSsystems = 1;
includeAllObsCodes = 1;
desiredGNSSsystems = [];
desiredObsCodes = [];
desiredObsBands = [];
readLLI  = 1;
readSS   = 1;

[GNSS_obs, GNSS_LLI, GNSS_SS, GNSS_SVs, time_epochs, nepochs, GNSSsystems,...
    obsCodes, approxPosition, max_sat, tInterval, markerName, rinexVersion, recType, timeSystem, leapSec, gnssType,...
    rinexProgr, rinexDate, antDelta, tFirstObs, tLastObs, clockOffsetsON, GLO_Slot2ChannelMap, success] = ...
    readRinexObs304(filename,readSS,readLLI,includeAllGNSSsystems,includeAllObsCodes, desiredGNSSsystems,... 
    desiredObsCodes, desiredObsBands);

 % changing working directory back
 cd(current_dir)

%% read only GPS, with only code observation types and all bands. Don't read SS and LLI

% changing working directory to readRinexObs304
cd(readRinexObs304_dirpath)

includeAllGNSSsystems = 0;
includeAllObsCodes = 0;
desiredGNSSsystems = ["G"];
desiredObsCodes = ["C"];
desiredObsBands = [1,2,3,4,5,6,7,8,9];
readLLI  = 0;
readSS   = 0;

[GNSS_obs, GNSS_LLI, GNSS_SS, GNSS_SVs, time_epochs, nepochs, GNSSsystems,...
    obsCodes, approxPosition, max_sat, tInterval, markerName, rinexVersion, recType, timeSystem, leapSec, gnssType,...
    rinexProgr, rinexDate, antDelta, tFirstObs, tLastObs, clockOffsetsON, GLO_Slot2ChannelMap, success] = ...
    readRinexObs304(filename,readSS,readLLI,includeAllGNSSsystems,includeAllObsCodes, desiredGNSSsystems,... 
    desiredObsCodes, desiredObsBands);
 
 % changing working directory back
 cd(current_dir)
 
 %% read all GNSS systems but GPS, code and phase observation types and bands 1-5. Don't read SS and LLI

% changing working directory to readRinexObs304
cd(readRinexObs304_dirpath)

includeAllGNSSsystems = 0;
includeAllObsCodes = 0;
desiredGNSSsystems = ["R", "C", "E"];
desiredObsCodes = ["C", "L"];
desiredObsBands = [1,2,3,4,5];
readLLI  = 0;
readSS   = 0;

[GNSS_obs, GNSS_LLI, GNSS_SS, GNSS_SVs, time_epochs, nepochs, GNSSsystems,...
    obsCodes, approxPosition, max_sat, tInterval, markerName, rinexVersion, recType, timeSystem, leapSec, gnssType,...
    rinexProgr, rinexDate, antDelta, tFirstObs, tLastObs, clockOffsetsON, GLO_Slot2ChannelMap, success] = ...
    readRinexObs304(filename,readSS,readLLI,includeAllGNSSsystems,includeAllObsCodes, desiredGNSSsystems,... 
    desiredObsCodes, desiredObsBands);
 
% changing working directory back
 cd(current_dir)