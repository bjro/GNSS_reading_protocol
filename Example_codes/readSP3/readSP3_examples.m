% This script illustrates how the input arguments of readSP3 and
% accompanying scripts can be used to both data from SP3 navigation files,
% but also how to interpolate a position this data.

% This script uses a SP3 navigation files tha contain all 4 GNSS systems.

%%
%  Store current directory
current_dir = pwd;

readSP3_dirpath = "..\..\readSP3";

%data dir relative to readRinexObs304_dirpath 
data_dir   = "..\Data\SP3";

sp3_nav_filename_1    = "GBM0MGXRAP_20200900000_01D_05M_ORB.SP3";
sp3_nav_filename_1 = append(data_dir, '\', sp3_nav_filename_1);

sp3_nav_filename_2    = "GBM0MGXRAP_20200910000_01D_05M_ORB.SP3";
sp3_nav_filename_2 = append(data_dir, '\', sp3_nav_filename_2);

sp3_nav_filename_3    = "GBM0MGXRAP_20200920000_01D_05M_ORB.SP3";
sp3_nav_filename_3 = append(data_dir, '\', sp3_nav_filename_3);
%% read a single SP3 file, excluding Beidou data, do not plot overview

% changing working directory to readRinexObs304
cd(readSP3_dirpath)


plot_overview = 0;
desiredGNSSsystems = ["G", "R", "E"];

[sat_positions, epoch_dates, navGNSSsystems, nEpochs, epochInterval] = read_multiple_SP3Nav(sp3_nav_filename_1, "", "", plot_overview, desiredGNSSsystems);

 % changing working directory back
 cd(current_dir)
%% read three SP3 file, including all GNSS systems, plot overview

% changing working directory to readRinexObs304
cd(readSP3_dirpath)
plot_overview = 1;

[sat_positions, epoch_dates, navGNSSsystems, nEpochs, epochInterval] = read_multiple_SP3Nav(sp3_nav_filename_1, sp3_nav_filename_2, sp3_nav_filename_3, plot_overview);

 % changing working directory back
 cd(current_dir)
%% read two SP3 file, excluding Beidou data, do not plot overview

% changing working directory to readRinexObs304
cd(readSP3_dirpath)

plot_overview = 0;

[sat_positions, epoch_dates, navGNSSsystems, nEpochs, epochInterval] = read_multiple_SP3Nav(sp3_nav_filename_1, sp3_nav_filename_2, "", plot_overview, desiredGNSSsystems);

 % changing working directory back
 cd(current_dir)
 
 %% read three SP3 files, all GNSS systems, do not plot overview, then interpolate position of a satelite ("G01")
 
 % changing working directory to readRinexObs304
cd(readSP3_dirpath)

plot_overview = 0;

[sat_positions, epoch_dates, navGNSSsystems, nEpochs, epochInterval] = read_multiple_SP3Nav(sp3_nav_filename_1, sp3_nav_filename_2, sp3_nav_filename_3, plot_overview);


% choosing a date inside the relevent time period and moving back 2 minutes
date = epoch_dates(35,:);
date(5) = date(5) - 2;

[X, Y, Z] = interpolatePreciseOrbits2ECEF("G01", date, epoch_dates, epochInterval, nEpochs, sat_positions, navGNSSsystems);

 % changing working directory back
 cd(current_dir)
 
  %% read three SP3 files, excluding GPS, do not plot overview, then interpolate position of a satelite ("G01"), SHOULD give error
  
 
 % changing working directory to readRinexObs304
cd(readSP3_dirpath)

plot_overview = 0;
desiredGNSSsystems = ["R", "E"];

[sat_positions, epoch_dates, navGNSSsystems, nEpochs, epochInterval] = read_multiple_SP3Nav(sp3_nav_filename_1, sp3_nav_filename_2, sp3_nav_filename_3, plot_overview, desiredGNSSsystems);


% choosing a date inside the relevent time period and moving back 2 minutes
date = epoch_dates(35,:);
date(5) = date(5) - 2;

[X, Y, Z] = interpolatePreciseOrbits2ECEF("G01", date, epoch_dates, epochInterval, nEpochs, sat_positions, navGNSSsystems);

 % changing working directory back
 cd(current_dir)
 