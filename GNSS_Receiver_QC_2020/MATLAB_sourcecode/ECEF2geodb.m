function x_g = ECEF2geodb(a,b,x_e)

% Transforms (XYZ)coordinates in the fixed earth centered reference frame ECEF,
%  to geodetic (lat, lon, ellip. height) coordinates
%--------------------------------------------------------------------------------------------------------------------------

%INPUTS:

% a:    Earth semi-major axis of ellipsoid

% b:    Earth semi-minor axis of ellipsoid

% x_e:  Column vector containg ECEF coordinates, ex. [X;Y;Z]
%--------------------------------------------------------------------------------------------------------------------------

%OUTPUTS
% x_g:  Column vector containing geodetic coordinates in format [lat;lon, ellip. height]
%--------------------------------------------------------------------------------------------------------------------------

%%
X = x_e(1);
Y = x_e(2);
Z = x_e(3);
 
% ellipsoid flattening
f = (a-b)/a ;
e2 = (a^2-b^2)/a^2 ;
E2 = (a^2-b^2)/b^2 ;
p = sqrt(X^2+Y^2) ;
k = atanc(Z,(1-f)*p) ;

lon = atanc(Y,X) ;
lat = atanc(Z+E2*b*(sin(k))^3,p-e2*a*(cos(k))^3) ;

N = a/(1-e2*(sin(lat))^2)^(1/2);

h = p*cos(lat) + Z*sin(lat)-(a^2/N) ;

x_g = [lat, lon, h]';

end
