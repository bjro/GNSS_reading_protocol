function v_l = ECEF2local(x_g, x_e)

% Function that transforms vector in ECEF reference frame to geodetic/local reference
% frame
%--------------------------------------------------------------------------------------------------------------------------
% INPUTS

% x_g:      Column vector containing geodetic coordinates in format [lat;lon, ellip. height]

% x_e:      Vector in ECEF reference frame in form [X;Y;Z]
%--------------------------------------------------------------------------------------------------------------------------

% OUTPUTS
% v_l:      Column vector in local reference frame in form [East, North, Up]
%--------------------------------------------------------------------------------------------------------------------------

lat = x_g(1);
lon = x_g(2);

% Directional Cosine Matrix rotating vector from ECEF to local refence plane
Re_l = [-sin(lon),           cos(lon),          0;...
        -sin(lat)*cos(lon), -sin(lat)*sin(lon), cos(lat);...
         cos(lat)*cos(lon),  cos(lat)*sin(lon), sin(lat)];

% Transform vector
v_l = Re_l*x_e;

end
