function analysisResults = GNSS_Receiver_QC_2020(rinObsFilename, sp3NavFilenames, phaseCodeLimit, ionLimit, cutoff_elevation_angle,...
    outputDir, plotEstimates, saveWorkspace, includeResultSummary, includeCompactSummary,  includeObservationOverview, includeLLIOverview)

% Function that, through the help of other functions: 
%   
%   - reads RINEX 3 observation files
%   - reads SP3 satellite navigation files
%   - computes elevation angles of satellites for every epoch
%   - makes estimates of multipath, ionospheric delay, and ambiguity slips
%       for all signals in RINEX 3 observation file
%   - plots estimates in graphs, if user desired it to
%   - computes and stores statistics on estimates
%   - writes an output files containing results

% This function calls on a range of functions. These in turn call on
% further more functions. The function called upon directly in 
% GNSS_Receiver_QC_2020 are:

%   - readRinexObs304.m
%   - computeSatElevations.m
%   - readFrequencyOverview.m
%   - signalAnalysis.m
%   - writeOutputFile.m

%--------------------------------------------------------------------------------------------------------------------------
% INPUTS:

% rinObsFilename:           string. Path to RINEX 3 observation file

% sp3_nav_filenames:        array. each element is a string which gives the
%                           path to a SP3 navigation file. The files should
%                           be sequential and ordered from earliest to
%                           latest

% phaseCodeLimit:           critical limit that indicates cycle slip for
%                           phase-code combination. Unit: m/s. If set to 0,
%                           default value will be used

% ionLimit:                 critical limit that indicates cycle slip for
%                           the rate of change of the ionopheric delay. 
%                           Unit: m/s. If set to 0, default value will be used

% cutoff_elevation_angle    Critical cutoff angle for satellite elevation angles, degrees
%                           Estimates where satellite elevation angle
%                           is lower than cutoff are removed, so are
%                           estimated slip periods

% outputDir:                string. Path to directory where output file 
%                           should be generated. If user does not wish to 
%                           specify output directory, this variable should 
%                           be empty string, "". In this case the output file 
%                           will be generated in sub-directory inside same 
%                           directory as GNSS_Receiver_QC_2020.m

% plotEstimates:            boolean. 1 if user desires estimates to be
%                           ploted in figures. 0 otherwise

% saveWorkspace:            boolean. 1 if user desires matlab workspace to
%                           be saved after processing. Will be saved in
%                           output directory.

% includeResultSummary:     boolean. 1 if user desires output file to
%                           include more detailed overview of statistics, 
%                           including for each individual satellites. 
%                           0 otherwise

% includeCompactSummary:    booleab. 1 if user desired output file to
%                           include more compact overview og statistics.

% includeObservationOverview:     boolean. 1 if user desires output file to
%                                   include overview of obseration types observed
%                                   by each satellite. 0 otherwise
%--------------------------------------------------------------------------------------------------------------------------
% OUTPUTS:

% analysisResults:          struct. Contains results of all alalyses, for
%                           all GNSS systems.
%--------------------------------------------------------------------------------------------------------------------------


%% Control user input arguments

n_sp3_files = length(sp3NavFilenames);

if ~(isa(rinObsFilename, 'string')|| isa(rinObsFilename, 'char'))
    fprintf(['ERROR(GNSS_Receiver_QC_2020): The input variable rinObsFilename must either be of type string or char vector\n',...
        'Argument is now of type %s\n'], class(rinObsFilename))
    analysisResults = NaN;
    return
end

if ~(isa(sp3NavFilenames, 'double')|| isa(sp3NavFilenames, 'char') || isa(sp3NavFilenames, 'string'))
    fprintf(['ERROR(GNSS_Receiver_QC_2020): The input variable sp3NavFilenames must either be of type string or char vector, or an array of these\n',...
        'Argument is now of type %s\n'], class(sp3NavFilenames))
    analysisResults = NaN;
    return
end


if isa(sp3NavFilenames, 'double')
   for sp3_file = 1:n_sp3_files
      if ~(isa(sp3NavFilenames(sp3_file), 'char')|| isa(sp3NavFilenames(sp3_file), 'string'))
         fprintf(['ERROR(GNSS_Receiver_QC_2020): Each element in sp3NavFilenames must either be of type string or char vector\n',...
        'Element %d is now of type %s\n'], sp3_file, class(sp3NavFilenames(sp3_file)))
         analysisResults = NaN;
         return
      end
   end
end

if ~(isa(rinObsFilename, 'string')|| isa(rinObsFilename, 'char'))
    fprintf(['ERROR(GNSS_Receiver_QC_2020): The input variable rinObsFilename must either be of type string or char vector\n',...
        'Argument is now of type %s\n'], class(rinObsFilename))
    analysisResults = NaN;
    return
end

if ~(plotEstimates == 1  || plotEstimates == 0)
    fprintf(['ERROR(GNSS_Receiver_QC_2020): The input variable plotEstimates must either be 1 or 0.\n',...
        'Argument is now %d\n'], plotEstimates)
    analysisResults = NaN;
    return
end

if ~(includeResultSummary == 1  || includeResultSummary == 0)
    fprintf(['ERROR(GNSS_Receiver_QC_2020): The input variable includeResultSummary must either be 1 or 0.\n',...
        'Argument is now %d\n'], includeResultSummary)
    analysisResults = NaN;
    return
end

if ~(includeCompactSummary == 1  || includeCompactSummary == 0)
    fprintf(['ERROR(GNSS_Receiver_QC_2020): The input variable includeCompactSummary must either be 1 or 0.\n',...
        'Argument is now %d\n'], includeCompactSummary)
    analysisResults = NaN;
    return
end

if ~(includeObservationOverview == 1  || includeObservationOverview == 0)
    fprintf(['ERROR(GNSS_Receiver_QC_2020): The input variable includeObservationOverview must either be 1 or 0.\n',...
        'Argument is now %d\n'], includeObservationOverview)
    analysisResults = NaN;
    return
end

fid = fopen(rinObsFilename, 'r');
if fid < 0
   fprintf('ERROR(GNSS_Receiver_QC_2020): RINEX observation file can not be found. Please check that the path is correct.\n') 
   analysisResults = NaN;
   return
end
fclose(fid);

for sp3_file = 1:n_sp3_files
   fid = fopen(sp3NavFilenames(sp3_file), 'r');
   if fid < 0 
      fprintf('ERROR(GNSS_Receiver_QC_2020): SP3 Navigation file %d can not be found. Please check that the path is correct.\n', sp3_file) 
      analysisResults = NaN;
      return
   end
   fclose(fid);
   
end


%% 
% Container maps
GNSSsystemCode2Fullname = containers.Map({'G', 'R', 'E', 'C'}, {'GPS', 'GLONASS', 'Galileo', 'BeiDou'});
GNSSsystem2BandsMap = containers.Map({'GPS', 'GLONASS', 'Galileo', 'BeiDou'}, ...
    {[1, 2, 5], [1, 2, 3, 4, 6], [1, 5, 6, 7, 8], [1, 2, 5, 6, 7, 8]});

%% Read observation file

includeAllGNSSsystems   = 0;
includeAllObsCodes      = 0;
desiredObsCodes = ["C", "L"];               % code and phase observations
desiredObsBands = 1:9;                      % all carrier bands
desiredGNSSsystems = ["G", "R", "E", "C"];  % All GNSS systems. 
readSS = 1;
readLLI = includeLLIOverview;
%                                             NOTE: Must be string array, not char

% Read RINEX 3.0x observation file
[GNSS_obs, GNSS_LLI, GNSS_SS, GNSS_SVs, time_epochs, nepochs, GNSSsystems,...
    obsCodes, approxPosition, max_sat, tInterval, markerName, rinexVersion, recType, timeSystem, leapSec, gnssType,...
    rinexProgr, rinexDate, antDelta, tFirstObs, tLastObs, clockOffsetsON, GLO_Slot2ChannelMap, success] = ...
    readRinexObs304(rinObsFilename, readSS, readLLI, includeAllGNSSsystems,includeAllObsCodes, desiredGNSSsystems,... 
    desiredObsCodes, desiredObsBands);

%% Compute satellite elevation angles from SP3 files
 
sat_elevation_angles = computeSatElevations(GNSS_SVs, GNSSsystems, approxPosition,...
    nepochs, time_epochs, max_sat, sp3NavFilenames);

%% Define carrier frequencies for every GNSS system.

% Note: Carrier band numbers follow RINEX 3 convention

nGNSSsystems = length(GNSSsystems);
max_GLO_ID = 36; 

% Read frequency overview file
frequencyOverviewFilename = 'Rinex_Frequency_Overview.txt';
[frequencyOverview_temp, frequencyGNSSsystemOrder, ~] = readFrequencyOverview(frequencyOverviewFilename);

% Initialize cell for storing carrier bands for all systems
frequencyOverview = cell(1, nGNSSsystems);

% read frequenxies from overview file
for i = 1:nGNSSsystems
    GNSSsystemIndex = find([frequencyGNSSsystemOrder{:}] == GNSSsystems{i}); 
    frequencyOverview{i} = frequencyOverview_temp{GNSSsystemIndex};
end


% Change cell describing GLONASS carrier frequencies so that each satellite
% frequency is described. 
% NOTE: This uses the GLONASS frequency channel information from RINEX 3
% observation header
if ismember("R", string(GNSSsystems))
    
    GNSSsystemIndex = find([GNSSsystems{:}] == "R"); 
    
    
    GLOSatID = cell2mat(keys(GLO_Slot2ChannelMap));
    frequencyOverviewGLO = nan(9, max_GLO_ID);
    for k = 1:9
        for j = 1:max_GLO_ID
            if ismember(j, GLOSatID)
                frequencyOverviewGLO(k, j) = frequencyOverview{GNSSsystemIndex}(k,1) +... 
                GLO_Slot2ChannelMap(j) * frequencyOverview{GNSSsystemIndex}(k,2);
            end
        end
    end
    
    % store GLONASS carrier frequencies in their new structure
    frequencyOverview{GNSSsystemIndex} = frequencyOverviewGLO;
end

%% Create observation type overview

% create overview for each GNSS system. Each overview gives the observation
% types for each of the RINEX 3 convention carrier bands. As no system has 
% observations on all 9 RINEX convention bands many of these "bands" will 
% be empty.
obsCodeOverview = cell(1, nGNSSsystems);
for i = 1:nGNSSsystems
    obsCodeOverview{i} = cell(1,9);
    for j = 1:9
       obsCodeOverview{i}{j} = []; 
    end
    nCodes = length(obsCodes{i});
    for j = 1:nCodes
       code = char(obsCodes{i}(j));
       if strcmp(code(1), 'C')
          band = str2double(code(2));
          obsCodeOverview{i}{band} = [obsCodeOverview{i}{band}; string(code)];
       end
    end
end

%% Build the structure of the struct used for storing results

% initialize variable storing total number of observation codes processed
nCodes_Total = 0;

% initialize results struct
analysisResults = struct;

% create field storing number of GNSS systems processed
analysisResults.nGNSSsystems = nGNSSsystems;
% Initialize field for storing full names of GNSS systems processed
analysisResults.GNSSsystems = {};

% create stucture of results struct to be ready for the data analysis
for sys = 1:nGNSSsystems
    
    % Full name of current GNSS system
    GNSSsystemName = GNSSsystemCode2Fullname(GNSSsystems{sys});
    % include full name of current GNSS system 
    analysisResults.GNSSsystems = [analysisResults.GNSSsystems, GNSSsystemName]; 
    
    % Initialize struct for current GNSS system
    current_sys_struct = struct;
    % Initialize observationOverview field as a struct
    current_sys_struct.observationOverview = struct;
    
    % extract the possible bands of current GNSS system, example GPS: 1,2,5
    GNSSsystemPossibleBands = GNSSsystem2BandsMap(GNSSsystemName);
    nPossibleBands = length(GNSSsystemPossibleBands);
    
    % Expand observationOverview struct to include field for every
    % satellite. Each of these field is a struct. Each of these structs
    % contains 1 field for every possible band for current GNSS system.
    % These fields are all strings that describe the observation types
    % processed from this band for this satellite.
    for i =1:max_sat(sys)
        % create field for current sat. Field is struct
       current_sys_struct.observationOverview.(strcat('Sat_', string(i))) = struct;
       current_sys_struct.observationOverview.(strcat('Sat_', string(i))).Bands = [];
       for j = 1:nPossibleBands
           current_sys_struct.observationOverview.(strcat('Sat_', string(i))).n_possible_bands = nPossibleBands;
           % in sat. struct create 1 field for every possible band for this
           % system as empty string
           current_sys_struct.observationOverview.(strcat('Sat_', string(i))).(strcat('Band_', string(GNSSsystemPossibleBands(j)))) = "";
           current_sys_struct.observationOverview.(strcat('Sat_', string(i))).Bands = [current_sys_struct.observationOverview.(strcat('Sat_', string(i))).Bands, strcat("Band_",  string(GNSSsystemPossibleBands(j)))];
       end
    end
    
    % Initilize fields for current system struct
    current_sys_struct.nBands = 0;
    current_sys_struct.Bands  = {};
    
   for bandNumInd = 1:9
       % see if current system has any observations in in carrier band(bandnum)
       nCodes_currentBand = length(obsCodeOverview{sys}{bandNumInd});
       if nCodes_currentBand > 0
           %Increment number of bands for current system struct
           current_sys_struct.nBands = ...
               current_sys_struct.nBands + 1;
           % Append current band to list of band for thi system struct
           current_sys_struct.Bands = ...
               [current_sys_struct.Bands, strcat("Band_", string(bandNumInd))];
           
           % create field for this band, field struct
           current_band_struct = struct;
           
           % Store number of codes in current band
           current_band_struct.nCodes = ...
               nCodes_currentBand;
           
           % Increment total number of codes processed
           nCodes_Total = nCodes_Total + nCodes_currentBand;
           
           % Store codes for this band
           current_band_struct.Codes = ...
               cellstr(obsCodeOverview{sys}{bandNumInd});
           
           % Store current band struct as field in current system struct 
           current_sys_struct.(current_sys_struct.Bands{current_sys_struct.nBands}) = current_band_struct;
       end
   end
   % Store current systems struct as field in results struct
   analysisResults.(analysisResults.GNSSsystems{sys}) = current_sys_struct;
end


%% Execute analysis of current data, produce results and store results in results struct

% Intialize wait bar
wbar = waitbar(0, 'INFO(GNSS\\_Receiver\\_QC\\_2020): Data analysis is being executed. Please wait.');
% Initialize counter of number of codes processed so far. This is used
% mainly for waitbar
codeNum = 0;


for sys = 1:nGNSSsystems
    % Get current GNSS system code, example GPS: G
    currentGNSSsystem = GNSSsystems{sys};
    
    % Make HARD copy of current system struct
    current_sys_struct = analysisResults.(analysisResults.GNSSsystems{sys});
    % Get number of carrier bands in current system struct
    nBands = current_sys_struct.nBands;
    
    % Itterate through Bands in system struct. 
    % NOTE variable "bandNumInd" is NOT the carrier band number, but the 
    %   index of that band in this system struct  
    for bandNumInd = 1:nBands
        % Make HARD copy of current band struct
        current_band_struct = current_sys_struct.(current_sys_struct.Bands{bandNumInd});
        % Get number of codes for current band struct
        nCodes = current_band_struct.nCodes;
        % Get current band full name
        currentBandName = current_sys_struct.Bands{bandNumInd};
        
        % For each code pseudorange observation in current band struct,
        % execute analysis once with every other signal in othe band to
        % create linear combination. The analysis with the most estimates
        % is the analysis that is stored.
       for i = 1:nCodes
           % Get code(range) and phase obervation codes
           range1_Code = current_band_struct.Codes{i};
           phase1_Code = "L"+extractAfter(range1_Code, 1);
           
           % Increment code counter and update waitbar
           codeNum = codeNum + 1;
           msg = sprintf(['INFO(GNSS\\_Receiver\\_QC\\_2020): Data analysis is being executed. Please wait.\nCurrently processing ',...
               '%s %s signal'], GNSSsystemCode2Fullname(currentGNSSsystem), range1_Code);
           waitbar(codeNum/nCodes_Total, wbar, msg);
           
           % Check if phase observation matching code observation has been
           % read from RINEX 3 observation file
           if ismember(phase1_Code, obsCodes{sys})
               % Initialize variable storing the best number for estimates
               % for the different analysis on current code
               best_nEstimates = 0;
               best_currentStats = NaN;
               
               % Itterate through the codes in the other bands to execute
               % analysis with them and current range1 code
               for secondBandnum = 1:nBands
                   % Disregard observation code in same carrier band as
                   % current range1 observation
                 if secondBandnum ~= bandNumInd
                     % Make HARD copy of the other band struct
                     other_band_struct = current_sys_struct.(current_sys_struct.Bands{secondBandnum});
                     % Get number of codes in other band struct
                     nCodesOtherBand = other_band_struct.nCodes; 
                     
                     % Itterate through codes in other band
                     for k = 1:nCodesOtherBand
                         % Get code(range) and phase obsertion codes from
                         % the other band
                        range2_Code = other_band_struct.Codes{k};
                        phase2_Code = "L"+extractAfter(range2_Code, 1);
                        
                        % Check if phase2 observation was read from RINEX 3
                        % observtaion file
                        if ismember(phase2_Code, obsCodes{sys})
                        
                    
                        % Execute the analysis of current combination of
                        % observations. Return statistics on analysis
                        
                        if isnan(GNSS_LLI)
                           LLI = GNSS_LLI;
                        else
                           LLI = GNSS_LLI{sys};
                        end
                        
                        [currentStats, success] = signalAnalysis(currentGNSSsystem, range1_Code, range2_Code, GNSSsystems, frequencyOverview, nepochs, tInterval, ...
                        max_sat(sys), GNSS_SVs{sys}, obsCodes{sys}, GNSS_obs{sys}, LLI, sat_elevation_angles{sys}, phaseCodeLimit, ionLimit, cutoff_elevation_angle);

                        if ~success
                            return
                        end
                        
                        % Get number of estimates produced from analysis
                        current_nEstimates = currentStats.nEstimates;
                        
                        % Check if current analysis has more estimate than
                        % previous
                        if current_nEstimates > best_nEstimates
                           % store current analysis results as "best so far"
                           best_nEstimates = current_nEstimates;
                           best_range1 = range1_Code;
                           best_range2 = range2_Code;
                           best_currentStats = currentStats;
                        end
                        % if phase2 observation is not read from RINEX 3
                        % observation file
                        else
                            fprintf(['INFO(GNSS_Receiver_QC_2020): %s code exists in RINEX observation file, but not %s\n',...
                                'Linear combinations using this signal is not used.\n\n'], ...
                                range2_Code, phase2_Code);
                            % remove range1 observation struct from other
                            % band struct, as it can not be used later
                            other_band_struct.Codes(ismember(other_band_struct.Codes, range2_Code)) = [];
                            % deincrement numbe rof codes in otehr band
                            % struct
                            other_band_struct.nCodes = other_band_struct.nCodes - 1; 
                            
                            % replace the, now altered, hard copy of other
                            % band struct in its original place in system struct
                            current_sys_struct.(current_sys_struct.Bands{secondBandnum}) = other_band_struct;
                        end
                     end % end of range2 code itteration
                 end 
              end % end of second band itteration
              
              % Display which analysis is the best(REMOVE LATER)
              %fprintf('%s %s Winner: %s\n\n', currentGNSSsystem, best_range1, best_range2)
              
              % Store best analysis result struct in current band struct
              current_code_struct = best_currentStats;
              
              
              % For every satellite that had an observation of range1, it
              % is stored in an overview. Hence the user can get overview
              % of which satellites have transmitted which observations
              
              % number of satellites for current system, observation or no
              nSat = length(current_code_struct.range1_slip_distribution_per_sat);
              for sat = 1:nSat
                  % if current satellite had observation of range1 code
                 if current_code_struct.n_range1_obs_per_sat(sat)>0
                     % Name of satellite struct
                     satCode = strcat('Sat_', string(sat));
                     
                     % check that code has not been added to list by fault
                     if ~contains(current_sys_struct.observationOverview.(satCode).(currentBandName), current_code_struct.range1_Code)
                         % Add current range1 code to string of codes for
                         % current stallite, sorted into bands
                         if strcmp(current_sys_struct.observationOverview.(satCode).(currentBandName), "")
                             current_sys_struct.observationOverview.(satCode).(currentBandName) = ...
                                strcat(current_sys_struct.observationOverview.(satCode).(currentBandName), current_code_struct.range1_Code);
                         else
                             current_sys_struct.observationOverview.(satCode).(currentBandName) = ...
                                strcat(current_sys_struct.observationOverview.(satCode).(currentBandName), ', ', current_code_struct.range1_Code);
                         end
                     end
                 end
              end
              
              
              % If plotEstimates boolean is 1, plot estimates from best 
              % analysis and store figures
              if plotEstimates
                  
                  % if user has not specified an output directory, set
                  % output directory to "Output_Files" 
                 if isempty(outputDir)
                    outputDir = 'Output_Files';
                 end
                  
                  % Unless output directory already exists, create output
                  % directory
                 if ~exist(outputDir, 'dir')
                    mkdir(outputDir)
                 end
                 
                 % Unless graph directory already exists, create directory
                 graphDir = strcat(outputDir,'/Graphs'); 
                 if ~exist(graphDir, 'dir')
                    mkdir(graphDir)
                 end
                 
                 % plot and save graphs
                  plotResults(current_code_struct.ion_delay_phase1, current_code_struct.multipath_range1, ...
                      current_code_struct.sat_elevation_angles, tInterval, currentGNSSsystem, ...
                      current_code_struct.range1_Code, current_code_struct.range2_Code, ...
                      current_code_struct.phase1_Code, current_code_struct.phase2_Code, graphDir)
              end % end of plotting graph section
             
              % place the current
              % code struct in its original place in current band struct
              current_band_struct.(range1_Code) = current_code_struct;
              
           else
               % if phase1 observation is not read from RINEX 3
               % observation file
               fprintf(['INFO(GNSS_Receiver_QC_2020): %s code exists in RINEX observation file, but not %s\n',...
                                'Linear combination using this signal is not used.\n\n'], ...
                   range1_Code, phase1_Code);
               current_band_struct.Codes(ismember(current_band_struct.Codes, range1_Code)) = [];
               current_band_struct.nCodes = current_band_struct.nCodes - 1; 
           end
       end % end of range1 itteration
       
       % replace the, now altered, hard copy of current
       % band struct in its original place in system struct
       current_sys_struct.(current_sys_struct.Bands{bandNumInd}) = current_band_struct;
       
    end
    
    % replace the, now altered, hard copy of current
    % system struct in its original place in results struct
    analysisResults.(analysisResults.GNSSsystems{sys}) = current_sys_struct;
end

% Store information needed for output file in result struct
rinex_obs_filename = strsplit(rinObsFilename, '/');
rinex_obs_filename = rinex_obs_filename{end};

analysisResults.ExtraOutputInfo  = struct;
analysisResults.ExtraOutputInfo.rinex_obs_filename  = rinex_obs_filename;
analysisResults.ExtraOutputInfo.markerName          = markerName;
analysisResults.ExtraOutputInfo.rinexVersion        = rinexVersion;
analysisResults.ExtraOutputInfo.rinexProgr          = rinexProgr;
analysisResults.ExtraOutputInfo.recType             = recType;
analysisResults.ExtraOutputInfo.tFirstObs           = tFirstObs;
analysisResults.ExtraOutputInfo.tLastObs            = tLastObs;
analysisResults.ExtraOutputInfo.tInterval           = tInterval;
analysisResults.ExtraOutputInfo.GLO_Slot2ChannelMap = GLO_Slot2ChannelMap;
analysisResults.ExtraOutputInfo.nEpochs = nepochs;

% store default limits or user set limits in struct
if phaseCodeLimit == 0
    analysisResults.ExtraOutputInfo.phaseCodeLimit  = 4/60*100;
else
    analysisResults.ExtraOutputInfo.phaseCodeLimit  = phaseCodeLimit;
end

if ionLimit == 0
    analysisResults.ExtraOutputInfo.ionLimit  = 4/60;
else
    analysisResults.ExtraOutputInfo.ionLimit  = ionLimit;
end



% compute number of receiver clock jumps and store
[nClockJumps, meanClockJumpInterval, stdClockJumpInterval] = detectClockJumps(GNSS_obs, nGNSSsystems, obsCodes, time_epochs, tInterval);
analysisResults.ExtraOutputInfo.nClockJumps = nClockJumps;

analysisResults.ExtraOutputInfo.meanClockJumpInterval = meanClockJumpInterval;
analysisResults.ExtraOutputInfo.stdClockJumpInterval = stdClockJumpInterval;
analysisResults.ExtraOutputInfo.nEpochs = nepochs;

fprintf('INFO(GNSS_Receiver_QC_2020): Analysis complete!\n')

if saveWorkspace
    
     msg = sprintf('INFO(GNSS\\_Receiver\\_QC\\_2020): MATLAB workspace is being saved. Please wait.');
     waitbar(1, wbar, msg);

    % if user has not specified an output directory, set
    % output directory to "Output_Files" 
     if isempty(outputDir)
        outputDir = 'Output_Files';
     end

      % Unless output directory already exists, create output
      % directory
     if ~exist(outputDir, 'dir')
        mkdir(outputDir)
     end

     % Unless graph directory already exists, create directory

    % save workspace
    workspaceFilename = 'Analysis_Workspace';
    FullWorkspacePath = fullfile(outputDir, workspaceFilename);
    save(FullWorkspacePath, 'analysisResults');
    
    disp('INFO(GNSS_Receiver_QC_2020): MATLAB workspace has been saved')
end
% close wait bar
close(wbar)

%% Export result data to csv file

% if export_data
%    [~, outputFilename, ~] = fileparts(rinObsFilename);
%    outputFilename = append(outputFilename,'_Data_Output.csv');
%    
%    %exportData(analysisResults, rinObsFilename, outputDir)
% end




%% Create output file

[~, outputFilename, ~] = fileparts(rinObsFilename);
outputFilename = append(outputFilename,'_Report.txt');


writeOutputFile(outputFilename, outputDir, analysisResults, includeResultSummary, includeCompactSummary, includeObservationOverview, includeLLIOverview)
disp('INFO(GNSS_Receiver_QC_2020): Output file has been written.')
end % end function
