GNSS Receiver Quality Check 2020 Data Output Header File
Software version: 1.00
Software developed by Bjørn-Eirik Roald
Norwegian University of Life Sciences(NMBU)

METADATA
* rinex_obs_filename:C:\Users\bjro\OneDrive - Norwegian University of Life Sciences\Documents\Faktisk_documents\Pliktarbeid\GMGD320\H21\Data\Statisk\Topcon\320m3010.21o
* rinexVersion:3.04
* rinexProgr:TPS2RIN 11.5
* markerName:320maaling2610_20211028_105745
* recType:TPS HIPER_VR        
** tFirstObs:2021/10/28 8:58:4.00 
** tLastObs:2021/10/28 11:1:41.00 
*** tInterval:1
*** nClockJumps:6
*** meanClockJumpInterval:1.348400e+03
*** stdClockJumpInterval:1.219836e+01
*** ionLimit: 0.067
*** phaseCodeLimit: 6.667
**** GLONASS Channels:1/1,2/-4,3/5,4/6,10/-7,12/-1,13/-2,17/4,18/-3,19/3,20/2,24/2
METADATA ENDS HERE

DATA FILE 1 DESCRIPTION

GNSS systems:GPS,GLONASS,Galileo,BeiDou

* GPS
Bands:Band_1,Band_2

** Band_1
Codes:C1C,C1W

*** C1C
range1_observations:1,36
phase1_observations:37,72
ion_delay_phase1:73,108
multipath_range1:109,144
sat_elevation_angles:145,180
C1C ENDS HERE

*** C1W
range1_observations:181,216
phase1_observations:217,252
ion_delay_phase1:253,288
multipath_range1:289,324
sat_elevation_angles:325,360
C1W ENDS HERE
Band_1 ENDS HERE

** Band_2
Codes:C2W

*** C2W
range1_observations:361,396
phase1_observations:397,432
ion_delay_phase1:433,468
multipath_range1:469,504
sat_elevation_angles:505,540
C2W ENDS HERE
Band_2 ENDS HERE
GPS ENDS HERE

* GLONASS
Bands:Band_1,Band_2

** Band_1
Codes:C1C

*** C1C
range1_observations:541,576
phase1_observations:577,612
ion_delay_phase1:613,648
multipath_range1:649,684
sat_elevation_angles:685,720
C1C ENDS HERE
Band_1 ENDS HERE

** Band_2
Codes:C2C

*** C2C
range1_observations:721,756
phase1_observations:757,792
ion_delay_phase1:793,828
multipath_range1:829,864
sat_elevation_angles:865,900
C2C ENDS HERE
Band_2 ENDS HERE
GLONASS ENDS HERE

* Galileo
Bands:Band_1,Band_5,Band_7

** Band_1
Codes:C1X

*** C1X
range1_observations:901,936
phase1_observations:937,972
ion_delay_phase1:973,1008
multipath_range1:1009,1044
sat_elevation_angles:1045,1080
C1X ENDS HERE
Band_1 ENDS HERE

** Band_5
Codes:C5X

*** C5X
range1_observations:1081,1116
phase1_observations:1117,1152
ion_delay_phase1:1153,1188
multipath_range1:1189,1224
sat_elevation_angles:1225,1260
C5X ENDS HERE
Band_5 ENDS HERE

** Band_7
Codes:C7X

*** C7X
range1_observations:1261,1296
phase1_observations:1297,1332
ion_delay_phase1:1333,1368
multipath_range1:1369,1404
sat_elevation_angles:1405,1440
C7X ENDS HERE
Band_7 ENDS HERE
Galileo ENDS HERE

* BeiDou
Bands:Band_2,Band_7

** Band_2
Codes:C2X

*** C2X
range1_observations:1441,1500
phase1_observations:1501,1560
ion_delay_phase1:1561,1620
multipath_range1:1621,1680
sat_elevation_angles:1681,1740
C2X ENDS HERE
Band_2 ENDS HERE

** Band_7
Codes:C7X

*** C7X
range1_observations:1741,1800
phase1_observations:1801,1860
ion_delay_phase1:1861,1920
multipath_range1:1921,1980
sat_elevation_angles:1981,2040
C7X ENDS HERE
Band_7 ENDS HERE
BeiDou ENDS HERE
DATA FILE 1 DESCRIPTION ENDS HERE

DATA FILE 2 DESCRIPTION


* GPS
Bands:Band_1,Band_2

** Band_1
Codes:C1C,C1W

*** C1C
mean_multipath_range1_overall:1,1,1
rms_multipath_range1_averaged:1,2,2
mean_ion_delay_phase1_overall:1,3,3
nEstimates:1,4,4
nRange1Obs:1,5,5
elevation_weighted_average_rms_multipath_range1:1,6,6
range1/phase1/range2/phase2 Codes:3,7,10
mean_multipath_range1_satellitewise:36,11,11
rms_multipath_range1_satellitewise:36,12,12
mean_ion_delay_phase1_satellitewise:36,13,13
mean_sat_elevation_angles:36,14,14
nEstimates_per_sat:36,15,15
n_range1_obs_per_sat:36,16,16
elevation_weighted_rms_multipath_range1_satellitewise:36,17,17
range1_slip_distribution:8,18,18
LLI_slip_distribution:8,19,19
slip_distribution_LLI_fusion:8,20,20
range1_slip_distribution_per_sat:8,21,56
LLI_slip_distribution_per_sat:8,57,92
slip_distribution_per_sat_LLI_fusion:8,93,128
C1C ENDS HERE

*** C1W
mean_multipath_range1_overall:1,129,129
rms_multipath_range1_averaged:1,130,130
mean_ion_delay_phase1_overall:1,131,131
nEstimates:1,132,132
nRange1Obs:1,133,133
elevation_weighted_average_rms_multipath_range1:1,134,134
range1/phase1/range2/phase2 Codes:3,135,138
mean_multipath_range1_satellitewise:36,139,139
rms_multipath_range1_satellitewise:36,140,140
mean_ion_delay_phase1_satellitewise:36,141,141
mean_sat_elevation_angles:36,142,142
nEstimates_per_sat:36,143,143
n_range1_obs_per_sat:36,144,144
elevation_weighted_rms_multipath_range1_satellitewise:36,145,145
range1_slip_distribution:8,146,146
LLI_slip_distribution:8,147,147
slip_distribution_LLI_fusion:8,148,148
range1_slip_distribution_per_sat:8,149,184
LLI_slip_distribution_per_sat:8,185,220
slip_distribution_per_sat_LLI_fusion:8,221,256
C1W ENDS HERE
Band_1 ENDS HERE

** Band_2
Codes:C2W

*** C2W
mean_multipath_range1_overall:1,257,257
rms_multipath_range1_averaged:1,258,258
mean_ion_delay_phase1_overall:1,259,259
nEstimates:1,260,260
nRange1Obs:1,261,261
elevation_weighted_average_rms_multipath_range1:1,262,262
range1/phase1/range2/phase2 Codes:3,263,266
mean_multipath_range1_satellitewise:36,267,267
rms_multipath_range1_satellitewise:36,268,268
mean_ion_delay_phase1_satellitewise:36,269,269
mean_sat_elevation_angles:36,270,270
nEstimates_per_sat:36,271,271
n_range1_obs_per_sat:36,272,272
elevation_weighted_rms_multipath_range1_satellitewise:36,273,273
range1_slip_distribution:8,274,274
LLI_slip_distribution:8,275,275
slip_distribution_LLI_fusion:8,276,276
range1_slip_distribution_per_sat:8,277,312
LLI_slip_distribution_per_sat:8,313,348
slip_distribution_per_sat_LLI_fusion:8,349,384
C2W ENDS HERE
Band_2 ENDS HERE
GPS ENDS HERE

* GLONASS
Bands:Band_1,Band_2

** Band_1
Codes:C1C

*** C1C
mean_multipath_range1_overall:1,385,385
rms_multipath_range1_averaged:1,386,386
mean_ion_delay_phase1_overall:1,387,387
nEstimates:1,388,388
nRange1Obs:1,389,389
elevation_weighted_average_rms_multipath_range1:1,390,390
range1/phase1/range2/phase2 Codes:3,391,394
mean_multipath_range1_satellitewise:36,395,395
rms_multipath_range1_satellitewise:36,396,396
mean_ion_delay_phase1_satellitewise:36,397,397
mean_sat_elevation_angles:36,398,398
nEstimates_per_sat:36,399,399
n_range1_obs_per_sat:36,400,400
elevation_weighted_rms_multipath_range1_satellitewise:36,401,401
range1_slip_distribution:8,402,402
LLI_slip_distribution:8,403,403
slip_distribution_LLI_fusion:8,404,404
range1_slip_distribution_per_sat:8,405,440
LLI_slip_distribution_per_sat:8,441,476
slip_distribution_per_sat_LLI_fusion:8,477,512
C1C ENDS HERE
Band_1 ENDS HERE

** Band_2
Codes:C2C

*** C2C
mean_multipath_range1_overall:1,513,513
rms_multipath_range1_averaged:1,514,514
mean_ion_delay_phase1_overall:1,515,515
nEstimates:1,516,516
nRange1Obs:1,517,517
elevation_weighted_average_rms_multipath_range1:1,518,518
range1/phase1/range2/phase2 Codes:3,519,522
mean_multipath_range1_satellitewise:36,523,523
rms_multipath_range1_satellitewise:36,524,524
mean_ion_delay_phase1_satellitewise:36,525,525
mean_sat_elevation_angles:36,526,526
nEstimates_per_sat:36,527,527
n_range1_obs_per_sat:36,528,528
elevation_weighted_rms_multipath_range1_satellitewise:36,529,529
range1_slip_distribution:8,530,530
LLI_slip_distribution:8,531,531
slip_distribution_LLI_fusion:8,532,532
range1_slip_distribution_per_sat:8,533,568
LLI_slip_distribution_per_sat:8,569,604
slip_distribution_per_sat_LLI_fusion:8,605,640
C2C ENDS HERE
Band_2 ENDS HERE
GLONASS ENDS HERE

* Galileo
Bands:Band_1,Band_5,Band_7

** Band_1
Codes:C1X

*** C1X
mean_multipath_range1_overall:1,641,641
rms_multipath_range1_averaged:1,642,642
mean_ion_delay_phase1_overall:1,643,643
nEstimates:1,644,644
nRange1Obs:1,645,645
elevation_weighted_average_rms_multipath_range1:1,646,646
range1/phase1/range2/phase2 Codes:3,647,650
mean_multipath_range1_satellitewise:36,651,651
rms_multipath_range1_satellitewise:36,652,652
mean_ion_delay_phase1_satellitewise:36,653,653
mean_sat_elevation_angles:36,654,654
nEstimates_per_sat:36,655,655
n_range1_obs_per_sat:36,656,656
elevation_weighted_rms_multipath_range1_satellitewise:36,657,657
range1_slip_distribution:8,658,658
LLI_slip_distribution:8,659,659
slip_distribution_LLI_fusion:8,660,660
range1_slip_distribution_per_sat:8,661,696
LLI_slip_distribution_per_sat:8,697,732
slip_distribution_per_sat_LLI_fusion:8,733,768
C1X ENDS HERE
Band_1 ENDS HERE

** Band_5
Codes:C5X

*** C5X
mean_multipath_range1_overall:1,769,769
rms_multipath_range1_averaged:1,770,770
mean_ion_delay_phase1_overall:1,771,771
nEstimates:1,772,772
nRange1Obs:1,773,773
elevation_weighted_average_rms_multipath_range1:1,774,774
range1/phase1/range2/phase2 Codes:3,775,778
mean_multipath_range1_satellitewise:36,779,779
rms_multipath_range1_satellitewise:36,780,780
mean_ion_delay_phase1_satellitewise:36,781,781
mean_sat_elevation_angles:36,782,782
nEstimates_per_sat:36,783,783
n_range1_obs_per_sat:36,784,784
elevation_weighted_rms_multipath_range1_satellitewise:36,785,785
range1_slip_distribution:8,786,786
LLI_slip_distribution:8,787,787
slip_distribution_LLI_fusion:8,788,788
range1_slip_distribution_per_sat:8,789,824
LLI_slip_distribution_per_sat:8,825,860
slip_distribution_per_sat_LLI_fusion:8,861,896
C5X ENDS HERE
Band_5 ENDS HERE

** Band_7
Codes:C7X

*** C7X
mean_multipath_range1_overall:1,897,897
rms_multipath_range1_averaged:1,898,898
mean_ion_delay_phase1_overall:1,899,899
nEstimates:1,900,900
nRange1Obs:1,901,901
elevation_weighted_average_rms_multipath_range1:1,902,902
range1/phase1/range2/phase2 Codes:3,903,906
mean_multipath_range1_satellitewise:36,907,907
rms_multipath_range1_satellitewise:36,908,908
mean_ion_delay_phase1_satellitewise:36,909,909
mean_sat_elevation_angles:36,910,910
nEstimates_per_sat:36,911,911
n_range1_obs_per_sat:36,912,912
elevation_weighted_rms_multipath_range1_satellitewise:36,913,913
range1_slip_distribution:8,914,914
LLI_slip_distribution:8,915,915
slip_distribution_LLI_fusion:8,916,916
range1_slip_distribution_per_sat:8,917,952
LLI_slip_distribution_per_sat:8,953,988
slip_distribution_per_sat_LLI_fusion:8,989,1024
C7X ENDS HERE
Band_7 ENDS HERE
Galileo ENDS HERE

* BeiDou
Bands:Band_2,Band_7

** Band_2
Codes:C2X

*** C2X
mean_multipath_range1_overall:1,1025,1025
rms_multipath_range1_averaged:1,1026,1026
mean_ion_delay_phase1_overall:1,1027,1027
nEstimates:1,1028,1028
nRange1Obs:1,1029,1029
elevation_weighted_average_rms_multipath_range1:1,1030,1030
range1/phase1/range2/phase2 Codes:3,1031,1034
mean_multipath_range1_satellitewise:60,1035,1035
rms_multipath_range1_satellitewise:60,1036,1036
mean_ion_delay_phase1_satellitewise:60,1037,1037
mean_sat_elevation_angles:60,1038,1038
nEstimates_per_sat:60,1039,1039
n_range1_obs_per_sat:60,1040,1040
elevation_weighted_rms_multipath_range1_satellitewise:60,1041,1041
range1_slip_distribution:8,1042,1042
LLI_slip_distribution:8,1043,1043
slip_distribution_LLI_fusion:8,1044,1044
range1_slip_distribution_per_sat:8,1045,1104
LLI_slip_distribution_per_sat:8,1105,1164
slip_distribution_per_sat_LLI_fusion:8,1165,1224
C2X ENDS HERE
Band_2 ENDS HERE

** Band_7
Codes:C7X

*** C7X
mean_multipath_range1_overall:1,1225,1225
rms_multipath_range1_averaged:1,1226,1226
mean_ion_delay_phase1_overall:1,1227,1227
nEstimates:1,1228,1228
nRange1Obs:1,1229,1229
elevation_weighted_average_rms_multipath_range1:1,1230,1230
range1/phase1/range2/phase2 Codes:3,1231,1234
mean_multipath_range1_satellitewise:60,1235,1235
rms_multipath_range1_satellitewise:60,1236,1236
mean_ion_delay_phase1_satellitewise:60,1237,1237
mean_sat_elevation_angles:60,1238,1238
nEstimates_per_sat:60,1239,1239
n_range1_obs_per_sat:60,1240,1240
elevation_weighted_rms_multipath_range1_satellitewise:60,1241,1241
range1_slip_distribution:8,1242,1242
LLI_slip_distribution:8,1243,1243
slip_distribution_LLI_fusion:8,1244,1244
range1_slip_distribution_per_sat:8,1245,1304
LLI_slip_distribution_per_sat:8,1305,1364
slip_distribution_per_sat_LLI_fusion:8,1365,1424
C7X ENDS HERE
Band_7 ENDS HERE
BeiDou ENDS HERE
DATA FILE 2 DESCRIPTION ENDS HERE

OBSERVATION OVERVIEW

* GPS
nSat:36

** Sat_1
Bands:Band_1,Band_2,Band_5
Band_1:C1C,C1W
Band_2:C2W
Band_5:
** Sat_2
Bands:Band_1,Band_2,Band_5
Band_1:C1C,C1W
Band_2:C2W
Band_5:
** Sat_3
Bands:Band_1,Band_2,Band_5
Band_1:
Band_2:
Band_5:
** Sat_4
Bands:Band_1,Band_2,Band_5
Band_1:C1C,C1W
Band_2:C2W
Band_5:
** Sat_5
Bands:Band_1,Band_2,Band_5
Band_1:
Band_2:
Band_5:
** Sat_6
Bands:Band_1,Band_2,Band_5
Band_1:C1C,C1W
Band_2:C2W
Band_5:
** Sat_7
Bands:Band_1,Band_2,Band_5
Band_1:C1C,C1W
Band_2:C2W
Band_5:
** Sat_8
Bands:Band_1,Band_2,Band_5
Band_1:
Band_2:
Band_5:
** Sat_9
Bands:Band_1,Band_2,Band_5
Band_1:C1C,C1W
Band_2:C2W
Band_5:
** Sat_10
Bands:Band_1,Band_2,Band_5
Band_1:
Band_2:
Band_5:
** Sat_11
Bands:Band_1,Band_2,Band_5
Band_1:
Band_2:
Band_5:
** Sat_12
Bands:Band_1,Band_2,Band_5
Band_1:C1C,C1W
Band_2:C2W
Band_5:
** Sat_13
Bands:Band_1,Band_2,Band_5
Band_1:
Band_2:
Band_5:
** Sat_14
Bands:Band_1,Band_2,Band_5
Band_1:
Band_2:
Band_5:
** Sat_15
Bands:Band_1,Band_2,Band_5
Band_1:
Band_2:
Band_5:
** Sat_16
Bands:Band_1,Band_2,Band_5
Band_1:
Band_2:
Band_5:
** Sat_17
Bands:Band_1,Band_2,Band_5
Band_1:C1C,C1W
Band_2:C2W
Band_5:
** Sat_18
Bands:Band_1,Band_2,Band_5
Band_1:
Band_2:
Band_5:
** Sat_19
Bands:Band_1,Band_2,Band_5
Band_1:C1C,C1W
Band_2:C2W
Band_5:
** Sat_20
Bands:Band_1,Band_2,Band_5
Band_1:C1C,C1W
Band_2:C2W
Band_5:
** Sat_21
Bands:Band_1,Band_2,Band_5
Band_1:C1C,C1W
Band_2:C2W
Band_5:
** Sat_22
Bands:Band_1,Band_2,Band_5
Band_1:C1C,C1W
Band_2:C2W
Band_5:
** Sat_23
Bands:Band_1,Band_2,Band_5
Band_1:
Band_2:
Band_5:
** Sat_24
Bands:Band_1,Band_2,Band_5
Band_1:
Band_2:
Band_5:
** Sat_25
Bands:Band_1,Band_2,Band_5
Band_1:C1C,C1W
Band_2:C2W
Band_5:
** Sat_26
Bands:Band_1,Band_2,Band_5
Band_1:C1C,C1W
Band_2:C2W
Band_5:
** Sat_27
Bands:Band_1,Band_2,Band_5
Band_1:
Band_2:
Band_5:
** Sat_28
Bands:Band_1,Band_2,Band_5
Band_1:
Band_2:
Band_5:
** Sat_29
Bands:Band_1,Band_2,Band_5
Band_1:
Band_2:
Band_5:
** Sat_30
Bands:Band_1,Band_2,Band_5
Band_1:
Band_2:
Band_5:
** Sat_31
Bands:Band_1,Band_2,Band_5
Band_1:C1C,C1W
Band_2:C2W
Band_5:
** Sat_32
Bands:Band_1,Band_2,Band_5
Band_1:
Band_2:
Band_5:
** Sat_33
Bands:Band_1,Band_2,Band_5
Band_1:
Band_2:
Band_5:
** Sat_34
Bands:Band_1,Band_2,Band_5
Band_1:
Band_2:
Band_5:
** Sat_35
Bands:Band_1,Band_2,Band_5
Band_1:
Band_2:
Band_5:
** Sat_36
Bands:Band_1,Band_2,Band_5
Band_1:
Band_2:
Band_5:

* GLONASS
nSat:36

** Sat_1
Bands:Band_1,Band_2,Band_3,Band_4,Band_6
Band_1:C1C
Band_2:C2C
Band_3:
Band_4:
Band_6:
** Sat_2
Bands:Band_1,Band_2,Band_3,Band_4,Band_6
Band_1:C1C
Band_2:C2C
Band_3:
Band_4:
Band_6:
** Sat_3
Bands:Band_1,Band_2,Band_3,Band_4,Band_6
Band_1:C1C
Band_2:C2C
Band_3:
Band_4:
Band_6:
** Sat_4
Bands:Band_1,Band_2,Band_3,Band_4,Band_6
Band_1:C1C
Band_2:C2C
Band_3:
Band_4:
Band_6:
** Sat_5
Bands:Band_1,Band_2,Band_3,Band_4,Band_6
Band_1:
Band_2:
Band_3:
Band_4:
Band_6:
** Sat_6
Bands:Band_1,Band_2,Band_3,Band_4,Band_6
Band_1:
Band_2:
Band_3:
Band_4:
Band_6:
** Sat_7
Bands:Band_1,Band_2,Band_3,Band_4,Band_6
Band_1:
Band_2:
Band_3:
Band_4:
Band_6:
** Sat_8
Bands:Band_1,Band_2,Band_3,Band_4,Band_6
Band_1:
Band_2:
Band_3:
Band_4:
Band_6:
** Sat_9
Bands:Band_1,Band_2,Band_3,Band_4,Band_6
Band_1:
Band_2:
Band_3:
Band_4:
Band_6:
** Sat_10
Bands:Band_1,Band_2,Band_3,Band_4,Band_6
Band_1:
Band_2:
Band_3:
Band_4:
Band_6:
** Sat_11
Bands:Band_1,Band_2,Band_3,Band_4,Band_6
Band_1:
Band_2:
Band_3:
Band_4:
Band_6:
** Sat_12
Bands:Band_1,Band_2,Band_3,Band_4,Band_6
Band_1:C1C
Band_2:C2C
Band_3:
Band_4:
Band_6:
** Sat_13
Bands:Band_1,Band_2,Band_3,Band_4,Band_6
Band_1:C1C
Band_2:C2C
Band_3:
Band_4:
Band_6:
** Sat_14
Bands:Band_1,Band_2,Band_3,Band_4,Band_6
Band_1:
Band_2:
Band_3:
Band_4:
Band_6:
** Sat_15
Bands:Band_1,Band_2,Band_3,Band_4,Band_6
Band_1:
Band_2:
Band_3:
Band_4:
Band_6:
** Sat_16
Bands:Band_1,Band_2,Band_3,Band_4,Band_6
Band_1:
Band_2:
Band_3:
Band_4:
Band_6:
** Sat_17
Bands:Band_1,Band_2,Band_3,Band_4,Band_6
Band_1:C1C
Band_2:C2C
Band_3:
Band_4:
Band_6:
** Sat_18
Bands:Band_1,Band_2,Band_3,Band_4,Band_6
Band_1:C1C
Band_2:C2C
Band_3:
Band_4:
Band_6:
** Sat_19
Bands:Band_1,Band_2,Band_3,Band_4,Band_6
Band_1:C1C
Band_2:C2C
Band_3:
Band_4:
Band_6:
** Sat_20
Bands:Band_1,Band_2,Band_3,Band_4,Band_6
Band_1:C1C
Band_2:C2C
Band_3:
Band_4:
Band_6:
** Sat_21
Bands:Band_1,Band_2,Band_3,Band_4,Band_6
Band_1:
Band_2:
Band_3:
Band_4:
Band_6:
** Sat_22
Bands:Band_1,Band_2,Band_3,Band_4,Band_6
Band_1:
Band_2:
Band_3:
Band_4:
Band_6:
** Sat_23
Bands:Band_1,Band_2,Band_3,Band_4,Band_6
Band_1:
Band_2:
Band_3:
Band_4:
Band_6:
** Sat_24
Bands:Band_1,Band_2,Band_3,Band_4,Band_6
Band_1:C1C
Band_2:C2C
Band_3:
Band_4:
Band_6:
** Sat_25
Bands:Band_1,Band_2,Band_3,Band_4,Band_6
Band_1:
Band_2:
Band_3:
Band_4:
Band_6:
** Sat_26
Bands:Band_1,Band_2,Band_3,Band_4,Band_6
Band_1:
Band_2:
Band_3:
Band_4:
Band_6:
** Sat_27
Bands:Band_1,Band_2,Band_3,Band_4,Band_6
Band_1:
Band_2:
Band_3:
Band_4:
Band_6:
** Sat_28
Bands:Band_1,Band_2,Band_3,Band_4,Band_6
Band_1:
Band_2:
Band_3:
Band_4:
Band_6:
** Sat_29
Bands:Band_1,Band_2,Band_3,Band_4,Band_6
Band_1:
Band_2:
Band_3:
Band_4:
Band_6:
** Sat_30
Bands:Band_1,Band_2,Band_3,Band_4,Band_6
Band_1:
Band_2:
Band_3:
Band_4:
Band_6:
** Sat_31
Bands:Band_1,Band_2,Band_3,Band_4,Band_6
Band_1:
Band_2:
Band_3:
Band_4:
Band_6:
** Sat_32
Bands:Band_1,Band_2,Band_3,Band_4,Band_6
Band_1:
Band_2:
Band_3:
Band_4:
Band_6:
** Sat_33
Bands:Band_1,Band_2,Band_3,Band_4,Band_6
Band_1:
Band_2:
Band_3:
Band_4:
Band_6:
** Sat_34
Bands:Band_1,Band_2,Band_3,Band_4,Band_6
Band_1:
Band_2:
Band_3:
Band_4:
Band_6:
** Sat_35
Bands:Band_1,Band_2,Band_3,Band_4,Band_6
Band_1:
Band_2:
Band_3:
Band_4:
Band_6:
** Sat_36
Bands:Band_1,Band_2,Band_3,Band_4,Band_6
Band_1:
Band_2:
Band_3:
Band_4:
Band_6:

* Galileo
nSat:36

** Sat_1
Bands:Band_1,Band_5,Band_6,Band_7,Band_8
Band_1:C1X
Band_5:C5X
Band_6:
Band_7:C7X
Band_8:
** Sat_2
Bands:Band_1,Band_5,Band_6,Band_7,Band_8
Band_1:
Band_5:
Band_6:
Band_7:
Band_8:
** Sat_3
Bands:Band_1,Band_5,Band_6,Band_7,Band_8
Band_1:C1X
Band_5:C5X
Band_6:
Band_7:C7X
Band_8:
** Sat_4
Bands:Band_1,Band_5,Band_6,Band_7,Band_8
Band_1:
Band_5:
Band_6:
Band_7:
Band_8:
** Sat_5
Bands:Band_1,Band_5,Band_6,Band_7,Band_8
Band_1:C1X
Band_5:C5X
Band_6:
Band_7:C7X
Band_8:
** Sat_6
Bands:Band_1,Band_5,Band_6,Band_7,Band_8
Band_1:
Band_5:
Band_6:
Band_7:
Band_8:
** Sat_7
Bands:Band_1,Band_5,Band_6,Band_7,Band_8
Band_1:
Band_5:
Band_6:
Band_7:
Band_8:
** Sat_8
Bands:Band_1,Band_5,Band_6,Band_7,Band_8
Band_1:C1X
Band_5:C5X
Band_6:
Band_7:C7X
Band_8:
** Sat_9
Bands:Band_1,Band_5,Band_6,Band_7,Band_8
Band_1:C1X
Band_5:C5X
Band_6:
Band_7:C7X
Band_8:
** Sat_10
Bands:Band_1,Band_5,Band_6,Band_7,Band_8
Band_1:
Band_5:
Band_6:
Band_7:
Band_8:
** Sat_11
Bands:Band_1,Band_5,Band_6,Band_7,Band_8
Band_1:
Band_5:
Band_6:
Band_7:
Band_8:
** Sat_12
Bands:Band_1,Band_5,Band_6,Band_7,Band_8
Band_1:
Band_5:
Band_6:
Band_7:
Band_8:
** Sat_13
Bands:Band_1,Band_5,Band_6,Band_7,Band_8
Band_1:C1X
Band_5:C5X
Band_6:
Band_7:C7X
Band_8:
** Sat_14
Bands:Band_1,Band_5,Band_6,Band_7,Band_8
Band_1:
Band_5:
Band_6:
Band_7:
Band_8:
** Sat_15
Bands:Band_1,Band_5,Band_6,Band_7,Band_8
Band_1:C1X
Band_5:C5X
Band_6:
Band_7:C7X
Band_8:
** Sat_16
Bands:Band_1,Band_5,Band_6,Band_7,Band_8
Band_1:
Band_5:
Band_6:
Band_7:
Band_8:
** Sat_17
Bands:Band_1,Band_5,Band_6,Band_7,Band_8
Band_1:
Band_5:
Band_6:
Band_7:
Band_8:
** Sat_18
Bands:Band_1,Band_5,Band_6,Band_7,Band_8
Band_1:
Band_5:
Band_6:
Band_7:
Band_8:
** Sat_19
Bands:Band_1,Band_5,Band_6,Band_7,Band_8
Band_1:
Band_5:
Band_6:
Band_7:
Band_8:
** Sat_20
Bands:Band_1,Band_5,Band_6,Band_7,Band_8
Band_1:
Band_5:
Band_6:
Band_7:
Band_8:
** Sat_21
Bands:Band_1,Band_5,Band_6,Band_7,Band_8
Band_1:
Band_5:
Band_6:
Band_7:
Band_8:
** Sat_22
Bands:Band_1,Band_5,Band_6,Band_7,Band_8
Band_1:
Band_5:
Band_6:
Band_7:
Band_8:
** Sat_23
Bands:Band_1,Band_5,Band_6,Band_7,Band_8
Band_1:
Band_5:
Band_6:
Band_7:
Band_8:
** Sat_24
Bands:Band_1,Band_5,Band_6,Band_7,Band_8
Band_1:C1X
Band_5:C5X
Band_6:
Band_7:C7X
Band_8:
** Sat_25
Bands:Band_1,Band_5,Band_6,Band_7,Band_8
Band_1:C1X
Band_5:C5X
Band_6:
Band_7:C7X
Band_8:
** Sat_26
Bands:Band_1,Band_5,Band_6,Band_7,Band_8
Band_1:C1X
Band_5:C5X
Band_6:
Band_7:C7X
Band_8:
** Sat_27
Bands:Band_1,Band_5,Band_6,Band_7,Band_8
Band_1:
Band_5:
Band_6:
Band_7:
Band_8:
** Sat_28
Bands:Band_1,Band_5,Band_6,Band_7,Band_8
Band_1:
Band_5:
Band_6:
Band_7:
Band_8:
** Sat_29
Bands:Band_1,Band_5,Band_6,Band_7,Band_8
Band_1:
Band_5:
Band_6:
Band_7:
Band_8:
** Sat_30
Bands:Band_1,Band_5,Band_6,Band_7,Band_8
Band_1:
Band_5:
Band_6:
Band_7:
Band_8:
** Sat_31
Bands:Band_1,Band_5,Band_6,Band_7,Band_8
Band_1:
Band_5:
Band_6:
Band_7:
Band_8:
** Sat_32
Bands:Band_1,Band_5,Band_6,Band_7,Band_8
Band_1:
Band_5:
Band_6:
Band_7:
Band_8:
** Sat_33
Bands:Band_1,Band_5,Band_6,Band_7,Band_8
Band_1:
Band_5:
Band_6:
Band_7:
Band_8:
** Sat_34
Bands:Band_1,Band_5,Band_6,Band_7,Band_8
Band_1:
Band_5:
Band_6:
Band_7:
Band_8:
** Sat_35
Bands:Band_1,Band_5,Band_6,Band_7,Band_8
Band_1:
Band_5:
Band_6:
Band_7:
Band_8:
** Sat_36
Bands:Band_1,Band_5,Band_6,Band_7,Band_8
Band_1:
Band_5:
Band_6:
Band_7:
Band_8:

* BeiDou
nSat:60

** Sat_1
Bands:Band_1,Band_2,Band_5,Band_6,Band_7,Band_8
Band_1:
Band_2:
Band_5:
Band_6:
Band_7:
Band_8:
** Sat_2
Bands:Band_1,Band_2,Band_5,Band_6,Band_7,Band_8
Band_1:
Band_2:
Band_5:
Band_6:
Band_7:
Band_8:
** Sat_3
Bands:Band_1,Band_2,Band_5,Band_6,Band_7,Band_8
Band_1:
Band_2:
Band_5:
Band_6:
Band_7:
Band_8:
** Sat_4
Bands:Band_1,Band_2,Band_5,Band_6,Band_7,Band_8
Band_1:
Band_2:
Band_5:
Band_6:
Band_7:
Band_8:
** Sat_5
Bands:Band_1,Band_2,Band_5,Band_6,Band_7,Band_8
Band_1:
Band_2:C2X
Band_5:
Band_6:
Band_7:C7X
Band_8:
** Sat_6
Bands:Band_1,Band_2,Band_5,Band_6,Band_7,Band_8
Band_1:
Band_2:C2X
Band_5:
Band_6:
Band_7:C7X
Band_8:
** Sat_7
Bands:Band_1,Band_2,Band_5,Band_6,Band_7,Band_8
Band_1:
Band_2:
Band_5:
Band_6:
Band_7:
Band_8:
** Sat_8
Bands:Band_1,Band_2,Band_5,Band_6,Band_7,Band_8
Band_1:
Band_2:
Band_5:
Band_6:
Band_7:
Band_8:
** Sat_9
Bands:Band_1,Band_2,Band_5,Band_6,Band_7,Band_8
Band_1:
Band_2:C2X
Band_5:
Band_6:
Band_7:C7X
Band_8:
** Sat_10
Bands:Band_1,Band_2,Band_5,Band_6,Band_7,Band_8
Band_1:
Band_2:
Band_5:
Band_6:
Band_7:
Band_8:
** Sat_11
Bands:Band_1,Band_2,Band_5,Band_6,Band_7,Band_8
Band_1:
Band_2:C2X
Band_5:
Band_6:
Band_7:C7X
Band_8:
** Sat_12
Bands:Band_1,Band_2,Band_5,Band_6,Band_7,Band_8
Band_1:
Band_2:C2X
Band_5:
Band_6:
Band_7:C7X
Band_8:
** Sat_13
Bands:Band_1,Band_2,Band_5,Band_6,Band_7,Band_8
Band_1:
Band_2:
Band_5:
Band_6:
Band_7:
Band_8:
** Sat_14
Bands:Band_1,Band_2,Band_5,Band_6,Band_7,Band_8
Band_1:
Band_2:C2X
Band_5:
Band_6:
Band_7:C7X
Band_8:
** Sat_15
Bands:Band_1,Band_2,Band_5,Band_6,Band_7,Band_8
Band_1:
Band_2:
Band_5:
Band_6:
Band_7:
Band_8:
** Sat_16
Bands:Band_1,Band_2,Band_5,Band_6,Band_7,Band_8
Band_1:
Band_2:
Band_5:
Band_6:
Band_7:
Band_8:
** Sat_17
Bands:Band_1,Band_2,Band_5,Band_6,Band_7,Band_8
Band_1:
Band_2:
Band_5:
Band_6:
Band_7:
Band_8:
** Sat_18
Bands:Band_1,Band_2,Band_5,Band_6,Band_7,Band_8
Band_1:
Band_2:
Band_5:
Band_6:
Band_7:
Band_8:
** Sat_19
Bands:Band_1,Band_2,Band_5,Band_6,Band_7,Band_8
Band_1:
Band_2:
Band_5:
Band_6:
Band_7:
Band_8:
** Sat_20
Bands:Band_1,Band_2,Band_5,Band_6,Band_7,Band_8
Band_1:
Band_2:
Band_5:
Band_6:
Band_7:
Band_8:
** Sat_21
Bands:Band_1,Band_2,Band_5,Band_6,Band_7,Band_8
Band_1:
Band_2:C2X
Band_5:
Band_6:
Band_7:C7X
Band_8:
** Sat_22
Bands:Band_1,Band_2,Band_5,Band_6,Band_7,Band_8
Band_1:
Band_2:C2X
Band_5:
Band_6:
Band_7:C7X
Band_8:
** Sat_23
Bands:Band_1,Band_2,Band_5,Band_6,Band_7,Band_8
Band_1:
Band_2:
Band_5:
Band_6:
Band_7:
Band_8:
** Sat_24
Bands:Band_1,Band_2,Band_5,Band_6,Band_7,Band_8
Band_1:
Band_2:
Band_5:
Band_6:
Band_7:
Band_8:
** Sat_25
Bands:Band_1,Band_2,Band_5,Band_6,Band_7,Band_8
Band_1:
Band_2:
Band_5:
Band_6:
Band_7:
Band_8:
** Sat_26
Bands:Band_1,Band_2,Band_5,Band_6,Band_7,Band_8
Band_1:
Band_2:
Band_5:
Band_6:
Band_7:
Band_8:
** Sat_27
Bands:Band_1,Band_2,Band_5,Band_6,Band_7,Band_8
Band_1:
Band_2:C2X
Band_5:
Band_6:
Band_7:
Band_8:
** Sat_28
Bands:Band_1,Band_2,Band_5,Band_6,Band_7,Band_8
Band_1:
Band_2:C2X
Band_5:
Band_6:
Band_7:C7X
Band_8:
** Sat_29
Bands:Band_1,Band_2,Band_5,Band_6,Band_7,Band_8
Band_1:
Band_2:
Band_5:
Band_6:
Band_7:
Band_8:
** Sat_30
Bands:Band_1,Band_2,Band_5,Band_6,Band_7,Band_8
Band_1:
Band_2:
Band_5:
Band_6:
Band_7:
Band_8:
** Sat_31
Bands:Band_1,Band_2,Band_5,Band_6,Band_7,Band_8
Band_1:
Band_2:
Band_5:
Band_6:
Band_7:
Band_8:
** Sat_32
Bands:Band_1,Band_2,Band_5,Band_6,Band_7,Band_8
Band_1:
Band_2:
Band_5:
Band_6:
Band_7:
Band_8:
** Sat_33
Bands:Band_1,Band_2,Band_5,Band_6,Band_7,Band_8
Band_1:
Band_2:C2X
Band_5:
Band_6:
Band_7:
Band_8:
** Sat_34
Bands:Band_1,Band_2,Band_5,Band_6,Band_7,Band_8
Band_1:
Band_2:C2X
Band_5:
Band_6:
Band_7:
Band_8:
** Sat_35
Bands:Band_1,Band_2,Band_5,Band_6,Band_7,Band_8
Band_1:
Band_2:
Band_5:
Band_6:
Band_7:
Band_8:
** Sat_36
Bands:Band_1,Band_2,Band_5,Band_6,Band_7,Band_8
Band_1:
Band_2:C2X
Band_5:
Band_6:
Band_7:
Band_8:
** Sat_37
Bands:Band_1,Band_2,Band_5,Band_6,Band_7,Band_8
Band_1:
Band_2:
Band_5:
Band_6:
Band_7:
Band_8:
** Sat_38
Bands:Band_1,Band_2,Band_5,Band_6,Band_7,Band_8
Band_1:
Band_2:
Band_5:
Band_6:
Band_7:
Band_8:
** Sat_39
Bands:Band_1,Band_2,Band_5,Band_6,Band_7,Band_8
Band_1:
Band_2:
Band_5:
Band_6:
Band_7:
Band_8:
** Sat_40
Bands:Band_1,Band_2,Band_5,Band_6,Band_7,Band_8
Band_1:
Band_2:
Band_5:
Band_6:
Band_7:
Band_8:
** Sat_41
Bands:Band_1,Band_2,Band_5,Band_6,Band_7,Band_8
Band_1:
Band_2:
Band_5:
Band_6:
Band_7:
Band_8:
** Sat_42
Bands:Band_1,Band_2,Band_5,Band_6,Band_7,Band_8
Band_1:
Band_2:
Band_5:
Band_6:
Band_7:
Band_8:
** Sat_43
Bands:Band_1,Band_2,Band_5,Band_6,Band_7,Band_8
Band_1:
Band_2:
Band_5:
Band_6:
Band_7:
Band_8:
** Sat_44
Bands:Band_1,Band_2,Band_5,Band_6,Band_7,Band_8
Band_1:
Band_2:
Band_5:
Band_6:
Band_7:
Band_8:
** Sat_45
Bands:Band_1,Band_2,Band_5,Band_6,Band_7,Band_8
Band_1:
Band_2:
Band_5:
Band_6:
Band_7:
Band_8:
** Sat_46
Bands:Band_1,Band_2,Band_5,Band_6,Band_7,Band_8
Band_1:
Band_2:
Band_5:
Band_6:
Band_7:
Band_8:
** Sat_47
Bands:Band_1,Band_2,Band_5,Band_6,Band_7,Band_8
Band_1:
Band_2:
Band_5:
Band_6:
Band_7:
Band_8:
** Sat_48
Bands:Band_1,Band_2,Band_5,Band_6,Band_7,Band_8
Band_1:
Band_2:
Band_5:
Band_6:
Band_7:
Band_8:
** Sat_49
Bands:Band_1,Band_2,Band_5,Band_6,Band_7,Band_8
Band_1:
Band_2:
Band_5:
Band_6:
Band_7:
Band_8:
** Sat_50
Bands:Band_1,Band_2,Band_5,Band_6,Band_7,Band_8
Band_1:
Band_2:
Band_5:
Band_6:
Band_7:
Band_8:
** Sat_51
Bands:Band_1,Band_2,Band_5,Band_6,Band_7,Band_8
Band_1:
Band_2:
Band_5:
Band_6:
Band_7:
Band_8:
** Sat_52
Bands:Band_1,Band_2,Band_5,Band_6,Band_7,Band_8
Band_1:
Band_2:
Band_5:
Band_6:
Band_7:
Band_8:
** Sat_53
Bands:Band_1,Band_2,Band_5,Band_6,Band_7,Band_8
Band_1:
Band_2:
Band_5:
Band_6:
Band_7:
Band_8:
** Sat_54
Bands:Band_1,Band_2,Band_5,Band_6,Band_7,Band_8
Band_1:
Band_2:
Band_5:
Band_6:
Band_7:
Band_8:
** Sat_55
Bands:Band_1,Band_2,Band_5,Band_6,Band_7,Band_8
Band_1:
Band_2:
Band_5:
Band_6:
Band_7:
Band_8:
** Sat_56
Bands:Band_1,Band_2,Band_5,Band_6,Band_7,Band_8
Band_1:
Band_2:
Band_5:
Band_6:
Band_7:
Band_8:
** Sat_57
Bands:Band_1,Band_2,Band_5,Band_6,Band_7,Band_8
Band_1:
Band_2:
Band_5:
Band_6:
Band_7:
Band_8:
** Sat_58
Bands:Band_1,Band_2,Band_5,Band_6,Band_7,Band_8
Band_1:
Band_2:
Band_5:
Band_6:
Band_7:
Band_8:
** Sat_59
Bands:Band_1,Band_2,Band_5,Band_6,Band_7,Band_8
Band_1:
Band_2:
Band_5:
Band_6:
Band_7:
Band_8:
** Sat_60
Bands:Band_1,Band_2,Band_5,Band_6,Band_7,Band_8
Band_1:
Band_2:
Band_5:
Band_6:
Band_7:
Band_8:
OBSERVATION OVERVIEW ENDS HERE
END OF FILE