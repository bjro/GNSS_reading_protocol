function sat_elevation_angles = computeSatElevations(GNSS_SVs, GNSSsystems, approxPosition,...
    nepochs, time_epochs, max_sat, sp3_nav_filenames)

% Function that computes the satellite elevation angles of all satellites
% of all GNSS systems at each epoch of the observations period.
%--------------------------------------------------------------------------------------------------------------------------
% INPUTS

% GNSS_SVs:                 Cell containing number of satellites with 
%                           observations and PRN for each satellite, 
%                           for each GNSS system

%                           GNSS_SVs{GNSSsystemIndex}(epoch,j)     
%                                       j=1: number of observed GPS satellites
%                                       j>1: PRN of observed satellites

% GNSSsystems:              cell array containing different GNSS 
%                           systems included in RINEX file. Elements are strings. 
%                           Must be either "G","R","E" or "C".  

% approxPosition:           array containing approximate position from rinex
%                           observation file header. [X, Y, Z]

% nepochs:                  number of epochs with observations in 
%                           rinex observation file.

% time_epochs:              matrix conatining gps-week and "time of week" 
%                           for each epoch
%                           time_epochs(epoch,i),   i=1: week
%                                                   i=2: time-of-week in seconds (tow)

% max_sat:                  array that stores max satellite PRN number for 
%                           each of the GNSS systems. Follows same order as GNSSsystems

% sp3_nav_filenames:        array. each element is a string which gives the
%                           path to a SP3 navigation file. The files should
%                           be sequential and ordered from earliest to
%                           latest
%--------------------------------------------------------------------------------------------------------------------------
% OUTPUTS

% sat_elevation_angles:     Cell contaning satellite elevation angles at each
%                           epoch, for each GNSS system. 
%                           sat_elevation_angles{GNSSsystemIndex}(epoch, PRN)
%--------------------------------------------------------------------------------------------------------------------------
%%
nGNSSsystems = length(GNSSsystems);
sat_elevation_angles = cell(nGNSSsystems,1);

if all(approxPosition == 0)
    fprintf(['ERROR(computeSatElevations): The approximate receiver position is missing.\n',... 
        'Please chack that APPROX POSITION XYZ is in header of Rinex file.\n',... 
        'Elevation angles will not be computed\n\n.'])
    return
end



%%

n_sp3_files = length(sp3_nav_filenames);

sat_positions = cell(n_sp3_files,1);
epoch_dates = cell(n_sp3_files,1);
navGNSSsystems = cell(n_sp3_files,1);
nEpochs = zeros(n_sp3_files,1);
epochInterval = zeros(n_sp3_files,1);

for sp3_file = 1:n_sp3_files
   [sat_positions_current_file, epoch_dates_current_file, navGNSSsystems_current_file, nEpochs_current_file, epochInterval_current_file, success] = readSP3Nav(sp3_nav_filenames(sp3_file));
   
   sat_positions{sp3_file} = sat_positions_current_file;
   epoch_dates{sp3_file} = epoch_dates_current_file;
   navGNSSsystems{sp3_file} = navGNSSsystems_current_file;
   nEpochs(sp3_file) = nEpochs_current_file;
   epochInterval(sp3_file) = epochInterval_current_file;
end

% combine data from different SP3 files
if n_sp3_files >= 2
    [sat_positions, epoch_dates, navGNSSsystems, nEpochs, epochInterval, multipleIntervals, firstFileIndices] = ...
       combineSP3Nav(n_sp3_files, sat_positions, epoch_dates, navGNSSsystems, nEpochs, epochInterval);
else
    sat_positions = sat_positions{1};
    epoch_dates = epoch_dates{1};
    navGNSSsystems = navGNSSsystems{1};
    nEpochs = nEpochs(1);
    epochInterval = epochInterval(1);
    multipleIntervals = 0;
    firstFileIndices = [1];
end


% creating a modified version of GNSS_SVs. Instead of only giving
% satellites with observations for each epoch, FirstLastObsEpochOverview
% gives every satellite who's first and last observations are before and
% after current epoch. Hence if a temporary period of time where a
% satellite does not have observations occurs the elevation angle of that
% satellite will still be computed.

FirstLastObsEpochOverview = cell(1, nGNSSsystems);


for i = 1:nGNSSsystems
   [nepochs_current_sys, max_sat_current_sys] = size(GNSS_SVs{i});  
   max_sat_current_sys = max_sat_current_sys - 1;
   FirstLastObsEpochOverview{i} = zeros(nepochs_current_sys, max_sat_current_sys);
   for PRN = 1:max_sat_current_sys
      % logical, 1 for every epoch with observation for this PRN
      dummy = sum(GNSS_SVs{i}(:, 2:end)==PRN,2);
      % find first and last epoch with observation for this PRN
      firstObsEpoch_current_sys = find(dummy==1, 1, 'first');
      lastObsEpoch_current_sys = find(dummy==1, 1, 'last');
      
      FirstLastObsEpochOverview{i}(firstObsEpoch_current_sys:lastObsEpoch_current_sys, PRN) = PRN;
   end
end


% Initialize progress bar
wbar = waitbar(0, 'INFO(computeSatElevations): Satellite elevation angles are being calculated. Please wait');

% overview of satellites without navigation data in sp3 file
satMissingData = {};

for k = 1:nGNSSsystems
    % Initialize data matrix for current GNSSsystem
   sat_elevation_angles{k} = zeros(nepochs, max_sat(k)); 
   sys = GNSSsystems{k};
   
   if ismember(sys, navGNSSsystems) 
      for epoch = 1:nepochs
          % Update progress bar 
         if mod(epoch, 500) == 0
              waitbar((epoch+nepochs*(k-1))/(nepochs*nGNSSsystems), wbar, ...
                  'INFO(computeSatElevations): Satellite elevation angles are being calculated. Please wait');
         end
          % GPS Week and time of week of current epoch
          week = time_epochs(epoch,1);
          tow  = time_epochs(epoch,2);
          % Satellites in current epoch that should have elevation computed
          SVs = nonzeros(FirstLastObsEpochOverview{k}(epoch, :));
          n_sat = length(SVs);

          for sat = 1:n_sat
              
              % Get satellite elevation angle of current sat at current epoch
              [elevation_angle, missing_nav_data] = get_elevation_angle(GNSSsystems{k}, SVs(sat), week, tow, sat_positions, nEpochs,... 
                  epoch_dates, epochInterval, navGNSSsystems, approxPosition, n_sp3_files, multipleIntervals, firstFileIndices);

              sat_elevation_angles{k}(epoch,SVs(sat)) = elevation_angle;

              if missing_nav_data
                  % combine PRN number and GNSS system char
                  SVN = strcat(GNSSsystems{k},num2str(SVs(sat)));
                  if ~any(strcmp(SVN, satMissingData))
                    satMissingData = [satMissingData, SVN]; 
                  end
              end
          end
      end 
   end
end

if ~isempty(satMissingData)
   disp(['INFO(computeSatElevations): The following satellites had missing orbit data in SP3 files.',...
   'Their elevation angles were set to 0 for these epochs']) 
    disp(satMissingData)
end

close(wbar)
disp('INFO(computeSatElevations): Satellite elevation angles have been computed')
end