function [nClockJumps, meanClockJumpInterval, stdClockJumpInterval] = detectClockJumps(GNSS_obs, nGNSSsystems, obsCodes, time_epochs, tInterval)

% Function that detects receiver clock jumps from GNSS observations. 
%--------------------------------------------------------------------------------------------------------------------------
% INPUTS

% GNSS_obs:                 cell containing a matrix for each GNSS system.
%                           Each matrix is a 3D matrix containing all 
%                           observation of current GNSS system for all epochs. 
%                           Order of obsType index is same order as in 
%                           obsCodes cell

%                           GNSS_obs{GNSSsystemIndex}(PRN, obsType, epoch)
%                                           GNSSsystemIndex: double,
%                                           1,2,...,numGNSSsystems
%                                           PRN: double
%                                           ObsType: double: 1,2,...,numObsTypes
%                                           epoch: double

% nGNSSsystems:             number of GNSS systems present

% obsCodes:                 Cell that defines the observation
%                           codes available for all GNSS system. Each cell
%                           element is another cell containing the codes for
%                           that GNSS system. Each element in this cell is a
%                           string with three-characters. The first 
%                           character (a capital letter) is an observation code 
%                           ex. "L" or "C". The second character (a digit)
%                           is a frequency code. The third character(a Capital letter)  
%                           is the attribute, ex. "P" or "X"

% time_epochs:              matrix conatining gps-week and "time of week" 
%                           for each epoch
%                           time_epochs(epoch,i),   i=1: week
%                                                   i=2: time-of-week in seconds (tow)

% tInterval:                observations interval; seconds. 
%--------------------------------------------------------------------------------------------------------------------------
% OUTPUTS:

% nClockJumps:              number of receiver clock jumps detected

% meanClockJumpInterval:    average time between receiver clock jumps,
%                           seconds

% stdClockJumpInterval:     standard deviation of receiver clock
%                           jumpintervals
%--------------------------------------------------------------------------------------------------------------------------

for i = 1:nGNSSsystems
    obsTypes = cellfun(@(x) x(1),obsCodes{i},'un',0);
    codeIndices = find(~cellfun('isempty', strfind(obsTypes,'C')));
    nCodeObs = length(codeIndices);
    
    current_obs = permute(GNSS_obs{i},[3 2 1]);
    [nepochs, ~, nSat] = size(current_obs);
    
    
    if i == 1
        reshaped_obs = reshape(current_obs(:,codeIndices,:), [nepochs, nSat*nCodeObs]);
    else
        reshaped_obs = [reshaped_obs, reshape(current_obs(:,codeIndices,:), [nepochs, nSat*nCodeObs])];
    end
end

obsChange = diff(reshaped_obs);

code_jump_epochs = find(all((abs(obsChange)> 2e5 )| obsChange == 0,2));

time_diff = diff(time_epochs);

if all(time_diff(:,1) == 0)
    time_diff = time_diff(:,2);
else
    
end

time_jump_epochs = find(abs(time_diff-tInterval)> 1e-4);

jump_epochs = union(time_jump_epochs, code_jump_epochs);
jump_epochs = unique(jump_epochs);

nClockJumps = length(jump_epochs);
meanClockJumpInterval = mean(diff(jump_epochs)*tInterval);
stdClockJumpInterval = std(diff(jump_epochs)*tInterval);
if isnan(meanClockJumpInterval)
   meanClockJumpInterval = 0;
   stdClockJumpInterval = 0;
end

end