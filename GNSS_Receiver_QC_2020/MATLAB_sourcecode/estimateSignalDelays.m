function [ion_delay_phase1, multipath_range1, multipath_range2, range1_slip_periods, range1_observations, phase1_observations, success] = estimateSignalDelays(range1_Code, range2_Code, ...
    phase1_Code, phase2_Code, carrier_freq1, carrier_freq2, nepochs, max_sat,...
     GNSS_SVs, obsCodes, GNSS_obs, currentGNSSsystem, tInterval, phaseCodeLimit, ionLimit)

% Function that takes observations of from the observation period and
% estimates the following delays on the signal:
%                                               ionospheric delay on the
%                                               first phase signal

%                                               multipath delay on the
%                                               first range signal

%                                               multipath delay on the
%                                               second range signal

% Also estimates epochs with ambiguity slip for the first signal.

% The estimates are all relative estimates. The multipath estimates are
% relative to a mean values. The ionosphere delay estimates are relative to
% the first estimate. These estimates must be reduced again by the
% releative value at ambiguity slips. The function calls on another
% function to estimate periods of ambiguity slips. It then corrects the
% delay estimates for these slips.
%--------------------------------------------------------------------------------------------------------------------------
% INPUTS

% range1_Code:          string, defines the observation type that will be 
%                       used as the first range observation. ex. "C1X", "C5X"    

% range2_Code:          string, defines the observation type that will be 
%                       used as the second range observation. ex. "C1X", "C5X"

% phase1_Code:          string, defines the observation type that will be 
%                       used as the first phase observation. ex. "L1X", "L5X"

% phase2_Code:          string, defines the observation type that will be 
%                       used as the second phase observation. ex. "L1X", "L5X"

% carrier_freq1:        carrier frequency of first phase signal. unit Hz

% carrier_freq2:        carrier frequency of second phase signal. unit Hz

% nepochs:              number of epochs with observations in rinex observation file.

% max_sat:              max PRN number of current GNSS system.

% GNSS_SVs:             matrix containing number of satellites with 
%                       obsevations for each epoch, and PRN for those satellites

%                       GNSS_SVs(epoch, j)  j=1: number of observed satellites
%                                           j>1: PRN of observed satellites

% obsCodes:             cell contaning strings. Cell defines the observation
%                       codes available from the current GNSS system. Each
%                       string is a three-character string, the first 
%                       character (a capital letter) is an observation code 
%                       ex. "L" or "C". The second character (a digit)
%                       is a frequency code. The third character(a Capital letter)  
%                       is the attribute, ex. "P" or "X"

% GNSS_obs:             3D matrix containing all observation of current 
%                       GNSS system for all epochs. Order of obsType index
%                       is same order as in obsCodes cell

%                       GNSS_obs(PRN, obsType, epoch)
%                                           PRN: double
%                                           ObsType: double: 1,2,...,numObsTypes
%                                           epoch: double

% currentGNSSsystem:    string, code to indicate which GNSS system is the
%                       current observations are coming from. Only used to
%                       decide if system uses CDMA or FDMA.

% tInterval:            observations interval; seconds. 

% phaseCodeLimit:       critical limit that indicates cycle slip for
%                       phase-code combination. Unit: m/s. If set to 0,
%                       default value will be used

% ionLimit:             critical limit that indicates cycle slip for
%                       the rate of change of the ionopheric delay. 
%                       Unit: m/s. If set to 0, default value will be used
%--------------------------------------------------------------------------------------------------------------------------
% OUTPUTS

% ion_delay_phase1:     matrix containing estimates of ionospheric delays 
%                       on the first phase signal for each PRN, at each epoch.

%                       ion_delay_phase1(epoch, PRN)

% multipath_range1:     matrix containing estimates of multipath delays 
%                       on the first range signal for each PRN, at each epoch.

%                       multipath_range2(epoch, PRN)

% multipath_range2:     matrix containing estimates of multipath delays 
%                       on the second range signal for each PRN, at each epoch.

%                       multipath_range2(epoch, PRN)

% range1_slip_periods:  cell, each cell element contains one matrix for
%                       every PRN. Each matrix contains epochs of range1
%                       slip starts in first column and slip ends in second
%                       column.

% range1_observations:  matrix. Contains all range1 observations for all
%                       epochs and for all SVs. 

%                       range1_observations(epoch, PRN)

% phase1_observations:  matrix. Contains all phase1 observations for all
%                       epochs and for all SVs. 

%                       range1_observations(epoch, PRN)

% success:              boolean, 1 if no error occurs, 0 otherwise

%--------------------------------------------------------------------------------------------------------------------------
%%

fid = fopen('GNSS_QC_Testing_00667.csv', 'a+');

FDMA_used = 0;
success = 1;

if strcmp(currentGNSSsystem, "R")
    FDMA_used = 1;
    carrier_freq1_list = carrier_freq1;
    carrier_freq2_list = carrier_freq2;
else
    alpha = carrier_freq1^2/carrier_freq2^2; % amplfication factor
end
% Define parameters
c = 299792458; % speed of light

if ionLimit ==0
    ionLimit = 4/60; % critical rate of change of ionosphere 
                       % delay  to indicate ambiguity slip on either 
                       % range1/phase1 signal, range2/phase2 signal, or
                       % both
end

if phaseCodeLimit == 0
   phaseCodeLimit = 4/60*100; % critical rate of change of
                        % N1_pseudo_estimate to indicate ambiguity slip on
                        % the range1/phase1 signal
end
% Initialize data matrices
ion_delay_phase1        = zeros(nepochs, max_sat);
multipath_range1        = zeros(nepochs, max_sat);
multipath_range2        = zeros(nepochs, max_sat);
N1_pseudo_estimate      = zeros(nepochs, max_sat);
missing_obs_overview    = zeros(nepochs, max_sat);
missing_range1_overview = zeros(nepochs, max_sat);


% Initialize cell for storing phase slip periods
ambiguity_slip_periods = cell(1, max_sat);
% Initialize cell for storing slip periods for only range1/phase1
range1_slip_periods = cell(1, max_sat);

% Make estimates of delays
for epoch = 1:nepochs

    % Calculate how many satelites with observations in current epoch
    n_sat= GNSS_SVs(epoch,1);
    
    % Itterate over all satellites of current epoch
    for i = 1:n_sat
        PRN = GNSS_SVs(epoch,1+i); % Get PRN
        
        if FDMA_used
            carrier_freq1 = carrier_freq1_list(PRN);
            carrier_freq2 = carrier_freq2_list(PRN);
            alpha = carrier_freq1^2/carrier_freq2^2; % amplfication factor
        end
        
        % Get observations
        range1 = GNSS_obs(PRN, ismember(obsCodes,range1_Code), epoch);
        range2 = GNSS_obs(PRN, ismember(obsCodes,range2_Code), epoch);
        
        phase1 = GNSS_obs(PRN, ismember(obsCodes,phase1_Code), epoch)*c/carrier_freq1;
        phase2 = GNSS_obs(PRN, ismember(obsCodes,phase2_Code), epoch)*c/carrier_freq2;
        
        if any(cellfun(@isempty, {range1, range2, phase1, phase2}) == 1)
            fprintf(['ERROR(estimateSignalDelays): There is no observation type %s. ' ...
                'Check for missing data in RINEX observation file!'], phase1_Code)
            success = 0;
            return
        end
        % if any of the four observations are missing, ie 0, estimate
        % remains 0 for that epoch and satellite
        if ~any([range1, range2, phase1, phase2] == 0)
            
            % Calculate estimate for Ionospheric delay on phase 1 signal
            ion_delay_phase1(epoch, PRN) = 1/(alpha-1)*(phase1-phase2);

            % Calculate multipath on both range signals
            multipath_range1(epoch,PRN) = range1 - (1 + 2/(alpha-1))*phase1 + (2/(alpha-1))*phase2;
            multipath_range2(epoch,PRN) = range2 - (2*alpha/(alpha-1))*phase1 + ((2*alpha)/(alpha-1) - 1)*phase2;
            
            N1_pseudo_estimate(epoch, PRN) = phase1 - range1;
        else
            % flag epoch and PRN as missing obs
            missing_obs_overview(epoch, PRN) = 1;
            if range1 == 0 || phase1 == 0
               missing_range1_overview(epoch, PRN) = 1; 
            else
               N1_pseudo_estimate(epoch, PRN) = phase1 - range1;
            end
        end
    end
end


% Detect and correct for ambiguity slips
for PRN = 1:max_sat
   % Get first and last epoch with observations for current PRN
   epoch_first_obs = find(abs(ion_delay_phase1(:,PRN))>0, 1);
   epoch_last_obs = find(abs(ion_delay_phase1(:,PRN))>0, 1, 'last'); 
   
   % Get first and last epoch with range 1 observations for current PRN
   epoch_first_range1obs = find(abs(N1_pseudo_estimate(:,PRN))>0, 1);
   epoch_last_range1obs = find(abs(N1_pseudo_estimate(:,PRN))>0, 1, 'last'); 
   
%    Run function to detect ambiguity slips for current epoch for
%    range1/phase1 only 


   range1_slip_epochs = detectPhaseSlips(N1_pseudo_estimate(:, PRN), missing_range1_overview(:, PRN),...
    epoch_first_range1obs, epoch_last_range1obs, tInterval, phaseCodeLimit);
   
   % Run function to detect ambiguity slips for current epoch for either 
   % range1/phase1 signal, range2/phase2 signal, or both
   ionosphere_slip_epochs = detectPhaseSlips(ion_delay_phase1(:, PRN), missing_obs_overview(:, PRN),...
    epoch_first_obs, epoch_last_obs, tInterval, ionLimit);
   
   s1 = length(ionosphere_slip_epochs);
   s2 = length(range1_slip_epochs);
   
   % make combined array of slip epochs from both lin. combinations used
    % to detects slips
    ambiguity_slip_epochs = unique(union(range1_slip_epochs, ionosphere_slip_epochs));
    
    
    range1_slip_epochs = intersect(range1_slip_epochs, ionosphere_slip_epochs);
    
    s3 = length(ambiguity_slip_epochs);
    s4 = length(range1_slip_epochs);

    fprintf(fid, '%s, %s, %s, %d, %d, %d, %d, %d\n', currentGNSSsystem, range1_Code, range2_Code, PRN, s1,s2,s3,s4);
    % Orginize slips detected on range1/phase1 signal only
    [range1_slip_periods{PRN}, ~] = orgSlipEpochs(range1_slip_epochs);
    
    % Orginize combined slips detected on range1/phase1 signal only
    [ambiguity_slip_periods{PRN}, n_slip_periods] = orgSlipEpochs(ambiguity_slip_epochs);
    
    
   % Set all zero estimates to NaN so that epochs with missing observations 
   % are not corrected
   ion_delay_phase1(ion_delay_phase1(:, PRN)==0, PRN) = NaN;
   multipath_range1(multipath_range1(:, PRN)==0, PRN) = NaN;
   multipath_range2(multipath_range2(:, PRN)==0, PRN) = NaN;

   % If there are no slips then there is only one "ambiguity period". All
   % estimates are therefore reduced by the same relative value
   if isempty(ambiguity_slip_periods{PRN})
       
       ion_delay_phase1(epoch_first_obs:end, PRN) = ion_delay_phase1(epoch_first_obs:end, PRN) -...
           ion_delay_phase1(epoch_first_obs, PRN);   
       
       multipath_range1(epoch_first_obs:end, PRN) = multipath_range1(epoch_first_obs:end, PRN) -...
           mean(nonzeros(multipath_range1(epoch_first_obs:end, PRN)));
       
       multipath_range2(epoch_first_obs:end, PRN) = multipath_range2(epoch_first_obs:end, PRN) -...
           mean(nonzeros(multipath_range2(epoch_first_obs:end, PRN)));
   else
         
       % Set all estimates of epochs with phase slips to NaN
       for slip_period = 1:n_slip_periods
           slip_start   = ambiguity_slip_periods{PRN}(slip_period,1);
           slip_end     = ambiguity_slip_periods{PRN}(slip_period,2);
           
           ion_delay_phase1(slip_start:slip_end, PRN) = NaN; 
           multipath_range1(slip_start:slip_end, PRN) = NaN;
           multipath_range2(slip_start:slip_end, PRN) = NaN;
       end
       
       % extract start and end of each "ambiguity period" and correct 
       % multipath and ionosphere estimates for each ambiguity period
       for ambiguity_period = 1:n_slip_periods + 1
           
           % if first ambiguity period
           if ambiguity_period == 1
               
               ambiguity_period_start       = epoch_first_obs;
               ambiguity_period_end         = ambiguity_slip_periods{PRN}(1,1) - 1; 
           % if last ambiguity period
           elseif ambiguity_period == n_slip_periods + 1
               
               ambiguity_period_start       = ambiguity_slip_periods{PRN}(end,2) + 1; 
               ambiguity_period_end         = epoch_last_obs; 
               
               % If last epoch with observation is a slip, then there is no
               % last ambiguity period
               if ambiguity_period_start > epoch_last_obs
                   ambiguity_period_start = [];
                   ambiguity_period_end = [];
               end
           else
               ambiguity_period_start       = ambiguity_slip_periods{PRN}(ambiguity_period-1, 2) + 1;
               ambiguity_period_end         = ambiguity_slip_periods{PRN}(ambiguity_period, 1) - 1;
           end
           
           % Ionosphere delay estimates of current ambiguity period is
           % reduced by first estimate of ambiguity period
           ion_delay_phase1(ambiguity_period_start:ambiguity_period_end, PRN) = ion_delay_phase1(ambiguity_period_start:ambiguity_period_end, PRN) -...
               ion_delay_phase1(ambiguity_period_start, PRN);
           
           % Multipath delays of current ambiguity period are reduced by
           % mean of estimates in ambiguity period, excluding NaN and
           multipath_range1(ambiguity_period_start:ambiguity_period_end, PRN) = multipath_range1(ambiguity_period_start:ambiguity_period_end, PRN) -...
               mean(multipath_range1(ambiguity_period_start:ambiguity_period_end, PRN), 'omitnan');
               
           multipath_range2(ambiguity_period_start:ambiguity_period_end, PRN) = multipath_range2(ambiguity_period_start:ambiguity_period_end, PRN) -...
               mean(multipath_range2(ambiguity_period_start:ambiguity_period_end, PRN), 'omitnan');   
       end 
   end  
   % Get range1 and phase 1 observations for all epochs and PRN
   range1_observations = squeeze(GNSS_obs(:, ismember(obsCodes,range1_Code), :))';
   phase1_observations = squeeze(GNSS_obs(:, ismember(obsCodes,phase1_Code), :))';

end

fprintf(fid, '\n');
fclose(fid);

end


