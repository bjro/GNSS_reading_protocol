function exportData(analysisResults, rinObsFilename, outputDir)


%% Initialize files

% if no output dir given, use default
if isempty(outputDir)
    outputDir = 'Outputs_Files';
end

% make output dir if it doesn't exist
if ~exist(outputDir, 'dir')
    mkdir(outputDir)
end

% make filenames
[~, rinObsFilename, ~] = fileparts(rinObsFilename);
headerFilename = fullfile(outputDir, append(rinObsFilename,'_Data_Output_Header.txt'));
OutputFilename1 = fullfile(outputDir, append(rinObsFilename,'_Data_Output_File_1.csv'));
OutputFilename2 = fullfile(outputDir, append(rinObsFilename,'_Data_Output_File_2.csv'));

fid = fopen(headerFilename, 'wt');


%% Write header intro

fprintf(fid, 'GNSS Receiver Quality Check 2020 Data Output Header File\n');
fprintf(fid, 'Software version: 1.00\n');
fprintf(fid, 'Software developed by Bjørn-Eirik Roald\nNorwegian University of Life Sciences(NMBU)\n\n');

%% Write header metadata

metaStruct = analysisResults.ExtraOutputInfo;

fprintf(fid, 'METADATA\n');
fprintf(fid, '* rinex_obs_filename:%s\n', metaStruct.rinex_obs_filename);
fprintf(fid, '* rinexVersion:%s\n', metaStruct.rinexVersion);
fprintf(fid, '* rinexProgr:%s\n', metaStruct.rinexProgr);
fprintf(fid, '* markerName:%s\n', metaStruct.markerName);
fprintf(fid, '* recType:%s\n', metaStruct.recType);
fprintf(fid, '** tFirstObs:%4d/%d/%d %d:%d:%.2f \n', ...
    metaStruct.tFirstObs(1),metaStruct.tFirstObs(2),metaStruct.tFirstObs(3),metaStruct.tFirstObs(4),metaStruct.tFirstObs(5),metaStruct.tFirstObs(6));
fprintf(fid, '** tLastObs:%4d/%d/%d %d:%d:%.2f \n', ...
    metaStruct.tLastObs(1),metaStruct.tLastObs(2),metaStruct.tLastObs(3),metaStruct.tLastObs(4),metaStruct.tLastObs(5),metaStruct.tLastObs(6));
fprintf(fid, '*** tInterval:%d\n', metaStruct.tInterval);
fprintf(fid, '*** nClockJumps:%d\n', metaStruct.nClockJumps);
fprintf(fid, '*** meanClockJumpInterval:%d\n', metaStruct.meanClockJumpInterval);
fprintf(fid, '*** stdClockJumpInterval:%d\n', metaStruct.stdClockJumpInterval);
fprintf(fid, '*** ionLimit:%6.3f\n', metaStruct.ionLimit);
fprintf(fid, '*** phaseCodeLimit:%6.3f\n', metaStruct.phaseCodeLimit);

% GLONASS data is present, write channel - satID map to headerfile
if ismember('GLONASS',analysisResults.GNSSsystems)
   fprintf(fid, '**** GLONASS Channels:');
   satID = cell2mat(keys(analysisResults.ExtraOutputInfo.GLO_Slot2ChannelMap));
   channels =  cell2mat(values(analysisResults.ExtraOutputInfo.GLO_Slot2ChannelMap));
   
   for i = 1:length(satID)
      if i > 1
         fprintf(fid, ',');  
      end
      fprintf(fid, '%d/%d', satID(i), channels(i));
   end
   fprintf(fid, '\n');
end
fprintf(fid, 'METADATA ENDS HERE\n');

%% write data file 1 and header description

nEpochs = analysisResults.ExtraOutputInfo.nEpochs;
Data1_colums = 0;
Data1_data_types_per_sat = 5;

% Compute number of colums in data1 file
for sysI = 1:analysisResults.nGNSSsystems
   sys = analysisResults.GNSSsystems{sysI};
   sysStruct = analysisResults.(sys);
   
   for bandI = 1:sysStruct.nBands
      band = sysStruct.Bands(bandI);
      bandStruct = sysStruct.(band);
      
   for codeI = 1:bandStruct.nCodes
      code = bandStruct.Codes{codeI};
      codeStruct = bandStruct.(code);
      
      [~, nSat] = size(codeStruct.range1_observations);
      Data1_colums = Data1_colums + nSat * Data1_data_types_per_sat;
   end
   end
end

% create data1 file
Data1 = NaN(nEpochs, Data1_colums);

current_column = 1;

fprintf(fid, '\nDATA FILE 1 DESCRIPTION\n\n');

% write list of GNSS systems
fprintf(fid, 'GNSS systems:');

for sysI = 1:analysisResults.nGNSSsystems
   if sysI >1
      fprintf(fid, ',');
   end
   sys = analysisResults.GNSSsystems{sysI};
   fprintf(fid, '%s', sys);
end
fprintf(fid, '\n');

% iterate systems
for sysI = 1:analysisResults.nGNSSsystems
   sys = analysisResults.GNSSsystems{sysI};
   sysStruct = analysisResults.(sys);
   
   % System title
   fprintf(fid, '\n* %s\n', sys);
   nBands = sysStruct.nBands;
   
   %List of band for current list
   fprintf(fid, 'Bands:');
   
   for bandI = 1:nBands
      band = sysStruct.Bands(bandI);
      if bandI > 1
         fprintf(fid, ',');
      end
      fprintf(fid, '%s', band);
   end
   fprintf(fid, '\n');
   
   % iterate through bands
   for bandI = 1:nBands
      band = sysStruct.Bands(bandI);
      bandStruct = sysStruct.(band);
      
      % band title
      fprintf(fid, '\n** %s\n', band);
      nCodes = bandStruct.nCodes;
      
      % list of codes for current band
      fprintf(fid, 'Codes:');
      for codeI = 1:nCodes
         code = bandStruct.Codes{codeI};
      if codeI > 1
         fprintf(fid, ',');
      end
      fprintf(fid, '%s', code);
      end
      fprintf(fid, '\n');
      
      % iterate through codes
      for codeI = 1:nCodes
         code = bandStruct.Codes{codeI};
         codeStruct = bandStruct.(code);
         
         % code title
         fprintf(fid, '\n*** %s\n', code);
         
         [~, nSat] = size(codeStruct.range1_observations);
         
         % write to data array and write start and end columns to
         % headerfile
         
         start_column = current_column;
         end_column = current_column + nSat - 1;
         Data1(:,start_column:end_column) = codeStruct.range1_observations;
         current_column = end_column + 1;
         fprintf(fid, 'range1_observations:%d,%d\n',start_column, end_column);
         
         start_column = current_column;
         end_column = current_column + nSat - 1;
         Data1(:,start_column:end_column) = codeStruct.phase1_observations;
         current_column = end_column + 1;
         fprintf(fid, 'phase1_observations:%d,%d\n',start_column, end_column);
         
         start_column = current_column;
         end_column = current_column + nSat - 1;
         Data1(:,start_column:end_column) = codeStruct.ion_delay_phase1;
         current_column = end_column + 1;
         fprintf(fid, 'ion_delay_phase1:%d,%d\n',start_column, end_column);
         
         start_column = current_column;
         end_column = current_column + nSat - 1;
         Data1(:,start_column:end_column) = codeStruct.multipath_range1;
         current_column = end_column + 1;
         fprintf(fid, 'multipath_range1:%d,%d\n',start_column, end_column);
         
         start_column = current_column;
         end_column = current_column + nSat - 1;
         Data1(:,start_column:end_column) = codeStruct.sat_elevation_angles;
         current_column = end_column + 1;
         fprintf(fid, 'sat_elevation_angles:%d,%d\n',start_column, end_column);
         
      fprintf(fid, '%s ENDS HERE\n', code);
      end
      fprintf(fid, '%s ENDS HERE\n', band);
   end
   fprintf(fid, '%s ENDS HERE\n', sys);
end
fprintf(fid, 'DATA FILE 1 DESCRIPTION ENDS HERE\n');



%% write data file 2 and header description



Data2_colums = 0;
Data2ColumnsPerCodePerSat = 2 + 3;

max_nSat = 0;

% compute number og columns for data2 array
for sysI = 1:analysisResults.nGNSSsystems
   sys = analysisResults.GNSSsystems{sysI};
   sysStruct = analysisResults.(sys);
   
   for bandI = 1:sysStruct.nBands
      band = sysStruct.Bands(bandI);
      bandStruct = sysStruct.(band);
      
   for codeI = 1:bandStruct.nCodes
      code = bandStruct.Codes{codeI};
      codeStruct = bandStruct.(code);
      
      [~, nSat] = size(codeStruct.range1_observations);
      if nSat > max_nSat
         max_nSat = nSat;
      end
      Data2_colums = Data2_colums + nSat * Data2ColumnsPerCodePerSat + 17;
   end
   end
end

Data2 = NaN(max_nSat, Data2_colums);
current_column = 1;

fprintf(fid, '\nDATA FILE 2 DESCRIPTION\n\n');

% itareate through systems
for sysI = 1:analysisResults.nGNSSsystems
   sys = analysisResults.GNSSsystems{sysI};
   sysStruct = analysisResults.(sys);
   
   % system title
   fprintf(fid, '\n* %s\n', sys);
   nBands = sysStruct.nBands;
   
   % List of bands
   fprintf(fid, 'Bands:');
   
   for bandI = 1:nBands
      band = sysStruct.Bands(bandI);
      if bandI > 1
         fprintf(fid, ',');
      end
      fprintf(fid, '%s', band);
   end
   fprintf(fid, '\n');
   
   % iterate through bands
   for bandI = 1:nBands
      band = sysStruct.Bands(bandI);
      bandStruct = sysStruct.(band);
      
      fprintf(fid, '\n** %s\n', band);
      nCodes = bandStruct.nCodes;
      
      % list of codes
      fprintf(fid, 'Codes:');
      for codeI = 1:nCodes
         code = bandStruct.Codes{codeI};
      if codeI > 1
         fprintf(fid, ',');
      end
      fprintf(fid, '%s', code);
      end
      fprintf(fid, '\n');
      
      % iterate through codes
      for codeI = 1:nCodes
         code = bandStruct.Codes{codeI};
         codeStruct = bandStruct.(code);
         
         % code title
         fprintf(fid, '\n*** %s\n', code);
         
         [~, nSat] = size(codeStruct.range1_observations);
         
         % write number of rows, start and end columns to header.
         % write to data2 array
         
         %mean_multipath_range1_overall
         nCols = 1;
         nRows = 1;
         start_column = current_column;
         end_column = start_column + (nCols-1);
         current_column = current_column + nCols;
         Data2(1:nRows, start_column:end_column) = codeStruct.mean_multipath_range1_overall;
         fprintf(fid, 'mean_multipath_range1_overall:%d,%d,%d\n',nRows, start_column, end_column);
         
         %rms_multipath_range1_averaged
         nCols = 1;
         nRows = 1;
         start_column = current_column;
         end_column = start_column + (nCols-1);
         current_column = current_column + nCols;
         Data2(1:nRows, start_column:end_column) = codeStruct.rms_multipath_range1_averaged;
         fprintf(fid, 'rms_multipath_range1_averaged:%d,%d,%d\n',nRows, start_column, end_column);
         
         %mean_ion_delay_phase1_overall
         nCols = 1;
         nRows = 1;
         start_column = current_column;
         end_column = start_column + (nCols-1);
         current_column = current_column + nCols;
         Data2(1:nRows, start_column:end_column) = codeStruct.mean_ion_delay_phase1_overall;
         fprintf(fid, 'mean_ion_delay_phase1_overall:%d,%d,%d\n',nRows, start_column, end_column);
         
         %nEstimates
         nCols = 1;
         nRows = 1;
         start_column = current_column;
         end_column = start_column + (nCols-1);
         current_column = current_column + nCols;
         Data2(1:nRows, start_column:end_column) = codeStruct.nEstimates;
         fprintf(fid, 'nEstimates:%d,%d,%d\n',nRows, start_column, end_column);
         
         %nRange1Obs
         nCols = 1;
         nRows = 1;
         start_column = current_column;
         end_column = start_column + (nCols-1);
         current_column = current_column + nCols;
         Data2(1:nRows, start_column:end_column) = codeStruct.nRange1Obs;
         fprintf(fid, 'nRange1Obs:%d,%d,%d\n',nRows, start_column, end_column);
         
         %elevation_weighted_average_rms_multipath_range1
         nCols = 1;
         nRows = 1;
         start_column = current_column;
         end_column = start_column + (nCols-1);
         current_column = current_column + nCols;
         Data2(1:nRows, start_column:end_column) = codeStruct.elevation_weighted_average_rms_multipath_range1;
         fprintf(fid, 'elevation_weighted_average_rms_multipath_range1:%d,%d,%d\n',nRows, start_column, end_column);
         
         %ObsCodes
         
         % storing the three characters of each code inthe first three
         % rows. Using ascii codes for first and last character.
         % order of columns is: range1_Code, phase1_Code, range2_Code, phase2_Code
         
         nCols = 4;
         nRows = 3;
         start_column = current_column;
         end_column = start_column + (nCols-1);
         current_column = current_column + nCols;
         range1_Code = char(codeStruct.range1_Code);
         range2_Code = char(codeStruct.range2_Code);
         phase1_Code = char(codeStruct.phase1_Code);
         phase2_Code = char(codeStruct.phase2_Code);
         Data2(1:nRows, start_column:end_column) = [double(range1_Code(1)), str2double(range1_Code(2)), double(range1_Code(3)); ...
                                                   double(phase1_Code(1)), str2double(phase1_Code(2)), double(phase1_Code(3)); ...
                                                   double(range2_Code(1)), str2double(range2_Code(2)), double(range2_Code(3)); ... 
                                                   double(phase2_Code(1)), str2double(phase2_Code(2)), double(phase2_Code(3))]';
                                                
         fprintf(fid, 'range1/phase1/range2/phase2 Codes:%d,%d,%d\n',nRows, start_column, end_column);
         
         %mean_multipath_range1_satellitewise
         nCols = 1;
         nRows = nSat;
         start_column = current_column;
         end_column = start_column + (nCols-1);
         current_column = current_column + nCols;
         Data2(1:nRows, start_column:end_column) = codeStruct.mean_multipath_range1_satellitewise';
         fprintf(fid, 'mean_multipath_range1_satellitewise:%d,%d,%d\n',nRows, start_column, end_column);
         
         %rms_multipath_range1_satellitewise
         nCols = 1;
         nRows = nSat;
         start_column = current_column;
         end_column = start_column + (nCols-1);
         current_column = current_column + nCols;
         Data2(1:nRows, start_column:end_column) = codeStruct.rms_multipath_range1_satellitewise';
         fprintf(fid, 'rms_multipath_range1_satellitewise:%d,%d,%d\n',nRows, start_column, end_column);
         
         %mean_ion_delay_phase1_satellitewise
         nCols = 1;
         nRows = nSat;
         start_column = current_column;
         end_column = start_column + (nCols-1);
         current_column = current_column + nCols;
         Data2(1:nRows, start_column:end_column) = codeStruct.mean_ion_delay_phase1_satellitewise';
         fprintf(fid, 'mean_ion_delay_phase1_satellitewise:%d,%d,%d\n',nRows, start_column, end_column);
         
         %mean_sat_elevation_angles
         nCols = 1;
         nRows = nSat;
         start_column = current_column;
         end_column = start_column + (nCols-1);
         current_column = current_column + nCols;
         Data2(1:nRows, start_column:end_column) = codeStruct.mean_sat_elevation_angles';
         fprintf(fid, 'mean_sat_elevation_angles:%d,%d,%d\n',nRows, start_column, end_column);
         
         %nEstimates_per_sat
         nCols = 1;
         nRows = nSat;
         start_column = current_column;
         end_column = start_column + (nCols-1);
         current_column = current_column + nCols;
         Data2(1:nRows, start_column:end_column) = codeStruct.nEstimates_per_sat';
         fprintf(fid, 'nEstimates_per_sat:%d,%d,%d\n',nRows, start_column, end_column);
         
         %n_range1_obs_per_sat
         nCols = 1;
         nRows = nSat;
         start_column = current_column;
         end_column = start_column + (nCols-1);
         current_column = current_column + nCols;
         Data2(1:nRows, start_column:end_column) = codeStruct.n_range1_obs_per_sat';
         fprintf(fid, 'n_range1_obs_per_sat:%d,%d,%d\n',nRows, start_column, end_column);
         
         %elevation_weighted_rms_multipath_range1_satellitewise
         nCols = 1;
         nRows = nSat;
         start_column = current_column;
         end_column = start_column + (nCols-1);
         current_column = current_column + nCols;
         Data2(1:nRows, start_column:end_column) = codeStruct.elevation_weighted_rms_multipath_range1_satellitewise';
         fprintf(fid, 'elevation_weighted_rms_multipath_range1_satellitewise:%d,%d,%d\n',nRows, start_column, end_column);
         
         %range1_slip_distribution
         nCols = 1;
         nRows = 8;
         start_column = current_column;
         end_column = start_column + (nCols-1);
         current_column = current_column + nCols;
         slipStruct = codeStruct.range1_slip_distribution;
         Data2(1:nRows, start_column:end_column) = [slipStruct.n_slips_0_10, ...
            slipStruct.n_slips_10_20, slipStruct.n_slips_20_30, ...
            slipStruct.n_slips_30_40, slipStruct.n_slips_40_50, ...
            slipStruct.n_slips_over50, slipStruct.n_slips_NaN, ...
            slipStruct.n_slips_Tot]';
         fprintf(fid, 'range1_slip_distribution:%d,%d,%d\n',nRows, start_column, end_column);
         
         %LLI_slip_distribution
         nCols = 1;
         nRows = 8;
         start_column = current_column;
         end_column = start_column + (nCols-1);
         current_column = current_column + nCols;
         slipStruct = codeStruct.LLI_slip_distribution;
         Data2(1:nRows, start_column:end_column) = [slipStruct.n_slips_0_10, ...
            slipStruct.n_slips_10_20, slipStruct.n_slips_20_30, ...
            slipStruct.n_slips_30_40, slipStruct.n_slips_40_50, ...
            slipStruct.n_slips_over50, slipStruct.n_slips_NaN, ...
            slipStruct.n_slips_Tot]';
         fprintf(fid, 'LLI_slip_distribution:%d,%d,%d\n',nRows, start_column, end_column);
         
         %slip_distribution_LLI_fusion
         nCols = 1;
         nRows = 8;
         start_column = current_column;
         end_column = start_column + (nCols-1);
         current_column = current_column + nCols;
         slipStruct = codeStruct.slip_distribution_LLI_fusion;
         Data2(1:nRows, start_column:end_column) = [slipStruct.n_slips_0_10, ...
            slipStruct.n_slips_10_20, slipStruct.n_slips_20_30, ...
            slipStruct.n_slips_30_40, slipStruct.n_slips_40_50, ...
            slipStruct.n_slips_over50, slipStruct.n_slips_NaN, ...
            slipStruct.n_slips_Tot]';
         fprintf(fid, 'slip_distribution_LLI_fusion:%d,%d,%d\n',nRows, start_column, end_column);
         
         %range1_slip_distribution_per_sat
         nRows = 8;
         nCols = nSat;
         start_column = current_column;
         end_column = start_column + (nCols-1);
         fprintf(fid, 'range1_slip_distribution_per_sat:%d,%d,%d\n',nRows, start_column, end_column);
         for satI = 1:nSat
            nCols = 1;
            nRows = 8;
            start_column = current_column;
            end_column = start_column + (nCols-1);
            current_column = current_column + nCols;
            slipStruct = codeStruct.range1_slip_distribution_per_sat{satI};
            Data2(1:nRows, start_column:end_column) = [slipStruct.n_slips_0_10, ...
               slipStruct.n_slips_10_20, slipStruct.n_slips_20_30, ...
               slipStruct.n_slips_30_40, slipStruct.n_slips_40_50, ...
               slipStruct.n_slips_over50, slipStruct.n_slips_NaN, ...
               slipStruct.n_slips_Tot]';  
         end
         
         %LLI_slip_distribution_per_sat
         nRows = 8;
         nCols = nSat;
         start_column = current_column;
         end_column = start_column + (nCols-1);
         fprintf(fid, 'LLI_slip_distribution_per_sat:%d,%d,%d\n',nRows, start_column, end_column);
         for satI = 1:nSat
            nCols = 1;
            nRows = 8;
            start_column = current_column;
            end_column = start_column + (nCols-1);
            current_column = current_column + nCols;
            slipStruct = codeStruct.LLI_slip_distribution_per_sat{satI};
            Data2(1:nRows, start_column:end_column) = [slipStruct.n_slips_0_10, ...
               slipStruct.n_slips_10_20, slipStruct.n_slips_20_30, ...
               slipStruct.n_slips_30_40, slipStruct.n_slips_40_50, ...
               slipStruct.n_slips_over50, slipStruct.n_slips_NaN, ...
               slipStruct.n_slips_Tot]';  
         end
         
         %slip_distribution_per_sat_LLI_fusion
         nRows = 8;
         nCols = nSat;
         start_column = current_column;
         end_column = start_column + (nCols-1);
         fprintf(fid, 'slip_distribution_per_sat_LLI_fusion:%d,%d,%d\n',nRows, start_column, end_column);
         for satI = 1:nSat
            nCols = 1;
            nRows = 8;
            start_column = current_column;
            end_column = start_column + (nCols-1);
            current_column = current_column + nCols;
            slipStruct = codeStruct.slip_distribution_per_sat_LLI_fusion{satI};
            Data2(1:nRows, start_column:end_column) = [slipStruct.n_slips_0_10, ...
               slipStruct.n_slips_10_20, slipStruct.n_slips_20_30, ...
               slipStruct.n_slips_30_40, slipStruct.n_slips_40_50, ...
               slipStruct.n_slips_over50, slipStruct.n_slips_NaN, ...
               slipStruct.n_slips_Tot]';  
         end
         fprintf(fid, '%s ENDS HERE\n', code);
      end
      fprintf(fid, '%s ENDS HERE\n', band);
   end
   fprintf(fid, '%s ENDS HERE\n', sys);
end

fprintf(fid, 'DATA FILE 2 DESCRIPTION ENDS HERE\n');



%% write observation overview file

fprintf(fid, '\nOBSERVATION OVERVIEW\n');

%iterate through systems
for sysI = 1:analysisResults.nGNSSsystems
   sys = analysisResults.GNSSsystems{sysI};
   overviewStruct = analysisResults.(sys).observationOverview;
   nSat = length(fieldnames(overviewStruct));
   
   % system title
   fprintf(fid, '\n* %s\n', sys);
   fprintf(fid, 'nSat:%d\n\n', nSat);
   
   % iterate through sataellites
   for satI = 1:nSat
      
      % sat title
      sat = append('Sat_', string(satI));
      satStruct = overviewStruct.(sat);
      nBands = satStruct.n_possible_bands;
      
      fprintf(fid, '** %s\n', sat);
      
      % band list
      fprintf(fid, 'Bands:');
      
      for bandI = 1:nBands
         band = satStruct.Bands(bandI);
         
         if bandI > 1
            fprintf(fid, ',');
         end
         fprintf(fid, '%s', band);
      end
      fprintf(fid, '\n');
      
      % iterate through bands
      for bandI = 1:nBands
         band = satStruct.Bands(bandI);
         
         % print codes of current band
         fprintf(fid, '%s:', band);
         
         nCodes = length(satStruct.(band));
         
         for codeI = 1:nCodes
            code = satStruct.(band)(codeI);
            if codeI > 1
               fprintf(fid, ',');
            end
            fprintf(fid, '%s', code);
         end
         fprintf(fid, '\n');
         
      end
   end
end
fprintf(fid, 'OBSERVATION OVERVIEW ENDS HERE\n');
fprintf(fid, 'END OF FILE');


%% Export Data files

% change NaN to 0 and export to csv

Data1(isnan(Data1)) = 0;
writematrix(Data1, OutputFilename1)

Data2(isnan(Data2)) = 0;
writematrix(Data2, OutputFilename2)






















end