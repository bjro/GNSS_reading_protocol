function LLI_slip_periods = getLLISlipPeriods(LLI_current_phase)

% Function that sorts all ambiguity slips indicated by LLI in RINEX
% observation file.
%--------------------------------------------------------------------------------------------------------------------------

% INPUTS:

% LLI_current_phase:        matrix. contains LLI indicators for all epochs
%                           for current GNSS system and phase pbservation,
%                           for all satellites.

%                           LLI_current_phase(epoch, satID)
%--------------------------------------------------------------------------------------------------------------------------

% OUTPUTS:

% LLI_slip_periods:         cell. One cell element for each satellite. Each
%                           cell is a matrix. The matrix contains indicated
%                           slip period start epochs in first column, and 
%                           end epochs in second column. 
%--------------------------------------------------------------------------------------------------------------------------

[~, nSat] = size(LLI_current_phase);
LLI_slip_periods = cell(1, nSat);

% Itterrate through all satellites in current GNSS system
for sat = 1:nSat
   LLI_current_sat = LLI_current_phase(:, sat);
   
   % get epochs where LLI indicate slip, for current satellite
   LLI_slips = find(ismember(LLI_current_sat, [1, 2, 3, 5, 6, 7])); %001, 010, 011, 101, 110, 111
   
   % if there are slips
   if ~isempty(LLI_slips)
       
       % dummy is logical. It will be 1 at indices where the following
       % slip epoch is NOT the epoch following the current slip epoch.
       % These will therefor be the indices where slip periods end.
       % The last slip end is not detected this way and is inserted
       % manually
       dummy = diff(LLI_slips)~=1;
       slip_period_ends = [LLI_slips(dummy); LLI_slips(end)];
       n_slip_periods = sum(dummy) +1 ;
        
       current_slip_periods = zeros(n_slip_periods,2); 
       % store slip ends
       current_slip_periods(:,2) = slip_period_ends;
       % store first slip start manually
       current_slip_periods(1,1) = LLI_slips(1);
       
       % insert remaining slip period starts
       for k = 2:n_slip_periods
          current_slip_periods(k, 1) = LLI_slips(find(LLI_slips == current_slip_periods(k-1, 2)) + 1); 
       end
   else
       current_slip_periods = [];
   end
   LLI_slip_periods{sat} = current_slip_periods;
end

end