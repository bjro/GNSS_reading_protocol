function [elevation_angle, missing_nav_data] = get_elevation_angle(sys, PRN, week, tow, sat_positions, nEpochs, epoch_dates,... 
    epochInterval, navGNSSsystems, x_e, n_sp3_files, multipleIntervals, firstFileIndices)

% Calculates elevation angle of a satelite with specified PRN at specified
% epoch, viewed from defined receiver position
%--------------------------------------------------------------------------------------------------------------------------
% INPUTS
% sys:            Satellite system, string, 
%                   ex. "E" or "G"

% PRN:            Satellite identification number, integer

% week:             GPS-week number, integer

% tow:              "time-of-week", integer

% x_e:              Coordinates, in ECEF reference frame, of receiver station,
%                   ex. [X;Y;Z]

% ephemerisCell:    cell containing satellite navigation ephemeris of all 
%                   satellites of observation period, of each GNSS system. 
%                   Each cell element contains ephemeris of one GNSS system.
%                   Order of cells is defined by navGNSSsystems.
%                   Each cell elements at the next level is a matrix 
%                   containining ephemeris of one satellite. The ephemeris of 
%                   one navigation block is conatined in one line.
%                   ephemerisCell{GNSSsystemIndex}{PRN}

%                   ephemerisCell{GNSSsystemIndex}{PRN}(navBlock, ephemeris)

% n_nav_blocks:     cell, one cell element for each GNSS system. Each cell
%                   element is another cell with one element for each
%                   satellite. Each of these elements contains the number
%                   of navigation ephemeris block for this satellite.
%                   n_nav_blocks{GNSSsystemIndex}{PRN}

% navGNSSsystems:   cell, conatins codes of GNSS systems with navigation 
%                   ephemeris stored in ephemerisCell. Elements are char.
%                   ex. 'G' or 'E'

% n_sp3_files:       int. number of SP3 files read

% multipleIntervals: boolean. 1 if the SP3 files do not have the same epoch 
%                    interval. 0 otherwise

% firstFileIndices:  array. The indices of sat_positions containing info
%                    about the first epoch of each of the SP3 files loaded.
%                    Length of array is equal to number of SP3 files loaded
%                    Only used if multipleIntervals is 1
%--------------------------------------------------------------------------------------------------------------------------
% OUTPUTS:

% elevation_angle: Elevation angle of specified satelite at specified
%                  epoch, view from specified receiver station. Unit: Degrees 

% missing_nav_data: Boolean, 1 if orbit data for current satellite is
%                   missing from sp3 file, o otherwise
%--------------------------------------------------------------------------------------------------------------------------
%%

% Define GRS80 ellipsoid parameters
a       = 6378137;
f       = 1/298.257222100882711243;
b       = a*(1-f);

%%

missing_nav_data = 0;

% get date in form of [year, month, week, day, min, sec] from GPS-week
%  "time-of-week"
date = gpstime2date(week, tow);

[Xs, Ys, Zs] = interpolatePreciseOrbits2ECEF(sys, PRN, date, epoch_dates, epochInterval, nEpochs, sat_positions, navGNSSsystems, n_sp3_files, multipleIntervals, firstFileIndices);

if all([Xs,Ys,Zs] == 0)
    missing_nav_data = 1;
    elevation_angle = 0;
else
    % Define vector from receiver to satellite
    dx_e = [Xs,Ys,Zs]'- x_e;

    % Get geodetic coordinates of receiver
    station_x_g = ECEF2geodb(a,b,x_e);

    % transform dx_e vector to local reference frame
    dx_l = ECEF2local(station_x_g, dx_e);

    % Local vector components
    e = dx_l(1);
    n = dx_l(2);
    u = dx_l(3);

    % Calculate elevation angle from receiver to satellite
    elevation_angle = atanc(u, sqrt(e^2+n^2))*180/pi;
end



end