function date = gpstime2date(week, tow)

% Calculates date from GPS-week number and "time-of-week"
%--------------------------------------------------------------------------------------------------------------------------
% INPUTS

% week:     GPS-week number, integer

% tow:      "time-of-week", integer
%--------------------------------------------------------------------------------------------------------------------------

% OUTPUTS

% date:     date in form of [year, month, day, hour, min, sec]
%--------------------------------------------------------------------------------------------------------------------------
%%
hour = floor(tow/3600);
res = tow/3600 - hour;
min = floor(res*60);
res = res*60-min;
sec = res*60;

% if hours is more than 24, extract days built up from hours
days_from_hours = floor(hour/24);
% hours left over
hour = hour - days_from_hours*24;

days_to_start_of_week = week*7;

% Origo of GPS-time: 06/01/1980 
t0 = datetime(1980,1,6);
t1 = t0 + days(days_to_start_of_week+days_from_hours); 

[year, month,day] = ymd(t1);


date = [year, month, day, hour, min, sec];

end