function [X, Y, Z] = interpolatePreciseOrbits2ECEF(sys, PRN, date, dates, epochInterval, nEpochs, sat_positions, navGNSSsystems, n_sp3_files, multipleIntervals, firstFileIndices)

% Function that finds positions of speficied satelite at nearest epochs.
% Then interpolates position at specified time from these epoch positions
%--------------------------------------------------------------------------------------------------------------------------
% INPUTS

% sys:            Satellite system, string, 
%                   ex. "E" or "G"

% PRN:            Satellite identification number, integer


% date:             array, date of specified epoch in form of [year, month, day, hour, min, sec]

% dates:            matrix. Each row contains date of one of the epochs in 
%                   the SP3 orbit file. 
%                   [nEpochs x 6]


% epochInterval:    interval of position epochs in SP3 file, seconds. If
%                   multipleIntervals is 1, then epochInterval is array with
%                   all intervals, length equal to number of SP3 files

% nEpochs:          number of position epochs in SP3 file, integer

% sat_positions:    cell. Each cell elements contains position data for a
%                   specific GNSS system. Order is defined by order of 
%                   navGNSSsystems. Each cell element is another cell that 
%                   stores position data of specific satellites of that 
%                   GNSS system. Each of these cell elements is a matrix 
%                   with [X, Y, Z] position of a epoch in each row.

%                   sat_positions{GNSSsystemIndex}{PRN}(epoch, :) = [X, Y, Z]

% navGNSSsystems:   cell. Contains char. Each string is a code for a
%                   GNSS system with position data stored in sat_positions.
%                   Must be one of: 'G', 'R', 'E', 'C'

% n_sp3_files:          int. number of SP3 files read

% multipleIntervals: boolean. 1 if the SP3 files do not have the same epoch 
%                    interval. 0 otherwise

% firstFileIndices:  array. The indices of sat_positions containing info
%                    about the first epoch of each of the SP3 files loaded.
%                    Length of array is equal to number of SP3 files loaded
%                    Only used if multipleIntervals is 1
%--------------------------------------------------------------------------------------------------------------------------
% OUTPUTS

% X, Y, Z:          ECEF coordinates of satellite at desired time computed
%                   by interpolation 
%--------------------------------------------------------------------------------------------------------------------------


if ~ismember(sys, navGNSSsystems)
  fprintf('ERROR(interpolatePreciseOrbits2ECEF): %s is not in the navigations data', sys) 
   [X, Y, Z] = deal(NaN);
   return
end
% degree of lagrange polynomial to be used
lagrangeDegree = 5;

% Amount of nodes
nNodes = lagrangeDegree + 1;

% Map mapping GNSS system code to GNSS system index
GNSSsystem_map = containers.Map(navGNSSsystems, 1:length(navGNSSsystems));

GNSSsystemIndex = find(navGNSSsystems == sys, 1);
%PRN = str2double(satID(2:end));



% date of first epoch of first and only file
tFirstEpochFirstFile = dates(firstFileIndices(1), :);


if multipleIntervals

   inFile = n_sp3_files;   
   tFirstEpochInFiles = cell(n_sp3_files, 1);
   
   for i = n_sp3_files:-1:1
      tFirstEpochInFiles{i} = dates(firstFileIndices(i), :);
      for j = 1:6
         if date(j)< tFirstEpochInFiles{i}(j)
            inFile = inFile - 1;
            break;
         end
      end
   end
   
   if inFile == 0
      fprintf('ERROR(interpolatePreciseOrbits2ECEF): Requested epoch date is outide of time period of SP3 files'); 
   end
   % time from first epoch in third file to desired epoch
   t_i_k = etime(date, tFirstEpochInFiles{inFile});
   % closest node before desired time
   closestEpochBeforeIndex = floor(t_i_k/epochInterval(inFile)) + 1 + (firstFileIndices(inFile)-1); 

   if inFile > 1
      % time from first epoch to desired epoch
      t_1_k = etime(date,  tFirstEpochInFiles(1));
   else
      % time from first epoch to desired epoch
      t_1_k = t_i_k;
   end
else
   % time from first epoch to desired epoch
   t_1_k = etime(date, tFirstEpochFirstFile);

   % closest node before desired time
   closestEpochBeforeIndex = floor(t_1_k/epochInterval) + 1;
end



% Get the index of the first and last node. If there is not enough epochs
% before or after desired time the degree of lagrange polynomial i reduces,
% as well as number of nodes
node1EpochIndex = closestEpochBeforeIndex - min(nNodes/2 - 1, closestEpochBeforeIndex-1);
diff1 = node1EpochIndex - (closestEpochBeforeIndex - (nNodes/2 - 1));

node8EpochIndex = closestEpochBeforeIndex + min(nNodes/2, nEpochs - closestEpochBeforeIndex);
diff2 = node8EpochIndex - (closestEpochBeforeIndex + nNodes/2);

node1EpochIndex = node1EpochIndex - diff2;
node8EpochIndex = node8EpochIndex - diff1;

% Indices of node epochs
nodeEpochs = node1EpochIndex:node8EpochIndex;

% reduce number of nodes if necessary
nNodes = nNodes - diff1*2 + diff2*2;

% Get positions at each node and relative time
nodePositions = zeros(nNodes, 3);
nodeTimes = zeros(nNodes, 1);
for i = 1:nNodes
   nodePositions(i, :) = sat_positions{GNSSsystemIndex}{PRN}(nodeEpochs(i), :);
   nodeTimes(i) = etime(dates(nodeEpochs(i), :), tFirstEpochFirstFile);
end

% Interpolate new posistion of satellite using a lagrange polynomial
X=barylag([nodeTimes, nodePositions(:, 1)], t_1_k);
Y=barylag([nodeTimes, nodePositions(:, 2)], t_1_k);
Z=barylag([nodeTimes, nodePositions(:, 3)], t_1_k);


% if user wants to plot node positions and interpolated positions, set
% plot_result to 1

plot_result = 0;

if plot_result
   
   figure, hold on
   plot((nodeTimes-nodeTimes(1))/60, nodePositions(:,1), 'r*');
   plot((t_1_k-nodeTimes(1))/60, X, 'g*');
   xlabel('Time (relative) [minutes]')
   ylabel('X [m]')
   title('Interpolated X value')
   legend('Node values', 'Interpolated value')

   figure, hold on
   plot((nodeTimes-nodeTimes(1))/60, nodePositions(:,2), 'r*');
   plot((t_1_k-nodeTimes(1))/60, Y, 'g*');
   xlabel('Time (relative) [minutes]')
   ylabel('Y [m]')
   title('Interpolated Y value')
   legend('Node values', 'Interpolated value')
   
   figure, hold on
   plot((nodeTimes-nodeTimes(1))/60, nodePositions(:,3), 'r*');
   plot((t_1_k-nodeTimes(1))/60, Z, 'g*');
   xlabel('Time (relative) [minutes]')
   ylabel('Z [m]')
   title('Interpolated Z value')
   legend('Node values', 'Interpolated value')
end

end