function plotResults(ion_delay_phase1, multipath_range1, sat_elevation_angles,... 
    tInterval, currentGNSSsystem, range1_Code, range2_Code, phase1_Code, phase2_Code, graphDir)

% Function that plots the results of estimates of delay on signals. The
% following plots are made:
%                           ionosphere delay on phase 1 signal vs time

%                           zenit mapped ionosphere delay on phase 1 
%                           signal vs time

%                           multipath delay on range 1 signal vs time

%                           multipath delay on range 1 signal vs elevation
%                           angle

%                           a combined plot of both multipath plots. This
%                           plot is also saved
%--------------------------------------------------------------------------------------------------------------------------
% INPUTS

% ion_delay_phase1:     matrix containing estimates of ionospheric delays 
%                       on the first phase signal for each PRN, at each epoch.

%                       ion_delay_phase1(epoch, PRN)

% multipath_range1:     matrix containing estimates of multipath delays 
%                       on the first range signal for each PRN, at each epoch.

%                       multipath_range1(epoch, PRN)

% sat_elevation_angles: Array contaning satellite elevation angles at each
%                       epoch, for current GNSS system. 

%                       sat_elevation_angles(epoch, PRN)

% tInterval:            observations interval; seconds. 

% currentGNSSsystem:    string containing code for current GNSS system,
%                       ex. "G" or "E"

% range1_Code:          string, defines the observation type that will be 
%                       used as the first range observation. ex. "C1X", "C5X"    

% range2_Code:          string, defines the observation type that will be 
%                       used as the second range observation. ex. "C1X", "C5X"

% phase1_Code:          string, defines the observation type that will be 
%                       used as the first phase observation. ex. "L1X", "L5X"

% phase2_Code:          string, defines the observation type that will be 
%                       used as the second phase observation. ex. "L1X", "L5X"

% graphDir:             string. Gives path to where figure the combined
%                       multipath figure should be saved.
%--------------------------------------------------------------------------------------------------------------------------

[n,m] = size(ion_delay_phase1);


% Map mapping GNSS system code to full name
GNSSsystemName_map = containers.Map({'G', 'R', 'E', 'C'}, {'GPS', 'GLONASS', 'Galileo', 'BeiDou'});
GNSSsystemName = GNSSsystemName_map(currentGNSSsystem);

% time stamps
t = (1:n)*tInterval;

%% Plot ionosphere delay vs time

fig4 = figure; 
set(gca, 'FontSize', 12)
grid on, hold on
for PRN = 1:m
    % only plot for PRN that have any observations
    if ~all(isnan(ion_delay_phase1(:,PRN)))
        plot(t, ion_delay_phase1(:,PRN), 'DisplayName', ['PRN' num2str(PRN)]) 
    end
end
xline = line(xlim, [0,0], 'Color', 'k', 'LineWidth', 1); % Draw line for X axis.
set(get(get(xline,'Annotation'),'LegendInformation'),'IconDisplayStyle','off');

l(3) = legend(gca,'show', 'Location', 'northeastoutside', 'NumColumns', 2);
xl(3) = xlabel('t [seconds]', 'FontSize', 19);
yl(3) = ylabel('Ionosphere delay [m]', 'FontSize', 19);
tit(3) = title(sprintf('Relative ionosphere delay vs time, %s\nPhase 1: %s\t\t Phase 2: %s',...
    GNSSsystemName, phase1_Code, phase2_Code), 'FontSize', 17);
%% Plot zenit mapped ionosphere delay vs time

ion_delay_phase1_zenit = ion_delay_phase1.*cos(asin(6371000/(6371000 + 450000) * sin((90-sat_elevation_angles)*pi/180)));

excluded_PRN = {};

fig5 = figure;
set(gca, 'FontSize', 12)
grid on, hold on
for PRN = 1:m
    % only plot for PRN that have any observations
    if ~all(isnan(ion_delay_phase1(:,PRN))) 
        if ~any(isnan(sat_elevation_angles(:, PRN)))
            plot(t, ion_delay_phase1_zenit(:, PRN), 'DisplayName', ['PRN' num2str(PRN)]) 
        else
            excluded_PRN = [excluded_PRN, num2str(PRN)+", "];
        end
    end
end
xline = line(xlim, [0,0], 'Color', 'k', 'LineWidth', 1); % Draw line for X axis.
set(get(get(xline,'Annotation'),'LegendInformation'),'IconDisplayStyle','off');

l(4) = legend(gca,'show', 'Location', 'northeastoutside', 'NumColumns', 2);
xl(4) = xlabel('t [seconds]', 'FontSize', 19);
yl(4) = ylabel('Ionosphere delay [m]', 'FontSize', 19);
if isempty(excluded_PRN)
    tit(4) = title(sprintf('Relative zenit mapped ionosphere delay vs time, %s\nPhase 1: %s\t\t Phase 2: %s',...
        GNSSsystemName, phase1_Code, phase2_Code), 'FontSize', 17);
else
    tit(4) = title(sprintf(['Relative zenit mapped ionosphere delay vs time, %s\nPhase 1: %s\t\t Phase 2: %s\n',...
        'Excluded PRN: %s'],GNSSsystemName, phase1_Code, phase2_Code, cell2mat(excluded_PRN)), 'FontSize', 17);
end
%% Plot multipath delay on range 1 signal vs time

fig1 = figure; 
set(gca, 'FontSize', 12)
grid on, hold on
for PRN = 1:m
    % only plot for PRN that have any observations
    if ~all(isnan(multipath_range1(:,PRN)))
        plot(t, multipath_range1(:,PRN), 'DisplayName', ['PRN' num2str(PRN)]) 
    end
end
xline = line(xlim, [0,0], 'Color', 'k', 'LineWidth', 1); % Draw line for X axis.
set(get(get(xline,'Annotation'),'LegendInformation'),'IconDisplayStyle','off');

% set y lim to mean values pluss minus 7 std
h = findobj(fig1, 'Type', 'line');
yvalues = get(h, 'Ydata');
if isa(yvalues, 'cell')
   yvalues = cat(2, yvalues{:});
   yvalues = yvalues(:);
   yvalues = yvalues(~isnan(yvalues));
else
   yvalues = [0, 1];
end

mean_y  = mean(yvalues);
std_y   = std(yvalues);
ylim([mean_y-7*std_y, mean_y+7*std_y]);

y_limit{1} = ylim;
l(1) = legend(gca,'show', 'Location', 'northeastoutside', 'NumColumns', 2);
xl(1) = xlabel('t [seconds]', 'FontSize', 19);
yl(1) = ylabel('Multipath delay [m]', 'FontSize', 19);
tit(1) = title(sprintf('Relative multipath delay vs time on %s signal, %s\nPhase 1: %s \t\t Phase 2: %s',...
    range1_Code, GNSSsystemName, phase1_Code, phase2_Code), 'FontSize', 17);
%% Plot multipath delay on range 1 signal vs elevation angles

excluded_PRN = {};

fig2 = figure; 
set(gca, 'FontSize', 12)
grid on, hold on
for PRN = 1:m
    % only plot for PRN that have any observations
    if ~all(isnan(multipath_range1(:,PRN))) 
        if ~any(isnan(sat_elevation_angles(:, PRN)))
            plot(sat_elevation_angles(:, PRN), multipath_range1(:,PRN), 'DisplayName', ['PRN' num2str(PRN)]) 
        else
            excluded_PRN = [excluded_PRN, num2str(PRN)+", "];
        end      
    end
end
xline = line(xlim, [0,0], 'Color', 'k', 'LineWidth', 1); % Draw line for X axis.
set(get(get(xline,'Annotation'),'LegendInformation'),'IconDisplayStyle','off');

% set y lim to mean values pluss minus 2 std
h = findobj(fig2, 'Type', 'line');
yvalues = get(h, 'Ydata');
if isa(yvalues, 'cell')
   yvalues = cat(2, yvalues{:});
   yvalues = yvalues(:);
   yvalues = yvalues(~isnan(yvalues));
else
   yvalues = [0, 1];
end
mean_y  = mean(yvalues);
std_y   = std(yvalues);
ylim([mean_y-7*std_y, mean_y+7*std_y]);

y_limit{2} = ylim;
l(2) = legend(gca,'show', 'Location', 'northeastoutside', 'NumColumns', 2);
xl(2) = xlabel('Satellite elevation angles [degrees]', 'FontSize', 19);
yl(2) = ylabel('Multipath delay [m]', 'FontSize', 19);

if isempty(excluded_PRN)
    tit(2) = title(sprintf('Relative multipath delay vs satellite elevation angle on %s signal, %s\nPhase 1: %s\t\t Phase 2: %s',...
        range1_Code, GNSSsystemName, phase1_Code, phase2_Code), 'FontSize', 17);
else
    tit(2) = title(sprintf(['Relative multipath delay vs satellite elevation angle on %s signal, %s\nPhase 1: %s\t\t Phase 2: %s\n',...
        'Excluded PRN: %s'],range1_Code, GNSSsystemName, phase1_Code, phase2_Code, cell2mat(excluded_PRN)), 'FontSize', 17);
end

%% Combine multipath delay on range 1 signal plots together

fig3 = figure('Position', [680 678 521 401]);
set(gca, 'FontSize', 12)
set(gca, 'FontSize', 12)
h(1) = subplot(2,1,1);
h(2) = subplot(2,1,2);

copyobj(allchild(get(fig1,'CurrentAxes')),h(1));
copyobj(allchild(get(fig2,'CurrentAxes')),h(2));

legend(h(1), l(1).String, 'Location', 'northeastoutside', 'NumColumns', 2)
legend(h(2), l(2).String, 'Location', 'northeastoutside', 'NumColumns', 2)
xlabel(h(1), xl(1).String, 'FontSize', 19)
xlabel(h(2), xl(2).String, 'FontSize', 19)
ylabel(h(1), yl(1).String, 'FontSize', 19)
ylabel(h(2), yl(2).String, 'FontSize', 19)
title(h(1), tit(1).String, 'FontSize', 17)
title(h(2), tit(2).String, 'FontSize', 17)

% save figure
filename = sprintf('%s_%s_%s_Multipath_Plot.pdf', GNSSsystemName, range1_Code, range2_Code);
full_filename = strcat(graphDir ,'/', filename);

set(fig3,'PaperOrientation','landscape');
set(fig3,'PaperUnits','normalized');
set(fig3,'PaperPosition', [0 0 1 1]);
print(fig3, '-dpdf', full_filename);

ylim(h(1), y_limit{1})
ylim(h(2), y_limit{2})

% save figure after changing y limit
filename = sprintf('%s_%s_%s_Multipath_Plot_Cropped.pdf', GNSSsystemName, range1_Code, range2_Code);
full_filename = strcat(graphDir ,'/', filename);

set(fig3,'PaperOrientation','landscape');
set(fig3,'PaperUnits','normalized');
set(fig3,'PaperPosition', [0 0 1 1]);
print(fig3, '-dpdf', full_filename);

%% Combine ionospheric delay plots together

fig6 = figure('Position', [680 678 521 401]);
set(gca, 'FontSize', 12)
h(3) = subplot(2,1,1);
h(4) = subplot(2,1,2);

copyobj(allchild(get(fig4,'CurrentAxes')),h(3));
copyobj(allchild(get(fig5,'CurrentAxes')),h(4));

legend(h(3), l(3).String, 'Location', 'northeastoutside', 'NumColumns', 2)
legend(h(4), l(4).String, 'Location', 'northeastoutside', 'NumColumns', 2)
xlabel(h(3), xl(3).String, 'FontSize', 19)
xlabel(h(4), xl(4).String, 'FontSize', 19)
ylabel(h(3), yl(3).String, 'FontSize', 19)
ylabel(h(4), yl(4).String, 'FontSize', 19)
title(h(3), tit(3).String, 'FontSize', 17)
title(h(4), tit(4).String, 'FontSize', 17)


% save figure
filename = sprintf('%s_%s_%s_Ionosphere_Plot.pdf', GNSSsystemName, phase1_Code, phase2_Code);
full_filename = strcat(graphDir ,'/', filename);

set(fig6,'PaperOrientation','landscape');
set(fig6,'PaperUnits','normalized');
set(fig6,'PaperPosition', [0 0 1 1]);
print(fig6, '-dpdf', full_filename);
 
end