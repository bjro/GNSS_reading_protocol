function [X, Y, Z] = preciseOrbits2ECEF(sys, PRN, date, dates, epochInterval, nEpochs, sat_positions, navGNSSsystems)

% Function that finds positions of speficied satelite at nearest epochs.
% Then interpolates position at specified time from these epoch positions
%--------------------------------------------------------------------------------------------------------------------------
% INPUTS

% sys:            Satellite system, string, 
%                   ex. "E" or "G"

% PRN:            Satellite identification number, integer

% date:             array, date of specified epoch in form of [year, month, day, hour, min, sec]

% dates:            matrix. Each row contains date of one of the epochs in 
%                   the SP3 orbit file. 
%                   [nEpochs x 6]


% epochInterval:    interval of position epochs in SP3 file, seconds

% nEpochs:          number of position epochs in SP3 file, integer

% sat_positions:    cell. Each cell elements contains position data for a
%                   specific GNSS system. Order is defined by order of 
%                   navGNSSsystems. Each cell element is another cell that 
%                   stores position data of specific satellites of that 
%                   GNSS system. Each of these cell elements is a matrix 
%                   with [X, Y, Z] position of a epoch in each row.

%                   sat_positions{GNSSsystemIndex}{PRN}(epoch, :) = [X, Y, Z]

% navGNSSsystems:   cell. Contains char. Each string is a code for a
%                   GNSS system with position data stored in sat_positions.
%                   Must be one of: 'G', 'R', 'E', 'C'
%--------------------------------------------------------------------------------------------------------------------------
% OUTPUTS

% X, Y, Z:          ECEF coordinates of satellite at desired time computed
%                   by interpolation 
%--------------------------------------------------------------------------------------------------------------------------



% degree of lagrange polynomial to be used
lagrangeDegree = 3;

% Amount of nodes
nNodes = lagrangeDegree + 1;

GNSSsystemIndex = find(navGNSSsystems == sys, 1);
%PRN = str2double(satID(2:end));

% date of first epoch
tFirstEpoch = dates(1, :);

% time from first epoch to desired epoch
tk = etime(date, tFirstEpoch);

% closest node before desired time
closestEpochBeforeIndex = floor(tk/epochInterval) + 1;

% Get the index of the first and last node. If there is not enough epochs
% before or after desired time the degree of lagrange polynomial i reduces,
% as well as number of nodes
node1EpochIndex = closestEpochBeforeIndex - min(nNodes/2 - 1, closestEpochBeforeIndex-1);
diff1 = node1EpochIndex - (closestEpochBeforeIndex - (nNodes/2 - 1));

node8EpochIndex = closestEpochBeforeIndex + min(nNodes/2, nEpochs - closestEpochBeforeIndex);
diff2 = node8EpochIndex - (closestEpochBeforeIndex + nNodes/2);

node1EpochIndex = node1EpochIndex - diff2;
node8EpochIndex = node8EpochIndex - diff1;

% Indices of node epochs
nodeEpochs = node1EpochIndex:node8EpochIndex;

% reduce number of nodes if necessary
nNodes = nNodes - diff1*2 + diff2*2;

% Get positions at each node and relative time
nodePositions = zeros(nNodes, 3);
nodeTimes = zeros(nNodes, 1);
for i = 1:nNodes
   nodePositions(i, :) = sat_positions{GNSSsystemIndex}{PRN}(nodeEpochs(i), :);
   nodeTimes(i) = etime(dates(nodeEpochs(i), :), tFirstEpoch);
end

% Interpolate new posistion of satellite using a lagrange polynomial
X=barylag([nodeTimes, nodePositions(:, 1)], tk);
Y=barylag([nodeTimes, nodePositions(:, 2)], tk);
Z=barylag([nodeTimes, nodePositions(:, 3)], tk);

end