function [frequencyOverview, GNSSsystems, success] = readFrequencyOverview(filename)

% Function that reads current frequencies of GNSS satellite carrier bands,
% from a text file.
% The band numbers follow the conventions of RINEX 3.
%--------------------------------------------------------------------------------------------------------------------------
% INPUTS:

% filename:             string, path and filename of frequency overview file             
%--------------------------------------------------------------------------------------------------------------------------
% OUTPUTS:

% frequencyOverview:    cell. Each elements conatins frequencies of one
%                       GNSS system. The order is decided by GNSSsystems.
%                       If the cell is for any system but GLONASS, the cell
%                       cotains an array with the frequency of band i at
%                       index i.

%                       frequencyOverview{GNSSsystemIndex}(bandnumber)
%                       unless GLONASS

%                       for GLONASS, the cell a matrix with two columns.
%                       First column gives core frequency of that band.
%                       Second column gives increment frequency of that
%                       band. 

% gnssSystems:          cell. Contains codes for each GNSS system. Must be
%                       either 'G', 'R', 'E', 'C'

% success:              boolean, 0 if error is thrown, 1 otherwise
%--------------------------------------------------------------------------------------------------------------------------
%%
success = 1;
fid = fopen(filename, 'r');

% Initialize variables
nGNSSsystems = 0;
GNSSsystems = {};
frequencyOverview = {};

% If invalid filename
if fid==-1
    disp('ERROR(readFrequencyOverview): Frequency overview filename does not exist!')
    success = 0;
    return
end

line = fgetl(fid);

% continue until end of file marker reached
while ~contains(line, 'eof')
    % read past header. first line after header has >
    while ~contains(line, '>')
        
        line = fgetl(fid);
        if line==-1
            disp('ERROR(readFrequencyOverview): End of file reached unexpectedly!')
            success = 0;
            return
        end
    end
    
    % increment number of GNSS systems
    nGNSSsystems = nGNSSsystems + 1;
    % Store current GNSS system
    GNSSsystems{nGNSSsystems} = string(line(2));
    
    % check that current GNSS system is a valid system
    currentGNSSsystem = GNSSsystems{nGNSSsystems};
    if ismember(currentGNSSsystem, ["G", "R", "E", "C"])
        % if current system is GLONASS, read core frequency and increment
        % frequencies
        if ~strcmp(currentGNSSsystem, "R")
            frequencyOverview{nGNSSsystems} = zeros(9,1);

            for k = 1:9
                line = fgetl(fid);
                line = line(3:end);

                frequencyOverview{nGNSSsystems}(k) = str2double(strtok(line));
            end
        % unlees current system is GLONASS, read frequencies.
        else
            frequencyOverview{nGNSSsystems} = zeros(9,2);

            for k = 1:9
                line = fgetl(fid);
                line = line(3:end);

                [freq, line] = strtok(line);
                dFreq = strtok(line);
                frequencyOverview{nGNSSsystems}(k,:) = [str2double(freq), str2double(dFreq)];
            end
        end
    end
    
    % get next line
    line = fgetl(fid);
end

disp('INFO(readFrequencyOverview): Frequency overview file has been read')

end