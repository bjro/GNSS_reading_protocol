function [sat_positions, epoch_dates, navGNSSsystems, nEpochs, epochInterval, success] = readSP3Nav(filename, desiredGNSSsystems)

% Function that reads the GNSS satellite position data from a SP3 position
% file. The function has been tested with sp3c and sp3d. NOTE: It is
% advised that any use of this function is made through the parent function
% "read_multiple_SP3Nav.m", as it has more functionality. 
%--------------------------------------------------------------------------------------------------------------------------
% INPUTS

% filename:             path and filename of sp3 position file, string

% desiredGNSSsystems:   array. Contains string. Each string is a code for a
%                       GNSS system that should have its position data stored 
%                       in sat_positions. Must be one of: "G", "R", "E",
%                       "C". If left undefined, it is automatically set to
%                       ["G", "R", "E", "C"]
%--------------------------------------------------------------------------------------------------------------------------
% OUTPUTS

% sat_positions:    cell. Each cell elements contains position data for a
%                   specific GNSS system. Order is defined by order of 
%                   navGNSSsystems. Each cell element is another cell that 
%                   stores position data of specific satellites of that 
%                   GNSS system. Each of these cell elements is a matrix 
%                   with [X, Y, Z] position of a epoch in each row.

%                   sat_positions{GNSSsystemIndex}{PRN}(epoch, :) = [X, Y, Z]

% epoch_dates:      matrix. Each row contains date of one of the epochs. 
%                   [nEpochs x 6]

% navGNSSsystems:   array. Contains string. Each string is a code for a
%                   GNSS system with position data stored in sat_positions.
%                   Must be one of: "G", "R", "E", "C"

% nEpochs:          number of position epochs, integer

% epochInterval:    interval of position epochs, seconds

% success:          boolean, 1 if no error occurs, 0 otherwise
%--------------------------------------------------------------------------------------------------------------------------

max_GNSSsystems = 4;

max_GPS_PRN     = 36; % Max number of GPS PRN in constellation
max_GLONASS_PRN = 36; % Max number of GLONASS PRN in constellation
max_Galileo_PRN = 36; % Max number of Galileo PRN in constellation
max_Beidou_PRN  = 60; % Max number of BeiDou PRN in constellation
max_sat = [max_GPS_PRN, max_GLONASS_PRN, max_Galileo_PRN, max_Beidou_PRN];

% Initialize variables
success = 1;

% Open nav file
fid=fopen(filename,'r');

% GNSS system order
navGNSSsystems = ["G", "R", "E", "C"];

% Map mapping GNSS system code to GNSS system index
GNSSsystem_map = containers.Map(navGNSSsystems, [1, 2, 3, 4]);

if nargin==1
   desiredGNSSsystems = ["G", "R", "E", "C"];
end

% If invalid filename
if fid==-1
    disp('ERROR(readSP3Nav): SP3 Navigation filename does not exist!')
    [sat_positions, epoch_dates, navGNSSsystems, nEpochs, epochInterval] = deal(NaN);
    success = 0;
    return
end

% Gobble up header
headerLine = 0;
line = fgetl(fid);

% all header lines begin with '*'
while ~strcmp(line(1), '*')
    headerLine = headerLine + 1;
    
    if headerLine == 1
       sp3Version = line(1:2);
       
       % control sp3 version
       if ~strcmp(sp3Version, '#c') && ~strcmp(sp3Version, '#d')
           sprintf(['ERROR(readSP3Nav): SP3 Navigation file is version %s, must be version c or d!'], sp3Version);
           [sat_positions, epoch_dates, navGNSSsystems, nEpochs, epochInterval] = deal(NaN);
           success = 0;
           return
       end
       
       % control that sp3 file is a position file and not a velocity file
       Pos_Vel_Flag = line(3);
       if ~strcmp(Pos_Vel_Flag, 'P')
           disp('ERROR(readSP3Nav): SP3 Navigation file is has velocity flag, should have position flag!');
           [sat_positions, epoch_dates, navGNSSsystems, nEpochs, epochInterval] = deal(NaN);
           success = 0;
           return
       end
       
       % Store coordinate system and amount of epochs
       CoordSys = line(47:51);
       nEpochs = str2double(line(33:39));
    end
    
    if headerLine == 2
        % Store GPS-week, "time-of-week" and epoch interval[seconds]
       GPS_Week = str2double(line(4:7));
       tow      = str2double(line(9:23));
       epochInterval = str2double(line(25:38));
    end
    
    if headerLine == 3
       
       % initialize array for storing indices of satellites to be excluded
       RemovedSatIndex = [];
       
       % store amount of satellites in each epoch, including undesired ones
       if strcmp(sp3Version, '#c')
        nSat = str2double(line(5:6)); 
       else
        nSat = str2double(line(4:6)); 
       end
       
       % remove beginning of line
       line = line(10:60);
       
       % Initialize array for storing the order of satellites in the SP3
       % file(ie. what PRN and GNSS system index)
       GNSSsystemIndexOrder  = zeros(1, nSat);
       PRNOrder         = zeros(1, nSat);
       
       % Keep reading lines until all satellite IDs have been read
       for k = 1:nSat
          
          % control that current satellite is amoung desired systems
          if ismember(line(1), desiredGNSSsystems)
              % Get GNSSsystemIndex from map container
              GNSSsystemIndex = GNSSsystem_map(line(1));
              % Get PRN number/slot number
              PRN = str2double(line(2:3));
              
              % remove satellite that has been read from line
              line = line(4:end);

              % Store GNSSsystemIndex and PRN in satellite order arrays
              GNSSsystemIndexOrder(k) = GNSSsystemIndex;
              PRNOrder(k) = PRN;
              
              % if current satellite ID was last of a line, read next line
              % and increment number of headerlines
              if mod(k,17)==0
                  line = fgetl(fid);
                  line = line(10:60);
                  headerLine = headerLine + 1;
              end
          % If current satellite ID is not amoung desired GNSS systems,
          % append its index to array of undesired satellites
          else
              RemovedSatIndex = [RemovedSatIndex, k];
              GNSSsystemIndexOrder(k) = NaN;
              PRNOrder(k) = NaN;
              
              % if current satellite ID was last of a line, read next line
              % and increment number of headerlines
              if mod(k,17)==0
                  line = fgetl(fid);
                  line = line(10:60);
                  headerLine = headerLine + 1;
              end
          end
       end
    end
    
    
    % read next line
    line = fgetl(fid);
end


% Initialize matrix for epoch dates
epoch_dates = zeros(nEpochs, 6);

% initialize cell structure for storing satellite positions
sat_positions = cell(1, max_GNSSsystems);
for k = 1:max_GNSSsystems
   sat_positions{k} = cell(1, max_sat(k));
   for j = 1:max_sat(k)
      sat_positions{k}{j} = zeros(nEpochs, 3); 
   end
end

% read satellite positions of every epoch
for k = 1:nEpochs
    % Store date of current epoch
    epoch_dates(k, :) = sscanf(line(4:31),'%d%d%d%d%d%f')';
    
    % store positions of all satellites for current epoch
    for i = 1:nSat
       % read next line
       line = fgetl(fid);
       
       % if current satellite is amoung desired systems, store positions
       if ~ismember(i, RemovedSatIndex)

           % Get PRN and GNSSsystemIndex of current satellite for
           % previously stored order
           PRN = PRNOrder(i);
           GNSSsystemIndex = GNSSsystemIndexOrder(i);
           
           % store position of current satellite in correct location in
           % cell structure
           sat_positions{GNSSsystemIndex}{PRN}(k, :) = 1e3 * sscanf(line(5:46),'%f%f%f')';
       end
    end
    
    % Get next line
    line = fgetl(fid);
end

% the next line should be eof. If not, raise warning
line = fgetl(fid);
if ~line == -1
    disp('ERROR(readSP3Nav): End of file was not reached when expected!!');
    success = 0;
    return
end

% remove NaN values
GNSSsystemIndexOrder(isnan(GNSSsystemIndexOrder)) = [];
PRNOrder(isnan(PRNOrder)) = [];

% remove GNSS systems not present in navigation file
sat_positions  = sat_positions(unique(GNSSsystemIndexOrder));
navGNSSsystems = navGNSSsystems(unique(GNSSsystemIndexOrder));

disp('INFO(readSP3Nav): SP3 Navigation text file has been read.')

end