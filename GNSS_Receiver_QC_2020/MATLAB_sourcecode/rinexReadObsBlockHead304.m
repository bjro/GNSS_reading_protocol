function [success, epochflag, clockOffset, date, numSV, eof] = ...
rinexReadObsBlockHead304(fid)
% Reads the metadata in the head of a RINEX 3.xx observations block, NOT 
% the header of the file.
%
% ATTENTION: Ignores all data in blocks with event flags with numbers
% greater than 1!!!
%
% Positioned in a RINEX 3.04 GNSS observations text file at the beginning
% of an observation block. In rinex 3.xx the line starts with '> '

% Based on the work of Ant�nio Pestana, rinexReadObsBlockHead211.m,
%--------------------------------------------------------------------------------------------------------------------------
% INPUTS

% fid:              Matlab identifier of an open RINEX 3.04 GNSS 
%                   observations text file positioned at the beginning
%                   of an observation block.
%--------------------------------------------------------------------------------------------------------------------------
% OUTPUTS

% success:          1 if function performs successfully, 0 otherwise
% 
% epochflag:        Rinex observations epoch flag, as follows:
%                       0: OK
%                       1: power failure between previous and current epoch
%                   From now on the "event flags":
%                       2: start moving antenna
%                       3: new site occupation
%                       4: header information follows
%                       5: external event (epoch is significant)

% clockOffset:          value of the receiver clock offset. If not present 
%                       in the metadata of the observations block 
%                       (it's optional RINEX 3.04 data)it is assumed to be 
%                       zero. If not zero implies that epoch, code, and 
%                       phase data have been corrected by applying 
%                       realtime-derived receiver clock offset

% date:                 time stamp of the observations block. Six-elements column-vector
%                       as follows:
%                           year: four-digits year (eg: 1959)
%                           month: integers 1..12
%                           day: integers 1..31
%                           hour: integers 0..24
%                           minute: integers 0..60
%                           second: reals 0..60

% numSV:                number of satellites with observations in with
%                       observations. This will include all satellite
%                       systems.
%--------------------------------------------------------------------------------------------------------------------------
%%

% Initialize variables
success = 1     ;
eof     = 0     ;
date    = []    ;
numSV   = 0     ;
epochflag   = [];
clockOffset = [];
noFlag = 1;


line = fgetl(fid); % returns -1 if only reads EOF

% end of observation file is reached at an expected point
if line == -1
  eof = 1;
  disp(['INFO(rinexReadObsBlockHead304): End of observations '...
        'text file reached'])
  return
end

% The first thing to do: the reading of the epoch flag
epochflag   = str2double(line(32));

% skip to next block if event flag is more than 1
while epochflag > 1
  noFlag = 0;  
  linejump = str2double(line(33:35));
  msg = sprintf('WARNING(rinexReadsObsBlockHead304): Observations event flag encountered. Flag = %d. %d lines were ignored.', epochflag, linejump);
   
for count=1:linejump + 1 % skip over current obs block and go to next one
  line = fgetl(fid);
end
epochflag = str2double(line(31));
end

% Gets the number of used satellites in obs epoch
numSV = str2double(line(33:35)); 

% Gets the receiver clock offset. This is optional data!
clockOffset = 0;
if size(line,2) == 56
  clockOffset = str2double(line(42:56));
end

% Reads the time stamp of the observations block (6 numerical values)
date = cell2mat(textscan(line(2:end),'%f', 6));

if ~noFlag
   msg = append(msg, sprintf(' Epoch date = %4d/%2d/%2d %2d:%2d:%6.4f', date(1),date(2),date(3),date(4),date(5),date(6)));
   disp(msg)
end
end