function [currentStats, success] = ...
    signalAnalysis(currentGNSSsystem, range1_Code, range2_Code, GNSSsystems, frequencyOverview, nepochs, ...
    tInterval, current_max_sat, current_GNSS_SVs, current_obsCodes, current_GNSS_obs, current_GNSS_LLI, current_sat_elevation_angles,...
    phaseCodeLimit, ionLimit, cutoff_elevation_angle)

% Function that executes a signal analysis on a specific GNSS code range
% signal for a specific GNSS system. Function computes statistics on

%--------------------------------------------------------------------------------------------------------------------------
% INPUTS:

% currentGNSSsystem:            string. Code that gives current GNSS
%                               system.
%                               ex. "G" or "E"

% range1_Code:                  string. obs code for first code pseudorange
%                               observation

% range2_Code:                  string. obs code for second code pseudorange
%                               observation

% GNSSsystems:                  cell array containing codes of GNSS systems. 
%                               Elements are strings.
%                               ex. "G" or "E"

% frequencyOverview:            cell. each elements contains carrier band
%                               frequenies for a specific GNSS system.
%                               Order is set by GNSSsystems. If system is
%                               GLONASS then element is matrix where each
%                               row i gives carrier band i frequencies for
%                               all GLONASS SVs. If not GLONASS, element is
%                               array with one frequency for each carrier
%                               band.

% nepochs:                      nepochs of observations in RINEX
%                               observation file. 

% tInterval:                    observation interval in seconds

% current_max_sat:              max PRN number of current GNSS system

% current_GNSS_SVs:             cell containing a matrix for current GNSS system.
%                               Each matrix contains number of satellites with 
%                               obsevations for each epoch, and PRN for those 
%                               satellites

%                               GNSS_SVs(epoch, j)  
%                                       j=1: number of observed satellites
%                                       j>1: PRN of observed satellites

% current_obsCodes:             Cell that defines the observation
%                               codes available in current GNSS system. 
%                               Each element in this cell is a
%                               string with three-characters. The first 
%                               character (a capital letter) is an observation code 
%                               ex. "L" or "C". The second character (a digit)
%                               is a frequency code. The third character(a Capital letter)  
%                               is the attribute, ex. "P" or "X"

% current_GNSS_obs:             3D matrix  containing all observation of 
%                               current GNSS system for all epochs. 
%                               Order of obsType index is same order as in 
%                               current_obsCodes

%                               current_GNSS_obs(PRN, obsType, epoch)
%                                           PRN: double
%                                           ObsType: double: 1,2,...,numObsTypes
%                                           epoch: double

% current_GNSS_LLI:             3D matrix  containing all Loss of Lock 
%                               indicators of current GNSS system for all epochs. 
%                               Order of obsType index is same order as in 
%                               current_obsCodes

%                               current_GNSS_LLI(PRN, obsType, epoch)
%                                           PRN: double
%                                           ObsType: double: 1,2,...,numObsTypes
%                                           epoch: double

% current_sat_elevation_angles: matrix contaning satellite elevation angles 
%                               at each epoch, for current GNSS system. 

%                               sat_elevation_angles(epoch, PRN)

% phaseCodeLimit:               critical limit that indicates cycle slip for
%                               phase-code combination. Unit: m/s. If set to 0,
%                               default value will be used

% ionLimit:                     critical limit that indicates cycle slip for
%                               the rate of change of the ionopheric delay. 
%                               Unit: m/s. If set to 0, default value will be used

% cutoff_elevation_angle        Critical cutoff angle for satellite elevation angles, degrees
%                               Estimates where satellite elevation angle
%                               is lower than cutoff are removed, so are
%                               estimated slip periods
%--------------------------------------------------------------------------------------------------------------------------
% OUPUTS:

% currentStats:                 struct. Contains statitics from analysis
%                               executed. More detail of each stattistic
%                               is given in function computeDelayStats.m

% success:                      boolean. 1 if no errors thrown, 0 otherwise
%--------------------------------------------------------------------------------------------------------------------------

% get corrosponding ohase codes to the range codes
phase1_Code = "L"+extractAfter(range1_Code, 1);
phase2_Code = "L"+extractAfter(range2_Code, 1);

% Get current GNSS system index
GNSSsystemIndex = find([GNSSsystems{:}] == currentGNSSsystem); % index of current system

% Get freuencies of carrier bands. These are arrays if current GNSS system
% is GLONASS. Each element for a specific GLONASS SV.
carrier_freq1 = frequencyOverview{GNSSsystemIndex}(str2double(extractBetween(range1_Code, 2, 2)), :);
carrier_freq2 = frequencyOverview{GNSSsystemIndex}(str2double(extractBetween(range2_Code, 2, 2)), :);

% Run function to compute estimates of ionospheric delay, multipath delays,
% slip periods of range1 signal.
[ion_delay_phase1, multipath_range1, ~, range1_slip_periods, range1_observations, phase1_observations, success] = estimateSignalDelays(range1_Code, range2_Code, ...
    phase1_Code, phase2_Code, carrier_freq1, carrier_freq2,nepochs, current_max_sat,...
     current_GNSS_SVs, current_obsCodes, current_GNSS_obs, currentGNSSsystem, tInterval, phaseCodeLimit, ionLimit);
 



% create logical mask for epochs where sat elevation is lower than cutoff
% or missing
cutoff_elevation_mask = current_sat_elevation_angles;
cutoff_elevation_mask(cutoff_elevation_mask<cutoff_elevation_angle) = 0;
cutoff_elevation_mask(cutoff_elevation_mask>=cutoff_elevation_angle) = 1;

% apply satellite elevation cutoff mask to estimates
ion_delay_phase1 = ion_delay_phase1 .* cutoff_elevation_mask;
multipath_range1 = multipath_range1 .* cutoff_elevation_mask;
range1_observations = range1_observations .* cutoff_elevation_mask;
phase1_observations = phase1_observations .* cutoff_elevation_mask;



% remove estimated slip periods if satellite elevation angle was lower than
% cutoff or missing.
for sat = 1:length(range1_slip_periods)
   current_sat_slip_periods = range1_slip_periods{sat};
   [n_slip_periods, ~] = size(current_sat_slip_periods);
   n_slips_removed = 0;
   for slip_period = 1:n_slip_periods
      if cutoff_elevation_mask(current_sat_slip_periods(slip_period-n_slips_removed, 1), sat) == 0 || cutoff_elevation_mask(current_sat_slip_periods(slip_period-n_slips_removed, 2), sat) == 0
         current_sat_slip_periods(slip_period - n_slips_removed, :) = [];
         n_slips_removed = n_slips_removed + 1;
      end
   end
   
   range1_slip_periods{sat} = current_sat_slip_periods;
end

if ~success
  currentStats = NaN;
  return
end

% compute slips from LLI in rinex file
if ~isnan(current_GNSS_LLI)
   LLI_current_phase = squeeze(current_GNSS_LLI(:, ismember(current_obsCodes, phase1_Code), :))';
   LLI_slip_periods = getLLISlipPeriods(LLI_current_phase);
else
   LLI_slip_periods = NaN;
end
 % compute statistics of estimates
[mean_multipath_range1, overall_mean_multipath_range1,...
    rms_multipath_range1, average_rms_multipath_range1,...
    mean_ion_delay_phase1, overall_mean_ion_delay_phase1, mean_sat_elevation_angles, nEstimates, nEstimates_per_sat,...
    nRange1Obs_Per_Sat, nRange1Obs, range1_slip_distribution_per_sat, range1_slip_distribution, LLI_slip_distribution_per_sat, LLI_slip_distribution, ...
    combined_slip_distribution_per_sat, combined_slip_distribution, elevation_weighted_rms_multipath_range1, ...
    elevation_weighted_average_rms_multipath_range1] = ...
    computeDelayStats(ion_delay_phase1, multipath_range1, current_sat_elevation_angles, ...
    range1_slip_periods, LLI_slip_periods, range1_observations, tInterval);


%%
% Store statistics of analysis in a struct.
currentStatsCell = deal({mean_multipath_range1, overall_mean_multipath_range1,...
    rms_multipath_range1, average_rms_multipath_range1,...
    mean_ion_delay_phase1, overall_mean_ion_delay_phase1, mean_sat_elevation_angles, nEstimates, nEstimates_per_sat,...
    nRange1Obs_Per_Sat, nRange1Obs, range1_slip_distribution_per_sat, range1_slip_distribution, LLI_slip_distribution_per_sat, LLI_slip_distribution,...
    combined_slip_distribution_per_sat, combined_slip_distribution, elevation_weighted_rms_multipath_range1, ...
    elevation_weighted_average_rms_multipath_range1, range1_observations, phase1_observations});

fieldNames = {'mean_multipath_range1_satellitewise', ...
    'mean_multipath_range1_overall',...
    'rms_multipath_range1_satellitewise',... 
    'rms_multipath_range1_averaged',...
    'mean_ion_delay_phase1_satellitewise', 'mean_ion_delay_phase1_overall',... 
    'mean_sat_elevation_angles', 'nEstimates', 'nEstimates_per_sat',...
    'n_range1_obs_per_sat', 'nRange1Obs', 'range1_slip_distribution_per_sat', 'range1_slip_distribution', ...
    'LLI_slip_distribution_per_sat', 'LLI_slip_distribution', 'slip_distribution_per_sat_LLI_fusion', 'slip_distribution_LLI_fusion', ...
    'elevation_weighted_rms_multipath_range1_satellitewise', ...
    'elevation_weighted_average_rms_multipath_range1', 'range1_observations', 'phase1_observations'};

currentStats = cell2struct(currentStatsCell, fieldNames, 2);

% Store estimates needed for plotting
currentStats.ion_delay_phase1 = ion_delay_phase1;
currentStats.multipath_range1 = multipath_range1;
currentStats.sat_elevation_angles = current_sat_elevation_angles;

% Store codes
currentStats.range1_Code = range1_Code;
currentStats.range2_Code = range2_Code;
currentStats.phase1_Code = phase1_Code;
currentStats.phase2_Code = phase2_Code;

% Store slips
currentStats.range1_slip_periods = range1_slip_periods;


end