function writeOutputFile(outputFilename, outputDir, analysisResults, includeResultSummary, includeCompactSummary,...
    includeObservationOverview, includeLLIOverview)




if isempty(outputDir)
    outputDir = 'Outputs_Files';
end

if ~exist(outputDir, 'dir')
    mkdir(outputDir)
end

outputFilename = fullfile(outputDir, outputFilename);


rinex_obs_filename      = analysisResults.ExtraOutputInfo.rinex_obs_filename;
markerName              = analysisResults.ExtraOutputInfo.markerName;
rinexVersion            = analysisResults.ExtraOutputInfo.rinexVersion;
rinexProgr              = analysisResults.ExtraOutputInfo.rinexProgr;
recType                 = analysisResults.ExtraOutputInfo.recType;
tFirstObs               = analysisResults.ExtraOutputInfo.tFirstObs;
tLastObs                = analysisResults.ExtraOutputInfo.tLastObs;
tInterval               = analysisResults.ExtraOutputInfo.tInterval;
GLO_Slot2ChannelMap     = analysisResults.ExtraOutputInfo.GLO_Slot2ChannelMap;
nClockJumps             = analysisResults.ExtraOutputInfo.nClockJumps;
stdClockJumpInterval    = analysisResults.ExtraOutputInfo.stdClockJumpInterval;
meanClockJumpInterval   = analysisResults.ExtraOutputInfo.meanClockJumpInterval;
ionLimit                = analysisResults.ExtraOutputInfo.ionLimit;
phaseCodeLimit          = analysisResults.ExtraOutputInfo.phaseCodeLimit;

meanClockJumpInterval   = seconds(meanClockJumpInterval);
meanClockJumpInterval.Format = 'hh:mm:ss';

GNSSsystems = analysisResults.GNSSsystems;
nGNSSsystems = length(GNSSsystems);

YesNoMap = containers.Map({1, 0}, {'Yes', 'No'});

GNSS_Name2Code = containers.Map({'GPS', 'GLONASS', 'Galileo', 'BeiDou'}, {'G', 'R', 'E', 'C'});

GPSBandNameMap      = containers.Map({1, 2, 5}, {'L1', 'L2', 'L5'});
GLONASSBandNameMap  = containers.Map({1, 2, 3, 4, 6}, {'G1', 'G2', 'G3', 'G1a', 'G2a'});
GalileoBandNameMap  = containers.Map({1, 5, 6, 7, 8}, {'E1', 'E5a', 'E6', 'E5b', 'E5(a+b)'});
BeiDouBandNameMap   = containers.Map({1, 2, 5, 6, 7, 8}, {'B1', 'B1-2', 'B2a', 'B3', 'B2b', 'B2(a+b)'});
GNSSBandNameMap     = containers.Map({'GPS', 'GLONASS', 'Galileo', 'BeiDou'}, {GPSBandNameMap, GLONASSBandNameMap, GalileoBandNameMap, BeiDouBandNameMap});

GPSBandFreqMap      = containers.Map({1, 2, 5}, {'1575.42', '1227.60', '1176.45'});
GLONASSBandFreqMap  = containers.Map({1, 2, 3, 4, 6}, {'1602 + k*9/16', '1246 + k*7/16', '1202.025', '1600.995', '1248.06'});
GalileoBandFreqMap  = containers.Map({1, 5, 6, 7, 8}, {'1575.42', '1176.45', '1278.75', '1207.140', '1191.795'});
BeiDouBandFreqMap   = containers.Map({1, 2, 5, 6, 7, 8}, {'1575.42', '1561.098', '1176.45', '1268.52', '1207.140', '1191.795'});
GNSSBandFreqMap     = containers.Map({'GPS', 'GLONASS', 'Galileo', 'BeiDou'}, {GPSBandFreqMap, GLONASSBandFreqMap, GalileoBandFreqMap, BeiDouBandFreqMap});

% Check if any LLI indicators at all

if includeLLIOverview
   LLI_Active = 0;
   for i = 1:nGNSSsystems
      current_sys_struct = analysisResults.(GNSSsystems{i});
      nBands = current_sys_struct.nBands;
      for j = 1:nBands
         current_band_struct = current_sys_struct.(current_sys_struct.Bands{j});
         nCodes = current_band_struct.nCodes;
         for k = 1:nCodes
             current_code_struct = current_band_struct.(current_band_struct.Codes{k});
             if current_code_struct.LLI_slip_distribution.n_slips_Tot > 0
                 LLI_Active = 1;
                 break
             end
         end
         if LLI_Active
             break
         end
      end
      if LLI_Active
          break
      end
   end

   if ~LLI_Active
       includeLLIOverview = 0;
   end
end
%% HEADER
fid = fopen(outputFilename, 'wt');

fprintf(fid, 'GNSS Receiver Quality Check 2020\n');
fprintf(fid, 'Software version: 1.00\n');
fprintf(fid, 'Last software version release: 02/06/2020\n\n');
fprintf(fid, 'Software developed by Bjørn-Eirik Roald\nNorwegian University of Life Sciences(NMBU)\n\n');
fprintf(fid, 'RINEX observation filename:\t\t %s\n', rinex_obs_filename);
fprintf(fid, 'RINEX version:\t\t\t\t\t %s\n', rinexVersion);
fprintf(fid, 'RINEX converting program:\t\t %s\n', rinexProgr);
fprintf(fid, 'Marker name:\t\t\t\t\t %s\n', markerName);
fprintf(fid, 'Receiver type:\t\t\t\t\t %s\n', recType);
fprintf(fid, 'Date of observation start:\t\t %4d/%d/%d %d:%d:%.2f \n', ...
    tFirstObs(1),tFirstObs(2),tFirstObs(3),tFirstObs(4),tFirstObs(5),tFirstObs(6));
fprintf(fid, 'Date of observation end:\t\t %4d/%d/%d %d:%d:%.2f \n', ...
    tLastObs(1),tLastObs(2),tLastObs(3),tLastObs(4),tLastObs(5),tLastObs(6));
fprintf(fid, 'Observation interval [seconds]:\t %d\n', tInterval);
fprintf(fid, 'Number of receiver clock jumps:\t %d\n', nClockJumps);
fprintf(fid, 'Average clock jumps interval:\t %s (std: %.2f seconds)\n\n', string(meanClockJumpInterval), stdClockJumpInterval);

fprintf(fid, 'Critical cycle slip limits [m/s]:\n');
fprintf(fid, '- Ionospheric delay:\t\t\t%6.3f\n', ionLimit);
fprintf(fid, '- Phase-code combination:\t\t%6.3f\n\n', phaseCodeLimit);

fprintf(fid, 'GNSS systems presents in RINEX observation file:\n');
for i = 1:nGNSSsystems
    fprintf(fid, '- %s\n', analysisResults.GNSSsystems{i});
end

if includeLLIOverview
   if ~LLI_Active
       fprintf(fid, ['\n\nNOTE: As there were no "Loss-of-Lock" indicators in RINEX observation file,\n',...
           'no information concerining "Loss-of-Lock" indicators is included in output file']);
   end
end

fprintf(fid, '\n\nUser-specified contend included in output file\n');
fprintf(fid, '- Include overview of observations for each satellite:\t\t\t%s\n', YesNoMap(includeObservationOverview));
fprintf(fid, '- Include compact summary of analysis estimates:\t\t\t\t%s\n', YesNoMap(includeCompactSummary));
fprintf(fid, '- Include detailed summary of analysis\n   estimates, including for each individual satellite:\t\t\t%s\n', YesNoMap(includeResultSummary));
fprintf(fid, '- Include information about "Loss-of-Lock"\n   indicators in detailed summary:\t\t\t\t\t\t\t\t%s\n', YesNoMap(includeLLIOverview));

fprintf(fid, '\n\n======================================================================================================================================================================================================================================================================================================================================================\n');
fprintf(fid, '======================================================================================================================================================================================================================================================================================================================================================\n\n');
fprintf(fid, 'END OF HEADER\n\n\n');
%% COMPLETENESS OVERVIEW
if includeObservationOverview
    fprintf(fid, '\n\n\n\n======================================================================================================================================================================================================================================================================================================================================================');
    fprintf(fid, '\n======================================================================================================================================================================================================================================================================================================================================================\n\n');
    fprintf(fid, 'OBSERVATION COMPLETENESS OVERVIEW\n\n\n');
    for i = 1:nGNSSsystems
        if strcmp(GNSSsystems{i}, 'GPS')

            fprintf(fid, 'GPS Observation overview\n');
            nSat = length(fieldnames(analysisResults.GPS.observationOverview));
            fprintf(fid, ' ___________________________________________________________________________________________________\n');
            fprintf(fid, '|  PRN   |        L1 Observations          |             L2 Observations          | L5 Observations |\n');
            for PRN = 1:nSat
                if ~all([analysisResults.GPS.observationOverview.(strcat('Sat_', string(PRN))).Band_1,...
                        analysisResults.GPS.observationOverview.(strcat('Sat_', string(PRN))).Band_2,...
                        analysisResults.GPS.observationOverview.(strcat('Sat_', string(PRN))).Band_5] == "")

                    fprintf(fid, '|________|_________________________________|______________________________________|_________________|\n');
                    fprintf(fid, '|%8s|%33s|%38s|%17s|\n',...
                        strcat(GNSS_Name2Code(analysisResults.GNSSsystems{i}), num2str(PRN)), ...
                        analysisResults.GPS.observationOverview.(strcat('Sat_', string(PRN))).Band_1,...
                        analysisResults.GPS.observationOverview.(strcat('Sat_', string(PRN))).Band_2,...
                        analysisResults.GPS.observationOverview.(strcat('Sat_', string(PRN))).Band_5);
                end
            end
            fprintf(fid, '|________|_________________________________|______________________________________|_________________|\n\n\n');

        elseif strcmp(GNSSsystems{i}, 'GLONASS')

            fprintf(fid, 'GLONASS Observation overview\n');
            nSat = length(fieldnames(analysisResults.GLONASS.observationOverview));
            fprintf(fid, ' ________________________________________________________________________________________________________________________\n');
            fprintf(fid, '| Sat ID | Frequency Channel | G1 Observations | G2 Observations | G3 Observations | G1a Observations | G2a Observations |\n');
            for PRN = 1:nSat
                if ~all([analysisResults.GLONASS.observationOverview.(strcat('Sat_', string(PRN))).Band_1,...
                        analysisResults.GLONASS.observationOverview.(strcat('Sat_', string(PRN))).Band_2,...
                        analysisResults.GLONASS.observationOverview.(strcat('Sat_', string(PRN))).Band_3,...
                        analysisResults.GLONASS.observationOverview.(strcat('Sat_', string(PRN))).Band_4,...
                        analysisResults.GLONASS.observationOverview.(strcat('Sat_', string(PRN))).Band_6] == "")

                    fprintf(fid, '|________|___________________|_________________|_________________|_________________|__________________|__________________|\n');
                    fprintf(fid, '|%8s|%19d|%17s|%17s|%17s|%18s|%18s|\n',...
                        strcat(GNSS_Name2Code(analysisResults.GNSSsystems{i}), num2str(PRN)), ...
                        GLO_Slot2ChannelMap(PRN),...
                        analysisResults.GLONASS.observationOverview.(strcat('Sat_', string(PRN))).Band_1,...
                        analysisResults.GLONASS.observationOverview.(strcat('Sat_', string(PRN))).Band_2,...
                        analysisResults.GLONASS.observationOverview.(strcat('Sat_', string(PRN))).Band_3,...
                        analysisResults.GLONASS.observationOverview.(strcat('Sat_', string(PRN))).Band_4,...
                        analysisResults.GLONASS.observationOverview.(strcat('Sat_', string(PRN))).Band_6);
                end
            end
            fprintf(fid, '|________|___________________|_________________|_________________|_________________|__________________|__________________|\n\n\n');

        elseif strcmp(GNSSsystems{i}, 'Galileo')

            fprintf(fid, 'Galileo Observation overview\n');
            nSat = length(fieldnames(analysisResults.Galileo.observationOverview));
            fprintf(fid, ' _________________________________________________________________________________________________________\n');
            fprintf(fid, '|  PRN   | E1 Observations | E5a Observations | E6 Observations | E5b Observations | G5(a+b) Observations |\n');
            for PRN = 1:nSat
                if ~all([analysisResults.Galileo.observationOverview.(strcat('Sat_', string(PRN))).Band_1,...
                        analysisResults.Galileo.observationOverview.(strcat('Sat_', string(PRN))).Band_5,...
                        analysisResults.Galileo.observationOverview.(strcat('Sat_', string(PRN))).Band_6,...
                        analysisResults.Galileo.observationOverview.(strcat('Sat_', string(PRN))).Band_7,...
                        analysisResults.Galileo.observationOverview.(strcat('Sat_', string(PRN))).Band_8] == "")

                    fprintf(fid, '|________|_________________|__________________|_________________|__________________|______________________|\n');
                    fprintf(fid, '|%8s|%17s|%18s|%17s|%18s|%22s|\n',...
                        strcat(GNSS_Name2Code(analysisResults.GNSSsystems{i}), num2str(PRN)), ...
                        analysisResults.Galileo.observationOverview.(strcat('Sat_', string(PRN))).Band_1,...
                        analysisResults.Galileo.observationOverview.(strcat('Sat_', string(PRN))).Band_5,...
                        analysisResults.Galileo.observationOverview.(strcat('Sat_', string(PRN))).Band_6,...
                        analysisResults.Galileo.observationOverview.(strcat('Sat_', string(PRN))).Band_7,...
                        analysisResults.Galileo.observationOverview.(strcat('Sat_', string(PRN))).Band_8);
                end
            end
            fprintf(fid, '|________|_________________|__________________|_________________|__________________|______________________|\n\n\n');

         elseif strcmp(GNSSsystems{i}, 'BeiDou')

            fprintf(fid, 'BeiDou Observation overview\n');
            nSat = length(fieldnames(analysisResults.BeiDou.observationOverview));
            fprintf(fid, ' ______________________________________________________________________________________________________________________________\n');
            fprintf(fid, '|  PRN   | B1 Observations | E1-2 Observations | B2a Observations | B3 Observations  | B2b Observations | B2(a+b) Observations |\n');
            for PRN = 1:nSat
                if ~all([analysisResults.BeiDou.observationOverview.(strcat('Sat_', string(PRN))).Band_1,...
                        analysisResults.BeiDou.observationOverview.(strcat('Sat_', string(PRN))).Band_2,...
                        analysisResults.BeiDou.observationOverview.(strcat('Sat_', string(PRN))).Band_5,...
                        analysisResults.BeiDou.observationOverview.(strcat('Sat_', string(PRN))).Band_6,...
                        analysisResults.BeiDou.observationOverview.(strcat('Sat_', string(PRN))).Band_7,...
                        analysisResults.BeiDou.observationOverview.(strcat('Sat_', string(PRN))).Band_8] == "")

                    fprintf(fid, '|________|_________________|___________________|__________________|__________________|__________________|______________________|\n');
                    fprintf(fid, '|%8s|%17s|%19s|%18s|%18s|%18s|%22s|\n',...
                        strcat(GNSS_Name2Code(analysisResults.GNSSsystems{i}), num2str(PRN)), ...
                        analysisResults.BeiDou.observationOverview.(strcat('Sat_', string(PRN))).Band_1,...
                        analysisResults.BeiDou.observationOverview.(strcat('Sat_', string(PRN))).Band_2,...
                        analysisResults.BeiDou.observationOverview.(strcat('Sat_', string(PRN))).Band_5,...
                        analysisResults.BeiDou.observationOverview.(strcat('Sat_', string(PRN))).Band_6,...
                        analysisResults.BeiDou.observationOverview.(strcat('Sat_', string(PRN))).Band_7,...
                        analysisResults.BeiDou.observationOverview.(strcat('Sat_', string(PRN))).Band_8);
                end
            end
            fprintf(fid, '|________|_________________|___________________|__________________|__________________|__________________|______________________|\n\n\n');

        end
    end
    fprintf(fid, '======================================================================================================================================================================================================================================================================================================================================================\n');
    fprintf(fid, '======================================================================================================================================================================================================================================================================================================================================================\n');
    fprintf(fid, 'END OF OBSERVATION COMPLETENESS OVERVIEW\n\n\n\n\n');
end
%% Compact Code analysis summary

if includeCompactSummary
    fprintf(fid, '\n\n\n\n======================================================================================================================================================================================================================================================================================================================================================');
    fprintf(fid, '\n======================================================================================================================================================================================================================================================================================================================================================\n\n');
    fprintf(fid, 'ANALYSIS RESULTS SUMMARY (COMPACT)\n\n\n');


    for i = 1:nGNSSsystems
        current_sys_struct = analysisResults.(GNSSsystems{i});
        nBands_current_sys = current_sys_struct.nBands;
        current_BandFreqMap = GNSSBandFreqMap(GNSSsystems{i});
        current_BandNameMap = GNSSBandNameMap(GNSSsystems{i});

        headermsg                       = '|                                             |';
        rmsMultiPathmsg                 = '|RMS multipath[meters]                        |';
        rmsMultiPathmsg_weighted        = '|Weighted RMS multipath[meters]               |';
        nSlipsmsg                       = '|N ambiguity slips periods                    |';
        slipRatiomsg                    = '|Ratio of N slip periods/N obs epochs [%%]     |';
        nSlipsOver10msg                 = '|N slip periods, elevation angle > 10 degrees |';
        nSlipsUnder10msg                = '|N slip periods, elevation angle < 10 degrees |';
        nSlipsNaNmsg                    = '|N slip periods, elevation angle not computed |';
        topline                         = ' _____________________________________________ ';
        bottomline                      = '|_____________________________________________|';

        fprintf(fid, '\n\n\n\n');
        fprintf(fid, '%s ANALYSIS SUMMARY\n\n', upper(analysisResults.GNSSsystems{i}));
        for j = 1:nBands_current_sys
            bandName = current_sys_struct.Bands{j};
            current_band_struct = current_sys_struct.(bandName);

            nCodes_current_band = current_band_struct.nCodes;
            for k = 1:nCodes_current_band
                codeName = current_band_struct.Codes{k};
                current_code_struct = current_band_struct.(codeName);

                topline                     = strcat(topline, '_________');
                bottomline                  = strcat(bottomline, '________|');

                headermsg                   = strcat(headermsg, sprintf('%8s|', codeName));
                rmsMultiPathmsg             = strcat(rmsMultiPathmsg, sprintf('%8.3f|', current_code_struct.rms_multipath_range1_averaged));
                rmsMultiPathmsg_weighted    = strcat(rmsMultiPathmsg_weighted, sprintf('%8.3f|', current_code_struct.elevation_weighted_average_rms_multipath_range1));
                slipRatiomsg                = strcat(slipRatiomsg, sprintf('%8.3f|', 100*current_code_struct.range1_slip_distribution.n_slips_Tot/current_code_struct.nRange1Obs)); 
                nSlipsmsg                   = strcat(nSlipsmsg, sprintf('%8d|', current_code_struct.range1_slip_distribution.n_slips_Tot));
                nSlipsOver10msg             = strcat(nSlipsOver10msg, sprintf('%8d|',  ...
                    sum([current_code_struct.range1_slip_distribution.n_slips_10_20, current_code_struct.range1_slip_distribution.n_slips_20_30, ...
                         current_code_struct.range1_slip_distribution.n_slips_30_40, current_code_struct.range1_slip_distribution.n_slips_40_50, ...
                         current_code_struct.range1_slip_distribution.n_slips_over50])));
                nSlipsUnder10msg            = strcat(nSlipsUnder10msg, sprintf('%8d|', current_code_struct.range1_slip_distribution.n_slips_0_10));
                nSlipsNaNmsg                = strcat(nSlipsNaNmsg, sprintf('%8d|', current_code_struct.range1_slip_distribution.n_slips_NaN));
            end
        end
        fprintf(fid, strcat(topline, '\n'));
        fprintf(fid, strcat(headermsg, '\n'));
        fprintf(fid, strcat(bottomline, '\n'));
        fprintf(fid, strcat(rmsMultiPathmsg, '\n'));
        fprintf(fid, strcat(bottomline, '\n'));
        fprintf(fid, strcat(rmsMultiPathmsg_weighted, '\n'));
        fprintf(fid, strcat(bottomline, '\n'));
        fprintf(fid, strcat(nSlipsmsg, '\n'));
        fprintf(fid, strcat(bottomline, '\n'));
        fprintf(fid, strcat(nSlipsOver10msg, '\n'));
        fprintf(fid, strcat(bottomline, '\n'));
        fprintf(fid, strcat(nSlipsUnder10msg, '\n'));    
        fprintf(fid, strcat(bottomline, '\n'));
        fprintf(fid, strcat(nSlipsNaNmsg, '\n'));    
        fprintf(fid, strcat(bottomline, '\n'));
        fprintf(fid, strcat(slipRatiomsg, '\n'));
        fprintf(fid, strcat(bottomline, '\n'));
    end
    fprintf(fid, '\n======================================================================================================================================================================================================================================================================================================================================================\n');
    fprintf(fid, 'END OF ANALYSIS RESULTS SUMMARY (COMPACT)\n\n\n\n\n');

end

%% Code analysis

if includeResultSummary
    
    for i = 1:nGNSSsystems
        current_sys = GNSSsystems{i};
        current_sys_struct = analysisResults.(GNSSsystems{i});
        nBands_current_sys = current_sys_struct.nBands;
        current_BandFreqMap = GNSSBandFreqMap(GNSSsystems{i});
        current_BandNameMap = GNSSBandNameMap(GNSSsystems{i});

        fprintf(fid, '\n\n\n\n======================================================================================================================================================================================================================================================================================================================================================');
        fprintf(fid, '\n======================================================================================================================================================================================================================================================================================================================================================\n\n');
        fprintf(fid, 'BEGINNING OF %s ANALYSIS\n\n', upper(analysisResults.GNSSsystems{i}));
        fprintf(fid, 'Amount of carrier bands analysed: %d \n', nBands_current_sys);
        for j = 1:nBands_current_sys
            bandName = current_sys_struct.Bands{j};
            current_band_struct = current_sys_struct.(bandName);

            nCodes_current_band = current_band_struct.nCodes;
            fprintf(fid, '\n\n======================================================================================================================================================================================================================================================================================================================================================\n\n');
            fprintf(fid, '%s (%s)\n\n', upper(analysisResults.(GNSSsystems{i}).Bands{j}), ...
                current_BandNameMap(str2double(bandName(end))));
            fprintf(fid, 'Frequency of carrier band [MHz]:\t\t\t\t\t %s\n', current_BandFreqMap(str2double(bandName(end))));
            fprintf(fid, 'Amount of code signals analysed in current band:\t %d \n', nCodes_current_band);
            for k = 1:nCodes_current_band

                current_code_struct = current_band_struct.(current_band_struct.Codes{k});
                fprintf(fid, '\n------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------\n\n');
                fprintf(fid, 'Code signal:\t\t\t\t\t\t\t\t\t %s\n\n', current_code_struct.range1_Code);
                fprintf(fid, 'Second code signal\n(Utilized for linear combinations):\t\t\t\t %s\n', current_code_struct.range2_Code);
                fprintf(fid, 'RMS multipath (All SVs) [meters]:\t\t\t\t%6.3f \n', current_code_struct.rms_multipath_range1_averaged);
                fprintf(fid, 'Weighted RMS multipath (All SVs) [meters]:\t\t%6.3f \n', current_code_struct.elevation_weighted_average_rms_multipath_range1);
                fprintf(fid, 'Number of %s observation epochs:\t\t\t\t %d \n', current_code_struct.range1_Code, current_code_struct.nRange1Obs);
                fprintf(fid, 'N epochs with multipath estimates:\t\t\t\t %d \n', current_code_struct.nEstimates);
                fprintf(fid, 'N ambiguity slips on %s signal:\t\t\t\t %d \n', ...
                    current_code_struct.range1_Code, current_code_struct.range1_slip_distribution.n_slips_Tot);
                fprintf(fid, 'Ratio of N slip periods/N %s obs epochs [%%]:\t %.3f\n',...
                    current_code_struct.range1_Code, 100*current_code_struct.range1_slip_distribution.n_slips_Tot/current_code_struct.nRange1Obs);


                nSat = length(current_code_struct.range1_slip_distribution_per_sat);
                
                if includeLLIOverview
                
                    if ~strcmp(current_sys, 'GLONASS')
                        fprintf(fid, '\nSatellite Overview\n');
                        fprintf(fid, ' _____________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________\n');
                        fprintf(fid, '|   |    n %s   | n Epochs with |   RMS   | Weighted RMS |  Average Sat. |                           |     Slip Periods/Obs      |       n Slip Periods      |       n Slip Periods      |       n Slip Periods      |       n Slip Periods      |       n Slip Periods      |       n Slip Periods      |       n Slip Periods      |\n', current_code_struct.range1_Code);
                        fprintf(fid, '|PRN|Observations|   Multipath   |Multipath|  Multipath   |Elevation Angle|       n Slip Periods      |         Ratio             |      Elevation Angle      |      Elevation Angle      |      Elevation Angle      |      Elevation Angle      |      Elevation Angle      |      Elevation Angle      |      Elevation Angle      |\n');
                        fprintf(fid, '|   |            |   Estimates   |[meters] |   [meters]   |   [degrees]   |                           |          [%%]              |       0-10 degrees        |        10-20 degrees      |        20-30 degrees      |        30-40 degrees      |        40-50 degrees      |        >50 degrees        |        NaN degrees        |\n');
                        fprintf(fid, '|   |            |               |         |              |               |___________________________|___________________________|___________________________|___________________________|___________________________|___________________________|___________________________|___________________________|___________________________|\n');
                        fprintf(fid, '|   |            |               |         |              |               | Analysed |  LLI  |  Both  | Analysed |  LLI  |  Both  | Analysed |  LLI  |  Both  | Analysed |  LLI  |  Both  | Analysed |  LLI  |  Both  | Analysed |  LLI  |  Both  | Analysed |  LLI  |  Both  | Analysed |  LLI  |  Both  | Analysed |  LLI  |  Both  |\n');
                        for PRN = 1:nSat
                           if current_code_struct.n_range1_obs_per_sat(PRN)>0
                               fprintf(fid, '|___|____________|_______________|_________|______________|_______________|__________|_______|________|__________|_______|________|__________|_______|________|__________|_______|________|__________|_______|________|__________|_______|________|__________|_______|________|__________|_______|________|__________|_______|________|\n');
                               fprintf(fid, '|%3s|%12d|%15d|%9.3f|%14.3f|%15.3f|%10d|%7d|%8d|%10.3f|%7.3f|%8.3f|%10d|%7d|%8d|%10d|%7d|%8d|%10d|%7d|%8d|%10d|%7d|%8d|%10d|%7d|%8d|%10d|%7d|%8d|%10d|%7d|%8d|\n', ...
                                  strcat(GNSS_Name2Code(analysisResults.GNSSsystems{i}), num2str(PRN)), ...
                                  current_code_struct.n_range1_obs_per_sat(PRN),...
                                  current_code_struct.nEstimates_per_sat(PRN),...
                                  current_code_struct.rms_multipath_range1_satellitewise(PRN),...
                                  current_code_struct.elevation_weighted_rms_multipath_range1_satellitewise(PRN),...
                                  current_code_struct.mean_sat_elevation_angles(PRN),...
                                  current_code_struct.range1_slip_distribution_per_sat{PRN}.n_slips_Tot,...
                                  current_code_struct.LLI_slip_distribution_per_sat{PRN}.n_slips_Tot,...
                                  current_code_struct.slip_distribution_per_sat_LLI_fusion{PRN}.n_slips_Tot,...
                                  100*current_code_struct.range1_slip_distribution_per_sat{PRN}.n_slips_Tot/current_code_struct.n_range1_obs_per_sat(PRN),...
                                  100*current_code_struct.LLI_slip_distribution_per_sat{PRN}.n_slips_Tot/current_code_struct.n_range1_obs_per_sat(PRN),...
                                  100*current_code_struct.slip_distribution_per_sat_LLI_fusion{PRN}.n_slips_Tot/current_code_struct.n_range1_obs_per_sat(PRN),...
                                  current_code_struct.range1_slip_distribution_per_sat{PRN}.n_slips_0_10,...
                                  current_code_struct.LLI_slip_distribution_per_sat{PRN}.n_slips_0_10,...
                                  current_code_struct.slip_distribution_per_sat_LLI_fusion{PRN}.n_slips_0_10,...
                                  current_code_struct.range1_slip_distribution_per_sat{PRN}.n_slips_10_20,...
                                  current_code_struct.LLI_slip_distribution_per_sat{PRN}.n_slips_10_20,...
                                  current_code_struct.slip_distribution_per_sat_LLI_fusion{PRN}.n_slips_10_20,...
                                  current_code_struct.range1_slip_distribution_per_sat{PRN}.n_slips_20_30,...
                                  current_code_struct.LLI_slip_distribution_per_sat{PRN}.n_slips_20_30,...
                                  current_code_struct.slip_distribution_per_sat_LLI_fusion{PRN}.n_slips_20_30,...
                                  current_code_struct.range1_slip_distribution_per_sat{PRN}.n_slips_30_40,...
                                  current_code_struct.LLI_slip_distribution_per_sat{PRN}.n_slips_30_40,...
                                  current_code_struct.slip_distribution_per_sat_LLI_fusion{PRN}.n_slips_30_40,...
                                  current_code_struct.range1_slip_distribution_per_sat{PRN}.n_slips_40_50,...
                                  current_code_struct.LLI_slip_distribution_per_sat{PRN}.n_slips_40_50,...
                                  current_code_struct.slip_distribution_per_sat_LLI_fusion{PRN}.n_slips_40_50,...
                                  current_code_struct.range1_slip_distribution_per_sat{PRN}.n_slips_over50,...
                                  current_code_struct.LLI_slip_distribution_per_sat{PRN}.n_slips_over50,...
                                  current_code_struct.slip_distribution_per_sat_LLI_fusion{PRN}.n_slips_over50,...
                                  current_code_struct.range1_slip_distribution_per_sat{PRN}.n_slips_NaN,...
                                  current_code_struct.LLI_slip_distribution_per_sat{PRN}.n_slips_NaN,...
                                  current_code_struct.slip_distribution_per_sat_LLI_fusion{PRN}.n_slips_NaN);
                           end
                        end
                            fprintf(fid, '|___|____________|_______________|_________|______________|_______________|__________|_______|________|__________|_______|________|__________|_______|________|__________|_______|________|__________|_______|________|__________|_______|________|__________|_______|________|__________|_______|________|__________|_______|________|\n');
                    else
                        fprintf(fid, '\nSatellite Overview\n');
                        fprintf(fid, ' ____________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________\n');
                        fprintf(fid, '|      | Frequency |    n %s   | n Epochs with |   RMS   | Weighted RMS |  Average Sat. |                           |        Slip/Obs           |       n Slip Periods      |       n Slip Periods      |       n Slip Periods      |       n Slip Periods      |       n Slip Periods      |       n Slip Periods      |       n Slip Periods      |\n', current_code_struct.range1_Code);
                        fprintf(fid, '|Sat ID|  Channel  |Observations|   Multipath   |Multipath|  Multipath   |Elevation Angle|       n Slip Periods      |         Ratio             |      Elevation Angle      |      Elevation Angle      |      Elevation Angle      |      Elevation Angle      |      Elevation Angle      |      Elevation Angle      |      Elevation Angle      |\n');
                        fprintf(fid, '|      |           |            |   Estimates   |[meters] |   [meters]   |   [degrees]   |                           |          [%%]              |       0-10 degrees        |        10-20 degrees      |        20-30 degrees      |        30-40 degrees      |        40-50 degrees      |        >50 degrees        |        NaN degrees        |\n');
                        fprintf(fid, '|      |           |            |               |         |              |               |___________________________|___________________________|___________________________|___________________________|___________________________|___________________________|___________________________|___________________________|___________________________|\n');
                        fprintf(fid, '|      |           |            |               |         |              |               | Analysed |  LLI  |  Both  | Analysed |  LLI  |  Both  | Analysed |  LLI  |  Both  | Analysed |  LLI  |  Both  | Analysed |  LLI  |  Both  | Analysed |  LLI  |  Both  | Analysed |  LLI  |  Both  | Analysed |  LLI  |  Both  | Analysed |  LLI  |  Both  |\n');
                        for PRN = 1:nSat
                           if current_code_struct.n_range1_obs_per_sat(PRN)>0
                               fprintf(fid, '|______|___________|____________|_______________|_________|______________|_______________|__________|_______|________|__________|_______|________|__________|_______|________|__________|_______|________|__________|_______|________|__________|_______|________|__________|_______|________|__________|_______|________|__________|_______|________|\n');
                               fprintf(fid, '|%6s|%11d|%12d|%15d|%9.3f|%14.3f|%15.3f|%10d|%7d|%8d|%10.3f|%7.3f|%8.3f|%10d|%7d|%8d|%10d|%7d|%8d|%10d|%7d|%8d|%10d|%7d|%8d|%10d|%7d|%8d|%10d|%7d|%8d|%10d|%7d|%8d|\n', ...
                                  strcat(GNSS_Name2Code(analysisResults.GNSSsystems{i}), num2str(PRN)), ...
                                  GLO_Slot2ChannelMap(PRN),...
                                  current_code_struct.n_range1_obs_per_sat(PRN),...
                                  current_code_struct.nEstimates_per_sat(PRN),...
                                  current_code_struct.rms_multipath_range1_satellitewise(PRN),...
                                  current_code_struct.elevation_weighted_rms_multipath_range1_satellitewise(PRN),...
                                  current_code_struct.mean_sat_elevation_angles(PRN),...
                                  current_code_struct.range1_slip_distribution_per_sat{PRN}.n_slips_Tot,...
                                  current_code_struct.LLI_slip_distribution_per_sat{PRN}.n_slips_Tot,...
                                  current_code_struct.slip_distribution_per_sat_LLI_fusion{PRN}.n_slips_Tot,...
                                  100*current_code_struct.range1_slip_distribution_per_sat{PRN}.n_slips_Tot/current_code_struct.n_range1_obs_per_sat(PRN),...
                                  100*current_code_struct.LLI_slip_distribution_per_sat{PRN}.n_slips_Tot/current_code_struct.n_range1_obs_per_sat(PRN),...
                                  100*current_code_struct.slip_distribution_per_sat_LLI_fusion{PRN}.n_slips_Tot/current_code_struct.n_range1_obs_per_sat(PRN),...
                                  current_code_struct.range1_slip_distribution_per_sat{PRN}.n_slips_0_10,...
                                  current_code_struct.LLI_slip_distribution_per_sat{PRN}.n_slips_0_10,...
                                  current_code_struct.slip_distribution_per_sat_LLI_fusion{PRN}.n_slips_0_10,...
                                  current_code_struct.range1_slip_distribution_per_sat{PRN}.n_slips_10_20,...
                                  current_code_struct.LLI_slip_distribution_per_sat{PRN}.n_slips_10_20,...
                                  current_code_struct.slip_distribution_per_sat_LLI_fusion{PRN}.n_slips_10_20,...
                                  current_code_struct.range1_slip_distribution_per_sat{PRN}.n_slips_20_30,...
                                  current_code_struct.LLI_slip_distribution_per_sat{PRN}.n_slips_20_30,...
                                  current_code_struct.slip_distribution_per_sat_LLI_fusion{PRN}.n_slips_20_30,...
                                  current_code_struct.range1_slip_distribution_per_sat{PRN}.n_slips_30_40,...
                                  current_code_struct.LLI_slip_distribution_per_sat{PRN}.n_slips_30_40,...
                                  current_code_struct.slip_distribution_per_sat_LLI_fusion{PRN}.n_slips_30_40,...
                                  current_code_struct.range1_slip_distribution_per_sat{PRN}.n_slips_40_50,...
                                  current_code_struct.LLI_slip_distribution_per_sat{PRN}.n_slips_40_50,...
                                  current_code_struct.slip_distribution_per_sat_LLI_fusion{PRN}.n_slips_40_50,...
                                  current_code_struct.range1_slip_distribution_per_sat{PRN}.n_slips_over50,...
                                  current_code_struct.LLI_slip_distribution_per_sat{PRN}.n_slips_over50,...
                                  current_code_struct.slip_distribution_per_sat_LLI_fusion{PRN}.n_slips_over50,...
                                  current_code_struct.range1_slip_distribution_per_sat{PRN}.n_slips_NaN,...
                                  current_code_struct.LLI_slip_distribution_per_sat{PRN}.n_slips_NaN,...
                                  current_code_struct.slip_distribution_per_sat_LLI_fusion{PRN}.n_slips_NaN);
                           end
                        end
                        fprintf(fid, '|______|___________|____________|_______________|_________|______________|_______________|__________|_______|________|__________|_______|________|__________|_______|________|__________|_______|________|__________|_______|________|__________|_______|________|__________|_______|________|__________|_______|________|__________|_______|________|\n');
                    end
                else
                    if ~strcmp(current_sys, 'GLONASS')
                        fprintf(fid, '\nSatellite Overview\n');
                        fprintf(fid, ' __________________________________________________________________________________________________________________________________________________________________________________________________________________________________ \n');
                        fprintf(fid, '|   |    n %s   | n Epochs with |   RMS   | Weighted RMS |  Average Sat. |               | Slip/Obs | n Slip Periods  | n Slip Periods  | n Slip Periods  | n Slip Periods  | n Slip Periods  | n Slip Periods  | n Slip Periods  |\n', current_code_struct.range1_Code);
                        fprintf(fid, '|PRN|Observations|   Multipath   |Multipath|  Multipath   |Elevation Angle|    n Slip     |  Ratio   | Elevation Angle | Elevation Angle | Elevation Angle | Elevation Angle | Elevation Angle | Elevation Angle | Elevation Angle |\n');
                        fprintf(fid, '|   |            |   Estimates   |[meters] |   [meters]   |   [degrees]   |    Periods    |   [%%]    |  0-10 degrees   |  10-20 degrees  |  20-30 degrees  |  30-40 degrees  |  40-50 degrees  |   >50 degrees   |   NaN degrees   |\n');
                        
                        for PRN = 1:nSat
                           if current_code_struct.n_range1_obs_per_sat(PRN)>0
                               fprintf(fid, '|___|____________|_______________|_________|______________|_______________|_______________|__________|_________________|_________________|_________________|_________________|_________________|_________________|_________________|\n');
                               fprintf(fid, '|%3s|%12d|%15d|%9.3f|%14.3f|%15.3f|%15d|%10.3f|%17d|%17d|%17d|%17d|%17d|%17d|%17d|\n', ...
                                  strcat(GNSS_Name2Code(analysisResults.GNSSsystems{i}), num2str(PRN)), ...
                                  current_code_struct.n_range1_obs_per_sat(PRN),...
                                  current_code_struct.nEstimates_per_sat(PRN),...
                                  current_code_struct.rms_multipath_range1_satellitewise(PRN),...
                                  current_code_struct.elevation_weighted_rms_multipath_range1_satellitewise(PRN),...
                                  current_code_struct.mean_sat_elevation_angles(PRN),...
                                  current_code_struct.range1_slip_distribution_per_sat{PRN}.n_slips_Tot,...
                                  100*current_code_struct.range1_slip_distribution_per_sat{PRN}.n_slips_Tot/current_code_struct.n_range1_obs_per_sat(PRN),...
                                  current_code_struct.range1_slip_distribution_per_sat{PRN}.n_slips_0_10,...
                                  current_code_struct.range1_slip_distribution_per_sat{PRN}.n_slips_10_20,...
                                  current_code_struct.range1_slip_distribution_per_sat{PRN}.n_slips_20_30,...
                                  current_code_struct.range1_slip_distribution_per_sat{PRN}.n_slips_30_40,...
                                  current_code_struct.range1_slip_distribution_per_sat{PRN}.n_slips_40_50,...
                                  current_code_struct.range1_slip_distribution_per_sat{PRN}.n_slips_over50,...
                                  current_code_struct.range1_slip_distribution_per_sat{PRN}.n_slips_NaN);
                           end
                        end
                            fprintf(fid, '|___|____________|_______________|_________|______________|_______________|_______________|__________|_________________|_________________|_________________|_________________|_________________|_________________|_________________|\n');
                    else
                        fprintf(fid, '\nSatellite Overview\n');
                        fprintf(fid, ' _________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________ \n');
                        fprintf(fid, '|      | Frequency |    n %s   | n Epochs with |   RMS   | Weighted RMS |  Average Sat. |               | Slip/Obs | n Slip Periods  | n Slip Periods  | n Slip Periods  | n Slip Periods  | n Slip Periods  | n Slip Periods  | n Slip Periods  |\n', current_code_struct.range1_Code);
                        fprintf(fid, '|Sat ID|  Channel  |Observations|   Multipath   |Multipath|  Multipath   |Elevation Angle|    n Slip     |  Ratio   | Elevation Angle | Elevation Angle | Elevation Angle | Elevation Angle | Elevation Angle | Elevation Angle | Elevation Angle |\n');
                        fprintf(fid, '|      |           |            |   Estimates   |[meters] |   [meters]   |   [degrees]   |    Periods    |   [%%]    |  0-10 degrees   |  10-20 degrees  |  20-30 degrees  |  30-40 degrees  |  40-50 degrees  |   >50 degrees   |   NaN degrees   |\n');
                        
                        for PRN = 1:nSat
                           if current_code_struct.n_range1_obs_per_sat(PRN)>0
                               fprintf(fid, '|______|___________|____________|_______________|_________|______________|_______________|_______________|__________|_________________|_________________|_________________|_________________|_________________|_________________|_________________|\n');
                               fprintf(fid, '|%6s|%11d|%12d|%15d|%9.3f|%14.3f|%15.3f|%15d|%10.3f|%17d|%17d|%17d|%17d|%17d|%17d|%17d|\n', ...
                                  strcat(GNSS_Name2Code(analysisResults.GNSSsystems{i}), num2str(PRN)), ...
                                  GLO_Slot2ChannelMap(PRN),...
                                  current_code_struct.n_range1_obs_per_sat(PRN),...
                                  current_code_struct.nEstimates_per_sat(PRN),...
                                  current_code_struct.rms_multipath_range1_satellitewise(PRN),...
                                  current_code_struct.elevation_weighted_rms_multipath_range1_satellitewise(PRN),...
                                  current_code_struct.mean_sat_elevation_angles(PRN),...
                                  current_code_struct.range1_slip_distribution_per_sat{PRN}.n_slips_Tot,...
                                  100*current_code_struct.range1_slip_distribution_per_sat{PRN}.n_slips_Tot/current_code_struct.n_range1_obs_per_sat(PRN),...
                                  current_code_struct.range1_slip_distribution_per_sat{PRN}.n_slips_0_10,...
                                  current_code_struct.range1_slip_distribution_per_sat{PRN}.n_slips_10_20,...
                                  current_code_struct.range1_slip_distribution_per_sat{PRN}.n_slips_20_30,...
                                  current_code_struct.range1_slip_distribution_per_sat{PRN}.n_slips_30_40,...
                                  current_code_struct.range1_slip_distribution_per_sat{PRN}.n_slips_40_50,...
                                  current_code_struct.range1_slip_distribution_per_sat{PRN}.n_slips_over50,...
                                  current_code_struct.range1_slip_distribution_per_sat{PRN}.n_slips_NaN);
                           end
                        end
                        fprintf(fid, '|______|___________|____________|_______________|_________|______________|_______________|_______________|__________|_________________|_________________|_________________|_________________|_________________|_________________|_________________|\n');
                    end
                    
                end
                

            end
        end
        fprintf(fid, '\n======================================================================================================================================================================================================================================================================================================================================================\n');
        fprintf(fid, '======================================================================================================================================================================================================================================================================================================================================================\n');
        fprintf(fid, 'END OF %s ANALYSIS\n\n\n\n', upper(analysisResults.GNSSsystems{i}));
    end

end

fprintf(fid, '\n\n\n\nEND OF OUTPUT FILE');
fclose(fid);
end