function createSignalStatsDetailsTable(app, tab, sys, band, code)
         
         sysNameMap = containers.Map({'GPS', 'GLONASS', 'Galileo', 'BeiDou'}, {'G', 'R', 'E', 'C'});
         sysID = sysNameMap(sys);
         
         columnNames = {'PRN', ...
                        'n C1C Observations', ...
                        'n Epochs with Multipath Estimates', ...
                        'RMS Multipath [meters]', ...
                        'Weighted RMS Multipath [meters]', ...
                        'Average Sat. Elevation Angle [degrees]', ...
                        'n Slip Periods', ...
                        'Slip/Obs Ratio [%]', ...
                        'n Slip Periods Elevation Angle 0-10 degrees', ...
                        'n Slip Periods Elevation Angle 10-20 degrees', ...
                        'n Slip Periods Elevation Angle 20-30 degrees', ...
                        'n Slip Periods Elevation Angle 30-40 degrees', ...
                        'n Slip Periods Elevation Angle 40-50 degrees', ...
                        'n Slip Periods Elevation Angle >50 degrees', ....
                        'n Slip Periods Elevation Angle NaN degrees'};
         
         rowLabels = {};
         
         [~, n_sat] = size(app.results.(sys).(band).(code).mean_multipath_range1_satellitewise);
         n_cols = 15;
         n_rows = n_sat;
         
         for sat = 1:n_sat
            if app.results.(sys).(band).(code).n_range1_obs_per_sat(sat) == 0
               n_rows = n_rows - 1;
            else
               rowLabels{end+1} = '';
            end
         end
         
         data = cell(n_rows, n_cols);
         
         row = 0;
         for sat = 1:n_sat
            if app.results.(sys).(band).(code).n_range1_obs_per_sat(sat) > 0
               row = row + 1;
               
               PRN = sat;
               n_obs = app.results.(sys).(band).(code).n_range1_obs_per_sat(sat);
               nEstimates = app.results.(sys).(band).(code).nEstimates_per_sat(sat);
               multipath_RMS = app.results.(sys).(band).(code).rms_multipath_range1_satellitewise(sat);
               weighted_multipath_RMS = app.results.(sys).(band).(code).elevation_weighted_rms_multipath_range1_satellitewise(sat);
               average_sat_angle = app.results.(sys).(band).(code).mean_sat_elevation_angles(sat);
               n_slips = app.results.(sys).(band).(code).range1_slip_distribution_per_sat{sat}.n_slips_Tot;
               slip_obs_ratio = 100*app.results.(sys).(band).(code).range1_slip_distribution_per_sat{sat}.n_slips_Tot/app.results.(sys).(band).(code).n_range1_obs_per_sat(sat);
               n_slips_0_10 = app.results.(sys).(band).(code).range1_slip_distribution_per_sat{sat}.n_slips_0_10;
               n_slips_10_20 = app.results.(sys).(band).(code).range1_slip_distribution_per_sat{sat}.n_slips_10_20;
               n_slips_20_30 = app.results.(sys).(band).(code).range1_slip_distribution_per_sat{sat}.n_slips_20_30;
               n_slips_30_40 = app.results.(sys).(band).(code).range1_slip_distribution_per_sat{sat}.n_slips_30_40;
               n_slips_40_50 = app.results.(sys).(band).(code).range1_slip_distribution_per_sat{sat}.n_slips_40_50;
               n_slips_over_50 = app.results.(sys).(band).(code).range1_slip_distribution_per_sat{sat}.n_slips_over50;
               n_slips_NaN = app.results.(sys).(band).(code).range1_slip_distribution_per_sat{sat}.n_slips_NaN;
               
               data{row, 1}   = sprintf('%3s', strcat(sysID, num2str(sat)));
               data{row, 2}   = sprintf('%.0d', n_obs);
               
               data{row, 4}   = sprintf('%.3f', multipath_RMS);
               data{row, 5}   = sprintf('%.3f', weighted_multipath_RMS);
               data{row, 6}   = sprintf('%.3f', average_sat_angle);
               
               data{row, 8}   = sprintf('%.3.3f', slip_obs_ratio);
               
               
               
               data{row, 7}   = sprintf('%.0d', n_slips);
               data{row, 9}   = sprintf('%.0d', n_slips_0_10);
               data{row, 10}  = sprintf('%.0d', n_slips_10_20);
               data{row, 11}  = sprintf('%.0d', n_slips_20_30);
               data{row, 12}  = sprintf('%.0d', n_slips_30_40);
               data{row, 13}  = sprintf('%.0d', n_slips_40_50);
               data{row, 14}  = sprintf('%.0d', n_slips_over_50);
               data{row, 15}  = sprintf('%.0d', n_slips_NaN);
               
               
               
               
               if nEstimates == 0
                  data{row, 3}   = '0';
               else
                  data{row, 3}   = sprintf('%.0d', nEstimates);
               end
               
               if n_slips == 0
                  data{row, 7}   = '0';
               else
                  data{row, 7}   = sprintf('%.0d', n_slips);
               end
               
               if n_slips_0_10 == 0
                  data{row, 9}   = '0';
               else
                  data{row, 9}   = sprintf('%.0d', n_slips_0_10);
               end
               
               if n_slips_10_20 == 0
                  data{row, 10}   = '0';
               else
                  data{row, 10}  = sprintf('%.0d', n_slips_10_20);
               end
               
               if n_slips_20_30 == 0
                  data{row, 11}   = '0';
               else
                  data{row, 11}  = sprintf('%.0d', n_slips_20_30);
               end
               
               if n_slips_30_40 == 0
                  data{row, 12}   = '0';
               else
                  data{row, 12}  = sprintf('%.0d', n_slips_30_40);
               end
               
               if n_slips_40_50 == 0
                  data{row, 13}   = '0';
               else
                  data{row, 13}  = sprintf('%.0d', n_slips_40_50);
               end
               
               if n_slips_over_50 == 0
                  data{row, 14}   = '0';
               else
                  data{row, 14}  = sprintf('%.0d', n_slips_over_50);
               end
               
               if n_slips_NaN == 0
                  data{row, 15}   = '0';
               else
                  data{row, 15}  = sprintf('%.0d', n_slips_NaN);
               end
               
               
            end
         end
         
         
         
         
         
         uitable("Parent", tab, "ColumnName", columnNames, 'RowName', rowLabels, "Data", data);
         
         rowHeight = 22;
         headerHeight = 27;
         
         parentHeight   = tab.Children(1).Parent.Position(4);
         parentWidth    = tab.Children(1).Parent.Position(3);
         
         tableHeight = floor(n_rows * rowHeight + headerHeight);
         tableWidth = floor(parentWidth * 4/5);
         
         tab.Children(1).Position = [floor((parentWidth-tableWidth)/2), floor((parentHeight-tableHeight)/2), tableWidth, tableHeight];
         
      end