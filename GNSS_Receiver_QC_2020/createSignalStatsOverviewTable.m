function createSignalStatsOverviewTable(app, tab, sys, band, code)
         
         rowNames = {'Signal code used for LC';...
            'RMS multipath[meters]';...
            'Weighted RMS multipath[meters]';...
            'N observation epochs';...
            'N multipath estimates';...
            'N cycle slip periods';...
            'Ratio of N slip periods/N obs epochs [%]'};
         
         rowLabels = {'', '', '', '', '', '', '', };
         
         data = cell(0,0);
         data(:,1) = rowNames;
         
         columnNames = {'', ''};
         
        
         
         n_range1_obs = app.results.(sys).(band).(code).nRange1Obs;
         n_multipath_estimates = app.results.(sys).(band).(code).nEstimates;
         multipath_RMS = app.results.(sys).(band).(code).rms_multipath_range1_averaged;
         elevation_weighted_multipath_RMS = app.results.(sys).(band).(code).elevation_weighted_average_rms_multipath_range1;
         n_cycle_slips = app.results.(sys).(band).(code).range1_slip_distribution.n_slips_Tot;
         
         slip_obs_ratio = (app.results.(sys).(band).(code).range1_slip_distribution.n_slips_Tot/app.results.(sys).(band).(code).nRange1Obs)*100;
         
         data(:, end+1) = cell(7, 1);
         
         data{1, end} = app.results.(sys).(band).(code).range2_Code;
         data{2, end} = sprintf('%.3f', multipath_RMS);
         data{3, end} = sprintf('%.3f', elevation_weighted_multipath_RMS);
         data{4, end} = sprintf('%.0d', n_range1_obs);
         data{5, end} = sprintf('%.0d', n_multipath_estimates);
         
         if n_cycle_slips == 0
            data{6, end} = '0';
         else
            data{6, end} = sprintf('%.0d', n_cycle_slips);
         end
         
         data{7, end} = sprintf('%3.3f', slip_obs_ratio);
            
         
         
         
         uitable("Parent", tab, "ColumnName", columnNames,  'RowName', rowLabels, "Data", data);
         
         n_rows = 7;
         rowHeight = 22;
         headerHeight = 27;
         
         parentHeight   = tab.Children(1).Parent.Position(4);
         parentWidth    = tab.Children(1).Parent.Position(3);
         
         tableHeight = floor(n_rows * rowHeight + headerHeight);
         tableWidth = floor(parentWidth*4/5);
         
         tab.Children(1).Position = [floor((parentWidth-tableWidth)/2), floor((parentHeight-tableHeight)/2), tableWidth, tableHeight];
         
      end