function createStatsSummaryTable(app, tab, sys)
         
         rowNames = {'RMS multipath[meters]';...
            'Weighted RMS multipath[meters]';...
            'N ambiguity slips periods';...
            'N slip periods, elevation angle > 10 degrees';...
            'N slip periods, elevation angle < 10 degrees';...
            'N slip periods, elevation angle not computed';...
            'Ratio of N slip periods/N obs epochs [%]'};
         
         rowLabels = {'', '', '', '', '', '', '', };
         
         data = cell(0,0);
         data(:,1) = rowNames;
         
         columnNames = {['']};
         
         nBands = app.results.(sys).nBands;
         for i = 1:nBands
            band = app.results.(sys).Bands(i);
            nCodes = app.results.(sys).(band).nCodes;
            
            
            for j = 1:nCodes
               code = app.results.(sys).(band).Codes{j};
               columnNames{end+1} = code;
               
               multipath_RMS = app.results.(sys).(band).(code).rms_multipath_range1_averaged;
               elevation_weighted_multipath_RMS = app.results.(sys).(band).(code).elevation_weighted_average_rms_multipath_range1;
               n_cycle_slips = app.results.(sys).(band).(code).range1_slip_distribution.n_slips_Tot;
               n_cycle_slips_under_10 = app.results.(sys).(band).(code).range1_slip_distribution.n_slips_0_10;
               n_cycle_slips_over_10 = app.results.(sys).(band).(code).range1_slip_distribution.n_slips_10_20 + ...
                                       app.results.(sys).(band).(code).range1_slip_distribution.n_slips_20_30 + ...
                                       app.results.(sys).(band).(code).range1_slip_distribution.n_slips_30_40 + ...
                                       app.results.(sys).(band).(code).range1_slip_distribution.n_slips_40_50 + ...
                                       app.results.(sys).(band).(code).range1_slip_distribution.n_slips_over50;
               n_cycle_slips_no_angle = app.results.(sys).(band).(code).range1_slip_distribution.n_slips_NaN;
               slip_obs_ratio = (app.results.(sys).(band).(code).range1_slip_distribution.n_slips_Tot/app.results.(sys).(band).(code).nRange1Obs)*100;
               
               data(:, end+1) = cell(7, 1);
               
               data{1, end} = sprintf('%.3f', multipath_RMS);
               data{2, end} = sprintf('%.3f', elevation_weighted_multipath_RMS);
               
               if n_cycle_slips == 0
                  data{3, end} = '0';
               else
                  data{3, end} = sprintf('%.0d', n_cycle_slips);
               end
               
               if n_cycle_slips_under_10 == 0
                  data{4, end} = '0';
               else
                  data{4, end} = sprintf('%.0d', n_cycle_slips_under_10);
               end
               
               if n_cycle_slips_over_10 == 0
                  data{5, end} = '0';
               else
                  data{5, end} = sprintf('%.0d', n_cycle_slips_over_10);
               end
               
               if n_cycle_slips_no_angle == 0
                  data{6, end} = '0';
               else
                  data{6, end} = sprintf('%.0d', n_cycle_slips_no_angle);
               end
               
               data{7, end} = sprintf('%3.3f', slip_obs_ratio);
               
            end
         end
         
         uitable("Parent", tab, "ColumnName", columnNames, 'RowName', rowLabels, "Data", data);
         
         n_rows = 7;
         rowHeight = 22;
         headerHeight = 27;
         
         parentHeight   = tab.Children(1).Parent.Position(4);
         parentWidth    = tab.Children(1).Parent.Position(3);
         
         tableHeight = floor(n_rows * rowHeight + headerHeight);
         tableWidth = floor(parentWidth*4/5);
         
         tab.Children(1).Position = [floor((parentWidth-tableWidth)/2), floor((parentHeight-tableHeight)/2), tableWidth, tableHeight];
      
      end