function plotCycleSlipDistributionTab(app, tab, sys, bandName, codeName)
         
         
         
         sat_elevation_angles = app.results.(sys).(bandName).(codeName).sat_elevation_angles;
         range1_slip_periods = app.results.(sys).(bandName).(codeName).range1_slip_periods;
         
         tInterval = app.results.ExtraOutputInfo.tInterval;
         
         range1_Code = app.results.(sys).(bandName).(codeName).range1_Code ;
         phase1_Code = app.results.(sys).(bandName).(codeName).phase1_Code ;
         phase2_Code = app.results.(sys).(bandName).(codeName).phase2_Code ;
         
         [n,m] = size(sat_elevation_angles);
         
         t = (1:n)*tInterval;
         
         tab.AutoResizeChildren = 'off';
         ax1 = subplot(1,1,1,'Parent',tab);
         
         set(ax1, 'FontSize', 12);
         hold(ax1, 'on');
         grid(ax1, 'on');
         
         
         for PRN = 1:m
           
            slips = range1_slip_periods{PRN};
            [n_slip_periods, ~] = size(slips);
            
            slip_epochs = zeros(n_slip_periods, 1); 
            elevation_angles = zeros(n_slip_periods, 1);
            
            for slip = 1:n_slip_periods
               slip_epochs(slip) = slips(slip,1);
               elevation_angles(slip) = sat_elevation_angles(slip_epochs(slip), PRN);
            end
               
               
            plot(ax1, t(slip_epochs), elevation_angles, "Marker", "*", 'DisplayName', ['PRN' num2str(PRN)], "LineStyle",'none')
         end
         
         h = findobj(ax1, 'Type', 'line');
         yvalues = get(h, 'Ydata');
         
         if isa(yvalues, 'cell')
            yvalues = cat(2, yvalues{:});
            yvalues = yvalues(:);
            yvalues = yvalues(~isnan(yvalues));
         else
            yvalues = [0, 1];
         end
         
         min_y  = min(yvalues);
         max_y  = max(yvalues);
         
         ylim(ax1, [min_y-5, max_y+5]);
         
         
         yticks(ax1, 1:5:90);
         yticklabels(ax1, 1:5:90);
         title(ax1, sprintf('Cycle slip Elevation Angle Distribution on %s signal, %s\nPhase 1: %s \t\t Phase 2: %s',...
             range1_Code, sys, phase1_Code, phase2_Code), 'FontSize', 17);
         xlabel(ax1, 't [seconds]', 'FontSize', 19);
         ylabel(ax1, 'Elevation Angle [degrees]', 'FontSize', 19);
         legend(ax1, 'show', 'Location', 'northeastoutside', 'NumColumns', 2);
         
         set(ax1, 'XColor', 'w', 'YColor', 'w')
         set(ax1.Title, 'Color', [1,1,1])
         
      end