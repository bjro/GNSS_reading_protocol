function plotCycleSlipOverviewTab(app, tab, sys, bandName, codeName)
         
         
         
         sat_elevation_angles = app.results.(sys).(bandName).(codeName).sat_elevation_angles;
         range1_slip_periods = app.results.(sys).(bandName).(codeName).range1_slip_periods;
         
         phase1_observations = app.results.(sys).(bandName).(codeName).phase1_observations;
         
         tInterval = app.results.ExtraOutputInfo.tInterval;
         
         range1_Code = app.results.(sys).(bandName).(codeName).range1_Code ;
         range2_Code = app.results.(sys).(bandName).(codeName).range2_Code ;
         phase1_Code = app.results.(sys).(bandName).(codeName).phase1_Code ;
         phase2_Code = app.results.(sys).(bandName).(codeName).phase2_Code ;
         
         [n,m] = size(sat_elevation_angles);
         
         t = (1:n)*tInterval;
         
         tab.AutoResizeChildren = 'off';
         ax1 = subplot(1,1,1,'Parent',tab);
         
         set(ax1, 'FontSize', 12);
         hold(ax1, 'on');
         grid(ax1, 'on');
         
         presentPRNs = num2cell(1:m);
         
         
         for PRN = 1:m
            
            
            first_obs_index   = find(phase1_observations(:, PRN) ~= 0, 1, 'first');
            last_obs_index    = find(phase1_observations(:, PRN) ~= 0, 1, 'last');
            
            if ~isempty(first_obs_index)
               obsOverview = ones(1, n);
               obsOverview(1:first_obs_index - 1) = NaN;
               obsOverview(last_obs_index + 1: end) = NaN;
               
               slips = range1_slip_periods{PRN};
               [n_slip_periods, ~] = size(slips);
               for slip = 1:n_slip_periods
                  obsOverview(slips(slip,1):slips(slip, 2)) = NaN;
               end
               
               plot(ax1, t, obsOverview*PRN)
               
               for slip = 1:n_slip_periods
                  plot(ax1, t(slips(slip, 1)), PRN, "Marker","+", "MarkerSize", 10, "Color", 'k')
               end
            else
               presentPRNs{PRN} = [];     
            end
               
         end
         
         firstPRNPresent   = find(~cellfun(@isempty, presentPRNs), 1, 'first');
         lastPRNPresent    = find(~cellfun(@isempty, presentPRNs), 1, 'last');
         
         ylim(ax1, [firstPRNPresent - 1, lastPRNPresent + 1]);
         yticks(ax1, 1:m);
         yticklabels(ax1, presentPRNs);
         title(ax1, sprintf('Cycle slips on %s signal, %s\nPhase 1: %s \t\t Phase 2: %s',...
             range1_Code, sys, phase1_Code, phase2_Code), 'FontSize', 17);
         xlabel(ax1, 't [seconds]', 'FontSize', 19);
         ylabel(ax1, 'PRN', 'FontSize', 19);
         
         set(ax1, 'XColor', 'w', 'YColor', 'w')
         set(ax1.Title, 'Color', [1,1,1])
         
      end