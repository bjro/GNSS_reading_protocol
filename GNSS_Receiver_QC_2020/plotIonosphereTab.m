
      function plotIonosphereTab(app, tab, sys, bandName, codeName)
         
         
         
         sat_elevation_angles = app.results.(sys).(bandName).(codeName).sat_elevation_angles;
         
         ion_delay_phase1 = app.results.(sys).(bandName).(codeName).ion_delay_phase1;
         ion_delay_phase1_zenit = ion_delay_phase1.*cos(asin(6371000/(6371000 + 450000) * sin((90-sat_elevation_angles)*pi/180)));
         
         
         tInterval = app.results.ExtraOutputInfo.tInterval;
         
         range1_Code = app.results.(sys).(bandName).(codeName).range1_Code ;
         range2_Code = app.results.(sys).(bandName).(codeName).range2_Code ;
         phase1_Code = app.results.(sys).(bandName).(codeName).phase1_Code ;
         phase2_Code = app.results.(sys).(bandName).(codeName).phase2_Code ;
         
         [n,m] = size(ion_delay_phase1);
         
         t = (1:n)*tInterval;
         
         tab.AutoResizeChildren = 'off';
         ax1 = subplot(2,1,1,'Parent',tab);
         ax2 = subplot(2,1,2,'Parent',tab);
         
         
         % Ionosphere vs time
         
         set(ax1, 'FontSize', 12);
         grid(ax1, 'on');
         hold(ax1, 'on');
         
         for PRN = 1:m
             % only plot for PRN that have any observations
             if ~all(isnan(ion_delay_phase1(:,PRN)))
                 plot(ax1, t, ion_delay_phase1(:,PRN), 'DisplayName', ['PRN' num2str(PRN)]) 
             end
         end
         
         xl = get(ax1, 'XLim');
         xline = line(ax1, xl, [0,0], 'Color', 'k', 'LineWidth', 1); % Draw line for X axis.
         set(get(get(xline,'Annotation'),'LegendInformation'),'IconDisplayStyle','off');
         
         legend(ax1,'show', 'Location', 'northeastoutside', 'NumColumns', 2);
         xlabel(ax1, 't [seconds]', 'FontSize', 19);
         ylabel(ax1, 'Ionosphere delay [m]', 'FontSize', 19);
         title(ax1, sprintf('Relative ionosphere delay vs time, %s\nPhase 1: %s\t\t Phase 2: %s',...
             sys, phase1_Code, phase2_Code), 'FontSize', 17);
          
         set(ax1, 'XColor', 'w', 'YColor', 'w')
         set(ax1.Title, 'Color', [1,1,1])
          
         % Zenit mapped Ionosphere vs time
         
         excluded_PRN = {};

         set(ax2, 'FontSize', 12);
         grid(ax2, 'on');
         hold(ax2, 'on');
         
         for PRN = 1:m
             % only plot for PRN that have any observations
             if ~all(isnan(ion_delay_phase1(:,PRN))) 
                 if ~any(isnan(sat_elevation_angles(:, PRN)))
                     plot(ax2, t, ion_delay_phase1_zenit(:, PRN), 'DisplayName', ['PRN' num2str(PRN)]) 
                 else
                     excluded_PRN = [excluded_PRN, num2str(PRN)+", "];
                 end
             end
         end
         
         xl = get(ax2, 'XLim');
         xline = line(ax2, xl, [0,0], 'Color', 'k', 'LineWidth', 1); % Draw line for X axis.
         set(get(get(xline,'Annotation'),'LegendInformation'),'IconDisplayStyle','off');
         
         legend(ax2, 'show', 'Location', 'northeastoutside', 'NumColumns', 2);
         xlabel(ax2, 't [seconds]', 'FontSize', 19);
         ylabel(ax2, 'Ionosphere delay [m]', 'FontSize', 19);
         
         if isempty(excluded_PRN)
             title(ax2, sprintf('Relative zenit mapped ionosphere delay vs time, %s\nPhase 1: %s\t\t Phase 2: %s',...
                 sys, phase1_Code, phase2_Code), 'FontSize', 17);
         else
             title(ax2, sprintf(['Relative zenit mapped ionosphere delay vs time, %s\nPhase 1: %s\t\t Phase 2: %s\n',...
                 'Excluded PRN: %s'],sys, phase1_Code, phase2_Code, cell2mat(excluded_PRN)), 'FontSize', 17);
         end
         
         set(ax2, 'XColor', 'w', 'YColor', 'w')
         set(ax2.Title, 'Color', [1,1,1])
         
         
      end