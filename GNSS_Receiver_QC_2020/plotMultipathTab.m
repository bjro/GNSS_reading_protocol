function plotMultipathTab(app, tab, sys, bandName, codeName)
         
        
         multipath_range1 = app.results.(sys).(bandName).(codeName).multipath_range1;
         sat_elevation_angles = app.results.(sys).(bandName).(codeName).sat_elevation_angles;
         tInterval = app.results.ExtraOutputInfo.tInterval;
         
         range1_Code = app.results.(sys).(bandName).(codeName).range1_Code ;
         range2_Code = app.results.(sys).(bandName).(codeName).range2_Code ;
         phase1_Code = app.results.(sys).(bandName).(codeName).phase1_Code ;
         phase2_Code = app.results.(sys).(bandName).(codeName).phase2_Code ;
         
         [n,m] = size(multipath_range1);
         
         t = (1:n)*tInterval;
         
         tab.AutoResizeChildren = 'off';
         ax1 = subplot(2,1,1,'Parent',tab);
         ax2 = subplot(2,1,2,'Parent',tab);
         
         
         % Multipath vs time
         
         set(ax1, 'FontSize', 12);
         grid(ax1, 'on');
         hold(ax1, 'on');
         
         for PRN = 1:m
             % only plot for PRN that have any observations
             if ~all(isnan(multipath_range1(:,PRN)))
                 plot(ax1, t, multipath_range1(:,PRN), 'DisplayName', ['PRN' num2str(PRN)]) 
             end
         end
         
         xl = get(ax1, 'XLim');
         xline = line(ax1, xl, [0,0], 'Color', 'k', 'LineWidth', 1); % Draw line for X axis.
         set(get(get(xline,'Annotation'),'LegendInformation'),'IconDisplayStyle','off');
         
         h = findobj(ax1, 'Type', 'line');
         yvalues = get(h, 'Ydata');
         
         if isa(yvalues, 'cell')
            yvalues = cat(2, yvalues{:});
            yvalues = yvalues(:);
            yvalues = yvalues(~isnan(yvalues));
         else
            yvalues = [0, 1];
         end
         
         mean_y  = mean(yvalues);
         std_y   = std(yvalues);
         ylim(ax1, [mean_y-5*std_y, mean_y+5*std_y]);
         
         legend(ax1, 'show', 'Location', 'northeastoutside', 'NumColumns', 2);
         xlabel(ax1, 't [seconds]', 'FontSize', 19);
         ylabel(ax1, 'Multipath delay [m]', 'FontSize', 19);
         title(ax1, sprintf('Relative multipath delay vs time on %s signal, %s\nPhase 1: %s \t\t Phase 2: %s',...
             range1_Code, sys, phase1_Code, phase2_Code), 'FontSize', 17);
         
         set(ax1, 'XColor', 'w', 'YColor', 'w')
         set(ax1.Title, 'Color', [1,1,1])
          
         
         % Multipath vs elevation angle
          
         excluded_PRN = {};

         set(ax2, 'FontSize', 12);
         grid(ax2, 'on');
         hold(ax2, 'on');
         for PRN = 1:m
             % only plot for PRN that have any observations
             if ~all(isnan(multipath_range1(:,PRN))) 
                 if ~any(isnan(sat_elevation_angles(:, PRN)))
                     plot(ax2, sat_elevation_angles(:, PRN), multipath_range1(:,PRN), 'DisplayName', ['PRN' num2str(PRN)]) 
                 else
                     excluded_PRN = [excluded_PRN, num2str(PRN)+", "];
                 end      
             end
         end
         
         xl = get(ax2, 'XLim');
         xline = line(ax2, xl, [0,0], 'Color', 'k', 'LineWidth', 1); % Draw line for X axis.
         set(get(get(xline,'Annotation'),'LegendInformation'),'IconDisplayStyle','off');
         
         h = findobj(ax2, 'Type', 'line');
         yvalues = get(h, 'Ydata');
         
         if isa(yvalues, 'cell')
            yvalues = cat(2, yvalues{:});
            yvalues = yvalues(:);
            yvalues = yvalues(~isnan(yvalues));
         else
            yvalues = [0, 1];
         end
         
         mean_y  = mean(yvalues);
         std_y   = std(yvalues);
         ylim(ax1, [mean_y-5*std_y, mean_y+5*std_y]);
         
         legend(ax2,'show', 'Location', 'northeastoutside', 'NumColumns', 2);
         xlabel(ax2, 'Satellite elevation angles [degrees]', 'FontSize', 19);
         ylabel(ax2, 'Multipath delay [m]', 'FontSize', 19);
         
         if isempty(excluded_PRN)
             title(ax2, sprintf('Relative multipath delay vs satellite elevation angle on %s signal, %s\nPhase 1: %s\t\t Phase 2: %s',...
                 range1_Code, sys, phase1_Code, phase2_Code), 'FontSize', 17);
         else
             title(ax2, sprintf(['Relative multipath delay vs satellite elevation angle on %s signal, %s\nPhase 1: %s\t\t Phase 2: %s\n',...
                 'Excluded PRN: %s'],range1_Code, GNSSsystemName, phase1_Code, phase2_Code, cell2mat(excluded_PRN)), 'FontSize', 17);
         end
         
         set(ax2, 'XColor', 'w', 'YColor', 'w')
         set(ax2.Title, 'Color', [1,1,1])
         
      end