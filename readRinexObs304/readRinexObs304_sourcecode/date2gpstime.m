function [week, tow] = date2gpstime( year,month,day,hour,min,sec )

% Calculates GPS-week number(integer) and "time-of-week" from year, month,
%   day, hour, min, sec

% Origo for GPS-time is 06.01.1980 00:00:00 UTC
% This function was originally developed by Ola Øvstedal 

% INPUTS

% year,month,day,hour,min,sec: date and time
%--------------------------------------------------------------------------------------------------------------------------
%
% OUTPUTS

% week: GPS-week

% tow: "time-of-week" of GPS-week
%--------------------------------------------------------------------------------------------------------------------------
%%

t0=datenum(1980,1,6);
t1=datenum(year,month,day);
week_flt = (t1-t0)/7;
week = fix(week_flt);
tow_0 = (week_flt-week)*604800;
tow = tow_0 + hour*3600 + min*60 + sec;

end

