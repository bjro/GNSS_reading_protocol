function [nepochs, tLastObs, tInterval, success] = ...
    rinexFindNEpochs304(filename, tFirstObs, tLastObs, tInterval)

% Function that computes number of epochs in Rinex 3.xx observation file.
%--------------------------------------------------------------------------------------------------------------------------
% INPUTS

% filename:         RINEX observation filename

% tFirstObs:        time stamp of the first observation record in the RINEX
%                   observations file; column vector 
%                   [YYYY; MM; DD; hh; mm; ss.sssssss]; 

% tLastObs:         time stamp of the last observation record in the RINEX
%                   observations file; column vector
%                   [YYYY; MM; DD; hh; mm; ss.sssssss]. If this information
%                   was not available in rinex observation header the
%                   default value is Nan. In this case the variable is
%                   determined in this function

% tInterval:        observations interval; seconds. If this information
%                   was not available in rinex observation header the
%                   default value is Nan. In this case the variable is
%                   determined in this function.
%--------------------------------------------------------------------------------------------------------------------------
% OUTPUTS

% nepochs:          number of epochs in Rinex observation file with
%                   observations

% tLastObs:         time stamp of the last observation record in the RINEX
%                   observations file; column vector
%                   [YYYY; MM; DD; hh; mm; ss.sssssss]. If this information
%                   was not available in rinex observation header the
%                   default value is Nan. In this case the variable is
%                   determined in this function

% tInterval:        observations interval; seconds. If this information
%                   was not available in rinex observation header the
%                   default value is Nan.

% success:                  Boolean. 1 if the function seems to be successful, 
%                           0 otherwise
%--------------------------------------------------------------------------------------------------------------------------

% ADVICE: The function rinexFindNEpochs() calculates the amount of observation epochs in
% advance. This calculation will be incredibly more effective if TIME OF
% LAST OBS is included in the header of the observation file. It is
% strongly advized to manually add this information to the header if it is 
% not included by default. 
%--------------------------------------------------------------------------------------------------------------------------
%% Testing input arguments
success = 1;
nepochs = 0;

% Test if filename is valid format
if ~isa(filename,'char')&&~isa(filename,'string')
    sprintf(['INPUT ERROR(rinexFindNEpoch): The input argument filename',...
        'is of type %s.\n Must be of type string or char'],class(filename))
    success = 0;
    return
end

% Test if filename is valid format
if ~isa(tFirstObs,'double')||length(tFirstObs)~=6
    sprintf(['INPUT ERROR(rinexFindNEpoch): The input argument tFirstObs',...
        ' is of type %s and has length %d.\n Must be of type double with length 6'],...
        class(tFirstObs), length(tFirstObs))
    success = 0;
    return
end

% Test if filename is valid format
if (~isa(tLastObs,'double') && length(tLastObs)~=6) && ~any(isnan(tLastObs))
    sprintf(['INPUT ERROR(rinexFindNEpoch): The input argument tLastObs',...
        ' is of type %s and has length %d.\n Must be of type double with length 6, or Nan'],...
        class(tLastObs), length(tLastObs))
    success = 0;
    return
end
    
%%

% open observation file
fid = fopen(filename, 'rt');
seconds_in_a_week = 604800;
% tLastObs is in header
if ~isnan(tLastObs) 
    % tInterval is not in header
    if isnan(tInterval) 
        
        tInterval_found = 0;
        first_epoch_found = 0;
         % calculate tInterval
        while ~tInterval_found % calculate tInterval
            
           line = fgetl(fid); 
           
           % start of new epoch
           answer = strfind(line,'>');
           if ~isempty(answer) 
              if ~first_epoch_found % first epoch
                  first_epoch_time = cell2mat(textscan(line(2:end),'%f', 6));
                  first_epoch_found = 1;
              else % seconds epoch
                  second_epoch_time = cell2mat(textscan(line(2:end),'%f', 6));
                  tInterval = second_epoch_time(6)-first_epoch_time(6);
                  tInterval_found = 1;
              end
           end
        end
    end
    
    [ week1,tow1 ] = date2gpstime( tFirstObs(1),tFirstObs(2), ...
        tFirstObs(3),tFirstObs(4),tFirstObs(5),tFirstObs(6) );
    [ week2,tow2 ] = date2gpstime( tLastObs(1),tLastObs(2), ...
        tLastObs(3),tLastObs(4),tLastObs(5),tLastObs(6) );
    n = ((week2*seconds_in_a_week+tow2) - (week1*seconds_in_a_week+tow1)) / tInterval;
    nepochs = floor(n) + 1;  

% if tLastObs is not in header. Function counts number of epochs manually 
% OBS: This can be VERY inefficent
else 
    disp('INFO(rinexFindEpochs304): The header of the rinex observation file does not contain TIME OF LAST OBS.\n',...
        'Calculating nepochs will will require considerably more time. Consider editing rinex header to include\n',...
        'TIME OF LAST HEADER')
    
    line = fgetl(fid); % read line, -1 if eof reached
    end_of_header_reached = 0;

    while ~end_of_header_reached % Read past header
        answer = strfind(line,'END OF HEADER'); % [] if string not found 
        if ~isempty(answer)
            end_of_header_reached = 1;
        end
        line = fgetl(fid);
    end
    
    while line ~= -1 
        answer = strfind(line,'>'); % [] if string not found 
        if ~isempty(answer)
           nepochs = nepochs + 1; 
        end
        line = fgetl(fid);
    end
end
disp('INFO(rinexFindNEpochs304): Amount of epochs have been computed')
fclose(fid);
end