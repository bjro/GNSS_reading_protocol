function [success, Obs,SVlist, numSV, LLI, SS, eof] =...
rinexReadObsBlock304(fid, numSV, nObsCodes, GNSSsystems, obsCodeIndex, readSS, readLLI)
% Reads all the observations from a RINEX observation block.
%
% Positioned at the beginning of the line immediately after the header of the
% observations block, reads all the observations in this block of a RINEX
% observations file. This function is meant to be used after using function
% rinexReadObsFileHeader304

% Based in the work of Antonio Pestana, rinexReadObsBlock211, March 2015
%--------------------------------------------------------------------------------------------------------------------------
% INPUTS

% fid:                  Matlab file identifier of a Rinex observations text file

% numSV:                total number of satellites with observations in
%                       current observation block, integer

% numOfObsCodes:        column vector containing number of observation
%                       types for each GNSS system. Order is the same as
%                       GNSSsystems

% GNSSsystems:          cell array containing codes of GNSS systems included 
%                       in RINEX observationfile. Elements are strings.
%                       ex. "G" or "E"

% obsCodeIndex:         cell with one cell element for each GNSS system.
%                       Order is the same as GNSSsystems. Each cell element
%                       contains an array of indices. These indices
%                       indicate the observation types that should be
%                       read for each GNSS system. ex. If one index for
%                       GPS is 1 then the first observation type for GPS
%                       should be read.

% readSS:                   Boolean, 0 or 1. 
%                           1 = read "Signal Strength" Indicators
%                           0 = do not read "Signal Strength" Indicators

% readLLI:                  Boolean, 0 or 1. 
%                           1 = read "Loss-Of-Lock Indicators"
%                           0 = do not read "Loss-Of-Lock Indicators"
%--------------------------------------------------------------------------------------------------------------------------
% OUTPUTS

% success:               Boolean. 1 if the function seems to be successful, 
%                       0 otherwise

% Obs:                  matrix [numSV x max_nObs] that stores all 
%                       observations of this observation block. max_nObs 
%                       is the highest number of observation codes that 
%                       any of the GNSS systems have. Which observation
%                       types that are associated with what collumn will
%                       vary between GNSS systems. SVlist will give
%                       overview of what GNSS system each row is connected
%                       to

% SVlist:               column cell [numSV x 1] that conatins the 
%                       identification code of each line of observation 
%                       block. ex. "G21". numSV is total number of 
%                       satellites minus amount of satellites removed.

% numSV:                numSV, unlike the input of same name, is the total 
%                       number of satellites minus amount of satellites 
%                       removed.

% LLI:                  matrix [numSV x max_nObs] that stores all 
%                       "loss-of-lock" indicators of this observation block. 
%                       max_nObs is the highest number of observation codes 
%                       that any of the GNSS systems have. Which observation
%                       types that are associated with what collumn will
%                       vary between GNSS systems. SVlist will give
%                       overview of what GNSS system each row is connected
%                       to

% SS:                   matrix [numSV x max_nObs] that stores all 
%                       "signal strength" indicators of this observation block. 
%                       max_nObs is the highest number of observation codes 
%                       that any of the GNSS systems have. Which observation
%                       types that are associated with what collumn will
%                       vary between GNSS systems. SVlist will give
%                       overview of what GNSS system each row is connected
%                       to

% eof:                  end-of-file flag; 1 if end-of-file was reached, 
%                       0 otherwise
%--------------------------------------------------------------------------------------------------------------------------
%% Initialize variables in case of input error
success                     = NaN;
eof                         = NaN;
max_n_obs_Types             = NaN;
Obs                         = NaN;
LLI                         = NaN;
SS                          = NaN;
SVlist                      = NaN;
removed_sat                 = NaN;
desiredGNSSsystems          = NaN;
%% Testing input arguments

% Test type of GNSS systems
if ~isa(GNSSsystems, 'cell')
   sprintf(['INPUT ERROR(rinexReadObsBlock304): The input argument GNSSsystems',...
        ' is of type %s.\n Must be of type cell'],class(GNSSsystems))
    success = 0;
    return;  
end

% Test type of numSV
if ~isa(numSV, 'double')
   sprintf(['INPUT ERROR(rinexReadObsBlock304): The input argument numSV',...
        ' is of type %s.\n Must be of type double'],class(numSV))
    success = 0;
    return; 
end

% Test type of numOfObsCodes
if ~isa(nObsCodes, 'double')
   sprintf(['INPUT ERROR(rinexReadObsBlock304): The input argument numOfObsTypes',...
        ' is of type %s.\n Must be of type double'],class(nObsCodes))
    success = 0;
    return;  
end

% Test size of numOfObsCodes
if length(nObsCodes) ~= length(GNSSsystems)
    sprintf(['INPUT ERROR(rinexReadObsBlock304): The input argument numOfObsTypes',...
        'must have same length as GNSSsystems'])
    success = 0;
    return; 
end

% Test type of obsCodeIndex
if ~isa(obsCodeIndex, 'cell')
   sprintf(['INPUT ERROR(rinexReadObsBlock304): The input argument numOfObsTypes',...
        ' is of type %s.\n Must be of type cell'],class(obsCodeIndex))
    success = 0;
    return;  
end

% Test type of cell elements of removed ObsCodeIndex
for k = 1:length(GNSSsystems)
    if ~isa(obsCodeIndex{k}, 'double')
        sprintf(['INPUT ERROR(rinexReadObsBlock304): Element with index %d in',...
            ' removedObsTypesIndex is not of type double as it should be. \nVariable has type %s'],...
            k, class(obsCodeIndex{k}))
        success = 0;
        return;
    end
end

%%

success = 1;
eof     = 0;

% Highest number of obs codes of any GNSS system
max_n_obs_Types = max(nObsCodes);

% Initialize variables
Obs = zeros(numSV, max_n_obs_Types) ;
SVlist = cell(numSV,1);

if readLLI
   LLI = zeros(numSV, max_n_obs_Types) ;
end
if readSS
   SS  = zeros(numSV, max_n_obs_Types) ;
end
              

% number of satellites excluded so far
removed_sat = 0                                 ;
desiredGNSSsystems = string(GNSSsystems)        ; 

% Gobble up observation block
for sat = 1:numSV
    
  line = fgetl(fid); % reads one line of text, returns -1 if eof reached
  if line == -1
    eof = 1;
    disp(['ERROR (rinexReadsObsBlock211): the end of the '...
          'observations text file was reached unexpectedly'])
    success = 0;
    return
  end

  SV = line(1:3); % Satellite code, ex. 'G11' or 'E03'
    
  if ~ismember(SV(1), desiredGNSSsystems) % exclude undesired GNSS systems
      removed_sat = removed_sat + 1;
  else
      
       % index of current GNSS system
       GNSSsystemIndex = find([GNSSsystems{:}] == SV(1)); 

       SVlist{sat - removed_sat,1} = SV; % Store SV of current row
       
       n_obs_current_system = nObsCodes(GNSSsystemIndex);
       
       for obs_num = 1:n_obs_current_system
          obsIndex = obsCodeIndex{GNSSsystemIndex}(obs_num);
          charPos = 4+(obsIndex-1)*16;
          % check that the current observation of the current GNSS system
          % is not on the list of obs types to be excluded
          

            % stringlength of next obs. 
            obsLen = min(14, length(line) - charPos); 
            % read next obs
            newObs = strtok(line(charPos:charPos+obsLen)); 

            % If observation missing, set to 0
            if ~isempty(newObs)
               newObs = str2double(newObs);
            else
               newObs = 0;
            end
            % Store new obs
            Obs(sat - removed_sat, obs_num) = newObs        ;
            
            if readLLI
               % read LLI of current obs (if present)
               if charPos+14<=length(line)  
                  newLLI = line(charPos+14); % loss of lock indicator
               else
                  newLLI = ' ';
               end

               % if no LLI set to -999
               if isspace(newLLI)
                  newLLI = -999;
               else
                  newLLI = str2double(newLLI);
               end
               % Store LLI
               LLI(sat - removed_sat, obs_num) = newLLI        ;
            end

            if readSS
               % read SS of current obs (if present)
               if charPos+15<=length(line)  
                  newSS = line(charPos+15); % signal strength
               else
                  newSS = ' ';
               end
            
               % if no SS set to -999
               if isspace(newSS)
                  newSS = -999;
               else
                  newSS = str2double(newSS);
               end
               %Store SS
               SS(sat - removed_sat, obs_num)  = newSS         ;
            end  
       end     
  end
end

% update number og satellites after satellites have been excluded
numSV = numSV - removed_sat;

%remove empty cell elements
SVlist(cellfun('isempty',SVlist))   = [];
Obs(end-removed_sat+1:end, :)       = [];

end
