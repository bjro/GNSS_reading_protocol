function [sat_positions_combined, epoch_dates_combined, navGNSSsystems, nEpochs_combined, epochInterval, multipleIntervals, firstFileIndices] = ...
    combineSP3Nav(n_sp3_files, sat_positions, epoch_dates, navGNSSsystems, nEpochs, epochInterval)

%%
% Function that combines the precise orbital data of two or three SP3
% files. Note that the SP3 files should first be read by the function
% readSP3Nav.m. 
%--------------------------------------------------------------------------------------------------------------------------
% INPUTS:

% n_sp3_files:          int. number of SP3 files read

% sat_positions:      cell. Conatains data from first SP3 file. Each cell 
%                       elements contains position data for a specific GNSS 
%                       system. Order is defined by order of navGNSSsystems. 
%                       Each cell element is another cell that stores 
%                       position data of specific satellites of that 
%                       GNSS system. Each of these cell elements is a matrix 
%                       with [X, Y, Z] position of a epoch in each row.

%                       sat_positions_1{GNSSsystemIndex}{PRN}(epoch, :) = [X, Y, Z]

% epoch_dates:        matrix. Each row contains date of one of the epochs 
%                       from the first SP3 file
%                       [nEpochs_1 x 6]

% navGNSSsystems:     array. Contains string. Each string is a code for a
%                       GNSS system with position data stored in sat_positions.
%                       Must be one of: "G", "R", "E", "C"

% nEpochs:            number of position epochs in first SP3 file, integer

% epochInterval:      interval of position epochs in first SP3 file, seconds

%--------------------------------------------------------------------------------------------------------------------------
% OUTPUTS:

% sat_positions_combined:        cell. Conatains data from all SP3 files. Each cell 
%                       elements contains position data for a specific GNSS 
%                       system. Order is defined by order of navGNSSsystems. 
%                       Each cell element is another cell that stores 
%                       position data of specific satellites of that 
%                       GNSS system. Each of these cell elements is a matrix 
%                       with [X, Y, Z] position of a epoch in each row.

% epoch_dates_combined:          matrix. Each row contains date of one of the epochs 
%                       from all two/three SP3 file
%                       [nEpochs_1 x 6]

% navGNSSsystems:       array. Contains string. Each string is a code for a
%                       GNSS system with position data stored in sat_positions.
%                       Must be one of: "G", "R", "E", "C"

% nEpochs_combined:              number of position epochs in all SP3 files, integer

% epochInterval:        array. interval of position epochs in SP3 file, seconds. If
%                       multipleIntervals is 1, then epochInterval is array with
%                       all intervals, length equal to number of SP3 files

% multipleIntervals:    boolean. 1 if the SP3 files do not have the same epoch 
%                       interval. 0 otherwise

% firstFileIndices:     array. The indices of sat_positions containing info
%                       about the first epoch of each of the SP3 files loaded.
%                       Length of array is equal to number of SP3 files loaded
%                       Only used if multipleIntervals is 1

% success:              boolean, 1 if no error occurs, 0 otherwise
%--------------------------------------------------------------------------------------------------------------------------


%%
multipleIntervals = 0;

for sp3_file_1 = 1:n_sp3_files - 1
   for sp3_file_2  = sp3_file_1+1:n_sp3_files
      
      % check that two SP3 files have same GNSS systems
      if ~isempty(setdiff(navGNSSsystems{sp3_file_1}, navGNSSsystems{sp3_file_2})) || ~isempty(setdiff(navGNSSsystems{sp3_file_2}, navGNSSsystems{sp3_file_1}))
         fprintf('ERROR(CombineSP3Nav): SP3 file %d and %d do not contain the same GNSS systems', sp3_file_1, sp3_file_2);
         [sat_positions, epoch_dates, navGNSSsystems, nEpochs, epochInterval] = deal(NaN);
         return
      end
      
      % check that SP3 file 1 and 2 have same interval
      if epochInterval(sp3_file_1) ~= epochInterval(sp3_file_2)
         fprintf('INFO(combineSP3Nav):The SP3 files %d and %d do not have the same epoch interval', sp3_file_1, sp3_file_2); 
         multipleIntervals = 1;
      end
   end
end




%%


if multipleIntervals
   firstFileIndices = zeros(n_sp3_files, 1);
   firstFileIndices(1) = 1;
   for sp3_file = 2:n_sp3_files
      [n_epoch_previous_file, ~] = size(epoch_dates{sp3_file-1});
      firstFileIndices(sp3_file) =  n_epoch_previous_file + firstFileIndices(sp3_file-1);
   end
else
   firstFileIndices = [1];
   epochInterval = epochInterval(1);
end

%%
nGNSSsystems = length(navGNSSsystems{1});
max_GPS_PRN     = 36; % Max number of GPS PRN in constellation
max_GLONASS_PRN = 36; % Max number of GLONASS PRN in constellation
max_Galileo_PRN = 36; % Max number of Galileo PRN in constellation
max_Beidou_PRN  = 60; % Max number of BeiDou PRN in constellation
max_sat = [max_GPS_PRN, max_GLONASS_PRN, max_Galileo_PRN, max_Beidou_PRN];

navGNSSsystems = navGNSSsystems{1};


% Combine epoch dates from first and second SP3 file
epoch_dates_combined = epoch_dates{1};
% Compute total amount of epochs
nEpochs_combined = nEpochs(1);

% initialize cell structure for storing combined satellite positions
sat_positions_combined = sat_positions{1};


% Combine satellite positions from two SP3 files

for sp3_file = 1:n_sp3_files-1
   for k = 1:nGNSSsystems
      for j = 1:max_sat(k)
          sat_positions_combined{k}{j} = [sat_positions_combined{k}{j}; sat_positions{sp3_file}{k}{j}];
      end
   end
   
   overlap = 0;
   % check that there is no overlap in epochs and that the data files are
   % in correct order
   for i = 1:6
      if epoch_dates{sp3_file+1}(1, i) > epoch_dates_combined(end, i)
         break
      elseif epoch_dates{sp3_file+1}(1, i) == epoch_dates_combined(end, i)
         continue
      else
         overlap = 1;
         break
      end
   end
   
   if overlap
      error('ERROR(CombineSP3Nav): SP3 file %d either has overlap with previous files, or occurs before previous files.',sp3_file+1)
   end
      
   epoch_dates_combined = [epoch_dates_combined; epoch_dates{sp3_file+1}];
   nEpochs_combined = nEpochs_combined + nEpochs(sp3_file+1);
end



end