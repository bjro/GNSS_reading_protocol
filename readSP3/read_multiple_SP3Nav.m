function [sat_positions, epoch_dates, navGNSSsystems, nEpochs, epochInterval] = read_multiple_SP3Nav(sp3_nav_filename_1, sp3_nav_filename_2, sp3_nav_filename_3, plot_overview, desiredGNSSsystems)

% Function that reads up to 3 SP3 navigations files and combines their data 
% into an accesible format.
%--------------------------------------------------------------------------------------------------------------------------
% INPUTS

% sp3_nav_filename_1:         string, path and filename of first SP3
%                             navigation file

% sp3_nav_filename_2:         string, path and filename of second SP3
%                             navigation file

% sp3_nav_filename_3:         string, path and filename of third SP3
%                             navigation file

% desiredGNSSsystems:         array. Contains string. Each string is a code for a
%                             GNSS system that should have its position data stored 
%                             in sat_positions. Must be one of: "G", "R", "E",
%                             "C". If left undefined, it is automatically set to
%                             ["G", "R", "E", "C"]

% plot_overview:              boolean, either 1 or 0.
%                             1 = plot overview of navigation data
%                             0 = do not plot overview of navigation data
%--------------------------------------------------------------------------------------------------------------------------
% OUTPUTS

% sat_positions:    cell. Each cell elements contains position data for a
%                   specific GNSS system. Order is defined by order of 
%                   navGNSSsystems. Each cell element is another cell that 
%                   stores position data of specific satellites of that 
%                   GNSS system. Each of these cell elements is a matrix 
%                   with [X, Y, Z] position of a epoch in each row.

%                   sat_positions{GNSSsystemIndex}{PRN}(epoch, :) = [X, Y, Z]

% epoch_dates:      matrix. Each row contains date of one of the epochs. 
%                   [nEpochs x 6]

% navGNSSsystems:   array. Contains string. Each string is a code for a
%                   GNSS system with position data stored in sat_positions.
%                   Must be one of: "G", "R", "E", "C"

% nEpochs:          number of position epochs, integer

% epochInterval:    interval of position epochs in SP3 file, seconds. If
%                   SP3 files have different intervals, then epochInterval 
%                   is array with all intervals, length equal to number of SP3 files

% n_sp3_files:      int. number of sp3 files read

% success:          boolean, 1 if no error occurs, 0 otherwise
%--------------------------------------------------------------------------------------------------------------------------
%%

% if desiredGNSSsystems not given as argument, set default
if nargin<5
   desiredGNSSsystems = ["G", "R", "E", "C"];
end

% Map container for full system names
GNSS_names = containers.Map({'G', 'R', 'E', 'C'}, {'GPS', 'GLONASS', 'Galileo', 'Beidou'});

n_sp3_files = 1;

% if filenames are empty, set booleans accordingly
if ~(isempty(sp3_nav_filename_2) || sp3_nav_filename_2 == "")
   n_sp3_files = 2;
   if ~(isempty(sp3_nav_filename_3) || sp3_nav_filename_3 == "")
      n_sp3_files = 3;
   end
end

% read first SP3 file
[sat_positions_1, epoch_dates_1, navGNSSsystems_1, nEpochs_1, epochInterval_1, success] = readSP3Nav(sp3_nav_filename_1, desiredGNSSsystems);

if success == 0
   return
end

% if two SP3 files inputted by user, read second SP3 file
if n_sp3_files >= 2
    [sat_positions_2, epoch_dates_2, navGNSSsystems_2, nEpochs_2, epochInterval_2, success] = readSP3Nav(sp3_nav_filename_2, desiredGNSSsystems);
else
    [sat_positions_2, epoch_dates_2, navGNSSsystems_2, nEpochs_2, epochInterval_2] = deal(NaN);
end

if success == 0
   return
end

% if three SP3 files inputted by user, read third SP3 file
if n_sp3_files == 3
    [sat_positions_3, epoch_dates_3, navGNSSsystems_3, nEpochs_3, epochInterval_3, success] = readSP3Nav(sp3_nav_filename_3, desiredGNSSsystems);
else
    [sat_positions_3, epoch_dates_3, navGNSSsystems_3, nEpochs_3, epochInterval_3] = deal(NaN);
end

if success == 0
   return
end

% combine data from different SP3 files
if n_sp3_files >= 2
    [sat_positions, epoch_dates, navGNSSsystems, nEpochs, epochInterval] = ...
        combineSP3Nav(n_sp3_files,...
        sat_positions_1, epoch_dates_1, navGNSSsystems_1, nEpochs_1, epochInterval_1,...
        sat_positions_2, epoch_dates_2, navGNSSsystems_2, nEpochs_2, epochInterval_2,...
        sat_positions_3, epoch_dates_3, navGNSSsystems_3, nEpochs_3, epochInterval_3);
else
    sat_positions = sat_positions_1;
    navGNSSsystems = navGNSSsystems_1;
    nEpochs = nEpochs_1;
    epoch_dates = epoch_dates_1;
    epochInterval = epochInterval_1;
end



nGNSSsystems = length(navGNSSsystems);
nav_data_comprehensiveness_overview = cell(nGNSSsystems,1);
missing_PRN = cell(nGNSSsystems,1);
for sysIndex = 1:nGNSSsystems
   sys = navGNSSsystems(sysIndex);
   msg = sprintf('INFO(read_multiple_SP3Nav): For %s, the SP3 file(s) do not contain navigation data regarding the following satellites: ', GNSS_names(sys));
   
   n_missing_PRN = 0;
   missing_PRN{sysIndex} = [];
   nPRN = length(sat_positions{sysIndex});
   
   nsat = length(sat_positions{sysIndex});
   nav_data_comprehensiveness_overview{sysIndex} = cell(1, nsat);
   for PRN = 1:nPRN
         if all(sat_positions{sysIndex}{PRN}==0, 'all')
            sat_positions{sysIndex}{PRN} = NaN;
            
            n_missing_PRN = n_missing_PRN +1 ;
            missing_PRN{sysIndex} = [missing_PRN{sysIndex}, PRN];
            msg = append(msg, sprintf('%d, ', missing_PRN{sysIndex}(n_missing_PRN)));
            nav_data_comprehensiveness_overview{sysIndex}{PRN} = NaN;
         else
            nav_data_comprehensiveness_overview{sysIndex}{PRN} = zeros(nEpochs, 1);
            
            for epoch = 1:nEpochs
               if ~all(sat_positions{sysIndex}{PRN}(epoch, :) == 0)
                  nav_data_comprehensiveness_overview{sysIndex}{PRN}(epoch) = 1;
               else
                  sat_positions{sysIndex}{PRN}(epoch,:) = [NaN, NaN, NaN];
                  nav_data_comprehensiveness_overview{sysIndex}{PRN}(epoch) = NaN;
               end
            end
         end 
   end
   disp(msg(1:end-2))
   
   if plot_overview
      figure, hold on
      title(sprintf('%s navigation data overview', GNSS_names(sys)))
      ylabel('Sat ID/PRN')
      xlabel(sprintf('epoch (epoch interval: %d seconds)', epochInterval))
      yticks(1:nsat)
      for PRN = 1:nsat
         if ~all(isnan(nav_data_comprehensiveness_overview{sysIndex}{PRN}))
            plot(PRN*nav_data_comprehensiveness_overview{sysIndex}{PRN}, '.', 'LineStyle','none')
         end
      end
   end
end 


